// UMDataBase.cpp : Defines the initialization routines for the DLL.
//
#include "StdAfx.h"
#include "UMDataBase.h"
#include "Resource.h"
#include "MDIDataBaseFrame.h"
#include "MySQLPageOneFormView.h"
#include "MySQLPageTwoFormView.h"

static AFX_EXTENSION_MODULE UMDataBaseDLL = { NULL, NULL };

HINSTANCE hInst = NULL;

// Added: EXPORTED function doDatabaseTables; 080131 p�d
extern "C" AFX_EXT_API void DoDatabaseTables(void);

// Initialize the DLL, register the classes etc
extern "C" AFX_EXT_API void InitModule(CWinApp *app,LPCTSTR suite,vecINDEX_TABLE &,vecINFO_TABLE &);

extern "C" int APIENTRY
DllMain(HINSTANCE hInstance, DWORD dwReason, LPVOID lpReserved)
{
	// Remove this if you use lpReserved
	UNREFERENCED_PARAMETER(lpReserved);

	if (dwReason == DLL_PROCESS_ATTACH)
	{
		
		// Extension DLL one-time initialization
		if (!AfxInitExtensionModule(UMDataBaseDLL, hInstance))
			return 0;
	}
	else if (dwReason == DLL_PROCESS_DETACH)
	{
		// Terminate the library before destructors are called
		AfxTermExtensionModule(UMDataBaseDLL);
	}
	hInst = hInstance;
	return 1;   // ok
}

// Initialize the DLL, register the classes etc
void DLL_BUILD InitModule(CWinApp *app,LPCTSTR suite,vecINDEX_TABLE &idx,vecINFO_TABLE &info)
{
	new CDynLinkLibrary(UMDataBaseDLL);
	CString sLangFN;
	CString sVersion;
	CString sCopyright;
	CString sCompany;
	// Setup the searchpath and name of the program.
	// This information is used e.g. in setting up the
	// Language filename in an OpenSuite() function; 051214 p�d
	sLangFN.Format(_T("%s%s"),getLanguageDir(),PROGRAM_NAME);

	// Form view to enter data

	app->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW,
			RUNTIME_CLASS(CMDIFrameDoc),
			RUNTIME_CLASS(CMDITableImportFrame),
			RUNTIME_CLASS(CTableImportFormView)));
	idx.push_back(INDEX_TABLE(IDD_FORMVIEW,suite,sLangFN,TRUE));

	app->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW1,
			RUNTIME_CLASS(CMDIFrameDoc),
			RUNTIME_CLASS(CMDISetupCompanyFrame),
			RUNTIME_CLASS(CSetupCompanyFormView)));
	idx.push_back(INDEX_TABLE(IDD_FORMVIEW1,suite,sLangFN,TRUE));

	// Get version information; 060803 p�d
	const LPCTSTR VER_NUMBER					= _T("FileVersion");
	const LPCTSTR VER_COMPANY					= _T("CompanyName");
	const LPCTSTR VER_COPYRIGHT					= _T("LegalCopyright");

	sVersion		= getVersionInfo(hInst,VER_NUMBER);
	sCopyright	= getVersionInfo(hInst,VER_COPYRIGHT);
	sCompany		= getVersionInfo(hInst,VER_COMPANY);
	info.push_back(INFO_TABLE(-999,2 /* User module */,
															 (TCHAR*)sLangFN.GetBuffer(),
															 (TCHAR*)sVersion.GetBuffer(),
															 (TCHAR*)sCopyright.GetBuffer(),
															 (TCHAR*)sCompany.GetBuffer()));

	DoDatabaseTables();
}

void DoDatabaseTables(void)
{
	// Check if there's a connection set; 080702 p�d
	if (getIsDBConSet() == 1)
	{
		runSQLScriptFileEx((HMS_ADMINISTRATOR_TABLE),(table_Administrator));
	}
}

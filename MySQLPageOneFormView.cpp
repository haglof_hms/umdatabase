// MySQLPageOneFormView.cpp : implementation file
//

#include "stdafx.h"
#include "UMDataBase.h"
//#include "MySQLView.h"
#include "MDIDataBaseFrame.h"
#include "MySQLPageOneFormView.h"

#include "ResLangFileReader.h"
#include ".\mysqlpageoneformview.h"

// CTableImportFormView

IMPLEMENT_DYNCREATE(CTableImportFormView, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CTableImportFormView, CXTResizeFormView)
	ON_WM_DESTROY()
	ON_WM_SIZE()
	ON_WM_COPYDATA()
	ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessage)
	ON_BN_CLICKED(IDC_BUTTON1, &CTableImportFormView::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, &CTableImportFormView::OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON3, &CTableImportFormView::OnBnClickedButton3)
	ON_CBN_SELCHANGE(IDC_COMBO1, &CTableImportFormView::OnCbnSelchangeCombo1)
	ON_CBN_SELCHANGE(IDC_COMBO2, &CTableImportFormView::OnCbnSelchangeCombo2)
	ON_NOTIFY(NM_CLICK, IDC_PAGEONE_REPORT, OnReportItemClick)
END_MESSAGE_MAP()

CTableImportFormView::CTableImportFormView()
	: CXTResizeFormView(CTableImportFormView::IDD)
{
	m_bInitialized = FALSE;
}

CTableImportFormView::~CTableImportFormView()
{
}

void CTableImportFormView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDC_GROUP1, m_wndGroup1);

	DDX_Control(pDX, IDC_FROM_LBL, m_wndLbl1);
	DDX_Control(pDX, IDC_TO_LBL, m_wndLbl2);

	DDX_Control(pDX, IDC_COMBO1, m_wndCBox1);
	DDX_Control(pDX, IDC_COMBO2, m_wndCBox2);

	DDX_Control(pDX, IDC_BUTTON1, m_wndBtnCheckAll);
	DDX_Control(pDX, IDC_BUTTON2, m_wndBtnUnCheckAll);
	DDX_Control(pDX, IDC_BUTTON3, m_wndBtnDoImport);
	//}}AFX_DATA_MAP
}

BOOL CTableImportFormView::PreCreateWindow(CREATESTRUCT& cs)
{
	if (!CXTResizeFormView::PreCreateWindow(cs))
		return FALSE;

	cs.dwExStyle |= WS_EX_STATICEDGE;
	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;

	return TRUE;

}

void CTableImportFormView::OnDestory(void)
{
	m_ilIcons.DeleteImageList();
	m_sarrDBInCBox2.RemoveAll();
	m_vecTableStruct.clear();
	m_pFrame = NULL;

	CXTResizeFormView::OnDestroy();
}

void CTableImportFormView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

	SetScaleToFitSize(CSize(90, 1));

	if (! m_bInitialized )
	{
		m_sLangAbrev = getLangSet();
		// Setup language filename; 051214 p�d
		m_sLangFN.Format(_T("%s%s%s%s"),getLanguageDir(),PROGRAM_NAME,m_sLangAbrev,LANGUAGE_FN_EXT);

		setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

		setupData();
		
		m_pFrame = (CMDITableImportFrame*)getFormViewByID(IDD_FORMVIEW)->GetParent();

		m_bInitialized = TRUE;
	}
}

BOOL CTableImportFormView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.admin_conn->isConnected();
	}
	return CXTResizeFormView::OnCopyData(pWnd, pData);
}

// Catch message sent from HMSShell (WM_USER_MSG_SUITE), by MDIChildWnd; 060215 p�d
LRESULT CTableImportFormView::OnSuiteMessage(WPARAM wParam,LPARAM lParam)
{
	return 0L;
}

void CTableImportFormView::OnReportItemClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	CXTPReportRows *pRows = NULL;
	CTableImportDataRec *pRec = NULL;
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	if (pItemNotify != NULL)
	{
		if (pItemNotify->pColumn->GetItemIndex() == 0)
		{
			if (isTableSelected()) return;

			m_wndBtnDoImport.EnableWindow(FALSE);

		}	// if (pItemNotify->pColumn->GetItemIndex() == 0)
	}

}

BOOL CTableImportFormView::isTableSelected(void)
{
	CXTPReportRows *pRows = NULL;
	CTableImportDataRec *pRec = NULL;
	pRows = m_wndReport.GetRows();
	if (pRows != NULL)
	{
		for (int i = 0;i < pRows->GetCount();i++)
		{
			pRec = (CTableImportDataRec*)pRows->GetAt(i)->GetRecord();
			if (pRec != NULL)
			{
				if (pRec->getColumnCheck(0))
				{
					// Condition for TRUE is that there's selctions in From and To Database; 081008 p�d
					m_wndBtnDoImport.EnableWindow(m_wndCBox1.GetCurSel() > CB_ERR && m_wndCBox2.GetCurSel() > CB_ERR);
					return TRUE;
				}	// if (pRec->getColumnCheck())
			}	// if (pRec != NULL)
		}	// for (int i = 0;i < pRows->GetCount();i++)
	}	// if (pRows != NULL)

	return FALSE;
}

// Check which tables user's selected for import.
// If he's selected properties and contacts, then table
// for connections between a Property and Contacts'll
// also be imported.
// If user seelcted contacts and categories then
// table to connect categories to a contact'll also 
// be imported; 081008 p�d
BOOL CTableImportFormView::isPropAndContactsSelected(void)
{
	BOOL bIsSelected = FALSE;

	CXTPReportRows *pRows = NULL;
	CTableImportDataRec *pRecProp = NULL;
	CTableImportDataRec *pRecContact = NULL;
	pRows = m_wndReport.GetRows();
	if (pRows != NULL)
	{
		pRecProp = (CTableImportDataRec*)m_vecTableStruct[6].m_pRecord;
		pRecContact = (CTableImportDataRec*)m_vecTableStruct[7].m_pRecord;

		if (pRecProp != NULL && pRecContact != NULL)
		{
			bIsSelected = (pRecProp->getColumnCheck(0) && pRecContact->getColumnCheck(0));
		}	// if (pRecProp != NULL && pRecConatct != NULL && pRecCategory != NULL)
	}	// if (pRows != NULL)
	pRows = NULL;
	pRecProp = NULL;
	pRecContact = NULL;

	return bIsSelected;
}

BOOL CTableImportFormView::isContactsAndCategoriesSelected(void)
{
	BOOL bIsSelected = FALSE;
	CXTPReportRows *pRows = NULL;
	CTableImportDataRec *pRecContact = NULL;
	CTableImportDataRec *pRecCategory = NULL;
	pRows = m_wndReport.GetRows();
	if (pRows != NULL)
	{
		pRecCategory = (CTableImportDataRec*)m_vecTableStruct[8].m_pRecord;
		pRecContact = (CTableImportDataRec*)m_vecTableStruct[7].m_pRecord;

		if (pRecContact != NULL && pRecCategory != NULL)
		{
			bIsSelected = (pRecContact->getColumnCheck(0) && pRecCategory->getColumnCheck(0));
		}	// if (pRecProp != NULL && pRecConatct != NULL && pRecCategory != NULL)
	}	// if (pRows != NULL)
	pRows = NULL;
	pRecContact = NULL;
	pRecCategory = NULL;

	return bIsSelected;
}

void CTableImportFormView::OnSize(UINT nType,int cx,int cy)
{
	CXTResizeFormView::OnSize(nType, cx, cy);

	// Need to set size of Report control; 051219 p�d
	RECT rect;
	GetClientRect(&rect);
	if (m_wndGroup1.GetSafeHwnd())
	{
		setResize(&m_wndGroup1,10,2,MIN_X_SIZE_TABLE_IMPORT-30,65);
	}

	if (m_wndReport.GetSafeHwnd())
	{
		setResize(&m_wndReport,10,100,MIN_X_SIZE_TABLE_IMPORT-30,rect.bottom - 110);
	}

}

// CTableImportFormView diagnostics

#ifdef _DEBUG
void CTableImportFormView::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

void CTableImportFormView::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif //_DEBUG

// PRIVATE

// PROTECTED

BOOL CTableImportFormView::setupData(void)
{
	CString sCBText;
	if (m_wndReport.GetSafeHwnd() == 0)
	{
		if (!m_wndReport.Create(this,IDC_PAGEONE_REPORT))
		{
			return FALSE;
		}
	}

	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			m_sMsgCap = (xml->str(IDS_STRING132));
			m_sActiveDBMsg = (xml->str(IDS_STRING1070));
			
			m_sPropContactMsg.Format(_T("%s\n%s\n\n"),
				(xml->str(IDS_STRING1006)),
				(xml->str(IDS_STRING1007)));
			
			m_sContactCategoryMsg.Format(_T("%s\n%s\n"),
				(xml->str(IDS_STRING1008)),
				(xml->str(IDS_STRING1009)));

			m_sDoImportMsg.Format(_T("%s\n%s\n"),
				(xml->str(IDS_STRING1072)),
				(xml->str(IDS_STRING1073)));

			m_sImportCompletedMsg = (xml->str(IDS_STRING165));
			
			m_sImportFrom = (xml->str(IDS_STRING1071));

			m_sSBarOKMsg = (xml->str(IDS_STRING144));

			m_sMsgChildWindows1.Format(_T("%s\n%s\n\n%s\n"),
					xml->str(IDS_STRING169),
					xml->str(IDS_STRING170),
					xml->str(IDS_STRING171));
			m_sMsgChildWindows2 = xml->str(IDS_STRING172);


			m_wndLbl1.SetWindowText((xml->str(IDS_STRING1001)));
			m_wndLbl2.SetWindowText((xml->str(IDS_STRING1002)));

			m_wndBtnCheckAll.SetWindowText((xml->str(IDS_STRING1003)));
			m_wndBtnUnCheckAll.SetWindowText((xml->str(IDS_STRING1004)));
			m_wndBtnDoImport.SetWindowText((xml->str(IDS_STRING1005)));

			VERIFY(m_ilIcons.Create(16,13, ILC_COLOR24|ILC_MASK, 0, 1));
			CBitmap bmp;
			VERIFY(bmp.LoadBitmap(IDB_BITMAP2));
			m_ilIcons.Add(&bmp, RGB(255, 0, 255));

			m_wndReport.SetImageList(&m_ilIcons);

			m_wndReport.ShowWindow(SW_NORMAL);

			m_wndReport.AddColumn(new CXTPReportColumn(0, _T(""), 20,FALSE,12));
			m_wndReport.AddColumn(new CXTPReportColumn(1, (xml->str(IDS_STRING1010)), 250));
			
			m_wndReport.GetReportHeader()->AllowColumnRemove(FALSE);
			m_wndReport.SetMultipleSelection( FALSE );
			m_wndReport.SetGridStyle( TRUE, xtpReportGridSolid );

			m_vecTableStruct.push_back(TABLE_STRUCT((xml->str(IDS_STRING1050)),TBL_PRICELIST));
			m_vecTableStruct.push_back(TABLE_STRUCT((xml->str(IDS_STRING1051)),TBL_COST_TEMPLATE));
			m_vecTableStruct.push_back(TABLE_STRUCT((xml->str(IDS_STRING1052)),TBL_TMPL_TEMPLATE));
			m_vecTableStruct.push_back(TABLE_STRUCT((xml->str(IDS_STRING1053)),TBL_POSTNUMBERS));
			m_vecTableStruct.push_back(TABLE_STRUCT((xml->str(IDS_STRING1054)),TBL_CMP));
			m_vecTableStruct.push_back(TABLE_STRUCT((xml->str(IDS_STRING1055)),TBL_SPECIES));
			m_vecTableStruct.push_back(TABLE_STRUCT((xml->str(IDS_STRING1056)),TBL_PROPERTIES));
			m_vecTableStruct.push_back(TABLE_STRUCT((xml->str(IDS_STRING1057)),TBL_CONTACTS,TBL_PROP_OWNER));
			m_vecTableStruct.push_back(TABLE_STRUCT((xml->str(IDS_STRING1058)),TBL_CATEGORIES,TBL_CATEGORIES_FOR_CONTACT));
			m_vecTableStruct.push_back(TABLE_STRUCT((xml->str(IDS_STRING1059)),TBL_ELV_TEMPLATES));
			m_vecTableStruct.push_back(TABLE_STRUCT((xml->str(IDS_STRING1060)),TBL_ELV_DOCUMENTS));
			m_vecTableStruct.push_back(TABLE_STRUCT((xml->str(IDS_STRING1061)),TBL_ESTI_TRAKT_TYPE));
			m_vecTableStruct.push_back(TABLE_STRUCT((xml->str(IDS_STRING1062)),TBL_ESTI_ORIGIN));
			m_vecTableStruct.push_back(TABLE_STRUCT((xml->str(IDS_STRING1063)),TBL_ESTI_INPDATA));
			m_vecTableStruct.push_back(TABLE_STRUCT((xml->str(IDS_STRING1064)),TBL_ESTI_CUTCLS));
			m_vecTableStruct.push_back(TABLE_STRUCT((xml->str(IDS_STRING1065)),TBL_ESTI_HGTCLS));
			m_vecTableStruct.push_back(TABLE_STRUCT((xml->str(IDS_STRING1066)),TBL_ELV_PROP_STATUS));

			// Populate report; 081008 p�d
			CXTPReportRecord *pRec;
			for (UINT i = 0;i < m_vecTableStruct.size();i++)
			{
				pRec = m_wndReport.AddRecord(new CTableImportDataRec(FALSE, m_vecTableStruct[i].m_sText, i));
				m_vecTableStruct[i].m_pRecord = pRec;	// save a pointer to the current record in the vector
			}

			m_wndReport.Populate();
			m_wndReport.UpdateWindow();

			// Add Databases in hms_administration to
			// m_wndCBox1; 081008 p�d
			// Add User types to report; 060217 p�d
			if (m_bConnected)
				userdb_get(m_dbConnectionData);
			if (userdb_list().size() > 0)
			{
				m_wndCBox1.ResetContent();
				for (UINT i = 0;i < userdb_list().size();i++)
				{
					CHmsUserDBRec rec = userdb_list()[i];
					sCBText.Format(_T("%s - %s"),rec.getDBName(),rec.getNotes());
					m_wndCBox1.AddString(sCBText);
				}	// for (UINT i = 0;i < userdb_list().size();i++)
			} // if (userdb_list().size() > 0)

			m_wndBtnDoImport.EnableWindow(FALSE);
			RECT rect;
			GetClientRect(&rect);
			if (m_wndReport.GetSafeHwnd())
				setResize(&m_wndReport,10,100,MIN_X_SIZE_TABLE_IMPORT-30,rect.bottom - 110);
		}
		delete xml;
	}	// if (fileExists(sLangFN))


	return TRUE;

}

void CTableImportFormView::OnBnClickedButton1()
{
	CTableImportDataRec *pRec = NULL;
	CXTPReportRows *pRows = m_wndReport.GetRows();
	if (pRows != NULL)
	{
		for (int i = 0;i < pRows->GetCount();i++)
		{
			pRec = (CTableImportDataRec*)pRows->GetAt(i)->GetRecord();
			if (pRec != NULL)
			{
				pRec->setColumnCheck(0,TRUE);
				pRec->setColumnBold(1,FALSE);
				m_wndReport.Populate();
				m_wndReport.UpdateWindow();
			}
		}	// for (int i = 0;i < pRows->GetCount();i++)
	}	// if (pRows != NULL)

	pRows = NULL;
	m_wndReport.Populate();

	// Condition for TRUE is that there's selctions in From and To Database; 081008 p�d
	m_wndBtnDoImport.EnableWindow(m_wndCBox1.GetCurSel() > CB_ERR && 
																m_wndCBox2.GetCurSel() > CB_ERR &&
																isTableSelected());
}

void CTableImportFormView::OnBnClickedButton2()
{
	CTableImportDataRec *pRec = NULL;
	CXTPReportRows *pRows = m_wndReport.GetRows();
	if (pRows != NULL)
	{
		for (int i = 0;i < pRows->GetCount();i++)
		{
			pRec = (CTableImportDataRec*)pRows->GetAt(i)->GetRecord();
			if (pRec != NULL)
			{
				pRec->setColumnCheck(0,FALSE);
				pRec->setColumnBold(1,FALSE);
				m_wndReport.Populate();
				m_wndReport.UpdateWindow();
			}
		}	// for (int i = 0;i < pRows->GetCount();i++)
	}	// if (pRows != NULL)

	pRows = NULL;
	m_wndReport.Populate();

	m_wndBtnDoImport.EnableWindow(FALSE);
}
void CTableImportFormView::OnBnClickedButton3()
{

	if (areThereChildWindows()) return;

	CString sMsg;
	CXTPReportRows *pRows = NULL;
	CTableImportDataRec *pRec = NULL;
	// Start by checkin' selected Tables; 081008 p�d
	sMsg.Empty();
	m_bIsPropAndContactSelected = isPropAndContactsSelected();
	m_bIsContactAndCategoriesSelected = isContactsAndCategoriesSelected();

	::SetCursor(::LoadCursor(NULL,IDC_WAIT));
	if (m_bIsPropAndContactSelected)
	{
		sMsg += m_sPropContactMsg;
	}
	if (m_bIsContactAndCategoriesSelected)
	{
		sMsg += m_sContactCategoryMsg;
	}
	if (!sMsg.IsEmpty())
		sMsg += _T("\n***********\n") + m_sDoImportMsg;
	else
		sMsg = m_sDoImportMsg;

	if (::MessageBox(this->GetSafeHwnd(),sMsg,m_sMsgCap,MB_ICONEXCLAMATION | MB_YESNO | MB_DEFBUTTON2) == IDNO)
		return;
	UpdateWindow();
	// Start carrying out the task of import of tabledata, from one database
	// to another; 081008 p�d
	pRows = m_wndReport.GetRows();
	if (pRows != NULL)
	{
		int nIndex = 0;
		int nSize = pRows->GetCount();

		// #4708: Fixa tabellayout i destinatonstabeller
		runDoAlterTablesInSuitesModules(m_sToDB);

		for (int nLoop=0; nLoop<nSize; nLoop++)
		{
			if ((pRec = (CTableImportDataRec*)pRows->GetAt(nLoop)->GetRecord()) != NULL)
				if (pRec->getColumnCheck(0))
				{
					nIndex = pRec->getColumnIndex();

					if(nIndex == 0)	// pricelists
					{	scriptPricelists(); pRec->setColumnBold(1,TRUE); m_wndReport.Populate();m_wndReport.UpdateWindow();	}
					else if(nIndex == 1)	// cost-templates
					{	scriptCostTemplates(); pRec->setColumnBold(1,TRUE); m_wndReport.Populate();m_wndReport.UpdateWindow();	}
					else if(nIndex == 2)	// stand-templates
					{	scriptTmplTemplates(); pRec->setColumnBold(1,TRUE); m_wndReport.Populate();m_wndReport.UpdateWindow();	}
					else if(nIndex == 3)	// postnumbers
					{	scriptPostNumbers(); pRec->setColumnBold(1,TRUE); m_wndReport.Populate();m_wndReport.UpdateWindow();	}
					else if(nIndex == 4)	// county, municipal and parish
					{	scriptCMP(); pRec->setColumnBold(1,TRUE); m_wndReport.Populate();m_wndReport.UpdateWindow();	}
					else if(nIndex == 5)	// species
					{	scriptSpecies(); pRec->setColumnBold(1,TRUE); m_wndReport.Populate();m_wndReport.UpdateWindow();	}
					else if(nIndex == 6)	// properties
					{	scriptProperties(); pRec->setColumnBold(1,TRUE); m_wndReport.Populate();m_wndReport.UpdateWindow();	}
					else if(nIndex == 7)	// contacts
					{	scriptContacts(); pRec->setColumnBold(1,TRUE); m_wndReport.Populate();m_wndReport.UpdateWindow();	}
					else if(nIndex == 8)	// categories
					{	scriptCategories(); pRec->setColumnBold(1,TRUE); m_wndReport.Populate();m_wndReport.UpdateWindow();	}
					else if(nIndex == 9)	// P30-price/High cost templates
					{	scriptElvTemplates(); pRec->setColumnBold(1,TRUE); m_wndReport.Populate();m_wndReport.UpdateWindow();	}
					else if(nIndex == 10)	// Document-templates
					{	scriptElvDocuments(); pRec->setColumnBold(1,TRUE); m_wndReport.Populate();m_wndReport.UpdateWindow();	}
					else if(nIndex == 11)	// Trakttypes
					{	scriptEstiTraktTypes(); pRec->setColumnBold(1,TRUE); m_wndReport.Populate();m_wndReport.UpdateWindow();	}
					else if(nIndex == 12)	// origin
					{	scriptEstiOrigin(); pRec->setColumnBold(1,TRUE); m_wndReport.Populate();m_wndReport.UpdateWindow();	}
					else if(nIndex == 13)	// Input data methods
					{	scriptEstiInpdata(); pRec->setColumnBold(1,TRUE); m_wndReport.Populate();m_wndReport.UpdateWindow();	}
					else if(nIndex == 14)	// Cut classes (Huggningsklasser)
					{	scriptEstiCutclas(); pRec->setColumnBold(1,TRUE); m_wndReport.Populate();m_wndReport.UpdateWindow();	}
					else if(nIndex == 15)	// Height classes (H�jdklasser)
					{	scriptEstiHgtcls(); pRec->setColumnBold(1,TRUE); m_wndReport.Populate();m_wndReport.UpdateWindow();	}
					else if(nIndex == 16)	// Status for Properties
					{	scriptDynamicStatus(); pRec->setColumnBold(1,TRUE); m_wndReport.Populate();m_wndReport.UpdateWindow();	}
				}
		}

	}	// if (pRows != NULL)

	// Set text on statusbar; 081008 p�d
	if (m_pFrame != NULL) m_pFrame->setStatusBarText(1,m_sSBarOKMsg);

	::SetCursor(::LoadCursor(NULL,IDC_ARROW));

	::MessageBox(this->GetSafeHwnd(),(m_sImportCompletedMsg),(m_sMsgCap),MB_ICONINFORMATION | MB_OK);
}

BOOL CTableImportFormView::scriptPricelists(void)
{
	// Set text on statusbar; 081008 p�d
	CString sSBarText;
	sSBarText.Format(_T("%s : %s -> %s"),m_sImportFrom,m_sFromDB,m_vecTableStruct[0].m_sText);
	if (m_pFrame != NULL) m_pFrame->setStatusBarText(1,sSBarText);

	return copyTable(_T("prn_pricelist_table"));
}

BOOL CTableImportFormView::scriptCostTemplates(void)
{
	// Set text on statusbar; 081008 p�d
	CString sSBarText;
	sSBarText.Format(_T("%s : %s -> %s"),m_sImportFrom,m_sFromDB,m_vecTableStruct[1].m_sText);
	if (m_pFrame != NULL) m_pFrame->setStatusBarText(1,sSBarText);

	return copyTable(_T("cost_template_table"));
}

BOOL CTableImportFormView::scriptTmplTemplates(void)
{
	// Set text on statusbar; 081008 p�d
	CString sSBarText;
	sSBarText.Format(_T("%s : %s -> %s"),m_sImportFrom,m_sFromDB,m_vecTableStruct[2].m_sText);
	if (m_pFrame != NULL) m_pFrame->setStatusBarText(1,sSBarText);

	return copyTable(_T("tmpl_template_table"));
}
BOOL CTableImportFormView::scriptPostNumbers(void)
{
	// Set text on statusbar; 081008 p�d
	CString sSBarText;
	sSBarText.Format(_T("%s : %s -> %s"),m_sImportFrom,m_sFromDB,m_vecTableStruct[3].m_sText);
	if (m_pFrame != NULL) m_pFrame->setStatusBarText(1,sSBarText);

	return copyTable(_T("fst_postnumber_table"));
}

BOOL CTableImportFormView::scriptCMP(void)
{
	// Set text on statusbar; 081008 p�d
	CString sSBarText;
	sSBarText.Format(_T("%s : %s -> %s"),m_sImportFrom,m_sFromDB,m_vecTableStruct[4].m_sText);
	if (m_pFrame != NULL) m_pFrame->setStatusBarText(1,sSBarText);

	return copyTable(_T("fst_county_municipal_parish_table"));
}
BOOL CTableImportFormView::scriptSpecies(void)
{
	// Set text on statusbar; 081008 p�d
	CString sSBarText;
	sSBarText.Format(_T("%s : %s -> %s"),m_sImportFrom,m_sFromDB,m_vecTableStruct[5].m_sText);
	if (m_pFrame != NULL) m_pFrame->setStatusBarText(1,sSBarText);

	return copyTable(_T("fst_species_table"));
}

BOOL CTableImportFormView::scriptProperties(void)
{
	// Set text on statusbar; 081008 p�d
	CString sSBarText;
	sSBarText.Format(_T("%s : %s -> %s"),m_sImportFrom,m_sFromDB,m_vecTableStruct[6].m_sText);
	if (m_pFrame != NULL) m_pFrame->setStatusBarText(1,sSBarText);

	return copyTable(_T("fst_property_table"));
}

// When scripting Contacts, we need to check if user also
// checked Properties. If so we'll import the table "fst_prop_owner_table"; 081009 p�d
BOOL CTableImportFormView::scriptContacts(void)
{
	// Set text on statusbar; 081008 p�d
	CString sSBarText;
	sSBarText.Format(_T("%s : %s -> %s"),m_sImportFrom,m_sFromDB,m_vecTableStruct[7].m_sText);
	if (m_pFrame != NULL) m_pFrame->setStatusBarText(1,sSBarText);

	BOOL bRet = TRUE;
	bRet = copyTable(_T("fst_contacts_table"));

	if (m_bIsPropAndContactSelected && bRet)
	{
		bRet = copyTable(_T("fst_prop_owner_table"));
	}

	return bRet;
}

BOOL CTableImportFormView::scriptCategories(void)
{
	// Set text on statusbar; 081008 p�d
	CString sSBarText;
	sSBarText.Format(_T("%s : %s -> %s"),m_sImportFrom,m_sFromDB,m_vecTableStruct[8].m_sText);
	if (m_pFrame != NULL) m_pFrame->setStatusBarText(1,sSBarText);

	BOOL bRet = TRUE;
	bRet = copyTable(_T("fst_category_table"));

	if (m_bIsContactAndCategoriesSelected && bRet)
	{
		bRet = copyTable(_T("fst_categories_for_contacts_table"));
	}

	return bRet;
}

BOOL CTableImportFormView::scriptElvTemplates(void)
{
	// Set text on statusbar; 081008 p�d
	CString sSBarText;
	sSBarText.Format(_T("%s : %s -> %s"),m_sImportFrom,m_sFromDB,m_vecTableStruct[9].m_sText);
	if (m_pFrame != NULL) m_pFrame->setStatusBarText(1,sSBarText);

	return copyTable(_T("elv_template_table"));
}

BOOL CTableImportFormView::scriptElvDocuments(void)
{
	// Set text on statusbar; 081008 p�d
	CString sSBarText;
	sSBarText.Format(_T("%s : %s -> %s"),m_sImportFrom,m_sFromDB,m_vecTableStruct[10].m_sText);
	if (m_pFrame != NULL) m_pFrame->setStatusBarText(1,sSBarText);

	return copyTable(_T("elv_document_template_table"));
}

BOOL CTableImportFormView::scriptEstiTraktTypes(void)
{
	// Set text on statusbar; 081008 p�d
	CString sSBarText;
	sSBarText.Format(_T("%s : %s -> %s"),m_sImportFrom,m_sFromDB,m_vecTableStruct[11].m_sText);
	if (m_pFrame != NULL) m_pFrame->setStatusBarText(1,sSBarText);

	return copyTable(_T("esti_trakt_type_table"));
}


BOOL CTableImportFormView::scriptEstiOrigin(void)
{
	// Set text on statusbar; 081008 p�d
	CString sSBarText;
	sSBarText.Format(_T("%s : %s -> %s"),m_sImportFrom,m_sFromDB,m_vecTableStruct[12].m_sText);
	if (m_pFrame != NULL) m_pFrame->setStatusBarText(1,sSBarText);

	return copyTable(_T("esti_origin_table"));
}

BOOL CTableImportFormView::scriptEstiInpdata(void)
{
	// Set text on statusbar; 081008 p�d
	CString sSBarText;
	sSBarText.Format(_T("%s : %s -> %s"),m_sImportFrom,m_sFromDB,m_vecTableStruct[13].m_sText);
	if (m_pFrame != NULL) m_pFrame->setStatusBarText(1,sSBarText);

	return copyTable(_T("esti_inpdata_table"));
}

BOOL CTableImportFormView::scriptEstiCutclas(void)
{
	// Set text on statusbar; 081008 p�d
	CString sSBarText;
	sSBarText.Format(_T("%s : %s -> %s"),m_sImportFrom,m_sFromDB,m_vecTableStruct[14].m_sText);
	if (m_pFrame != NULL) m_pFrame->setStatusBarText(1,sSBarText);

	return copyTable(_T("esti_cutcls_table"));
}

BOOL CTableImportFormView::scriptEstiHgtcls(void)
{
	// Set text on statusbar; 081008 p�d
	CString sSBarText;
	sSBarText.Format(_T("%s : %s -> %s"),m_sImportFrom,m_sFromDB,m_vecTableStruct[15].m_sText);
	if (m_pFrame != NULL) m_pFrame->setStatusBarText(1,sSBarText);

	return copyTable(_T("esti_hgtcls_table"));
}

BOOL CTableImportFormView::scriptDynamicStatus(void)
{
	// Set text on statusbar; 081008 p�d
	CString sSBarText;
	sSBarText.Format(_T("%s : %s -> %s"),m_sImportFrom,m_sFromDB,m_vecTableStruct[16].m_sText);
	if (m_pFrame != NULL) m_pFrame->setStatusBarText(1,sSBarText);

	return copyTable(_T("elv_property_status_table"));
}

void CTableImportFormView::OnCbnSelchangeCombo1()
{
	CString sCBText;
	int nIndex = m_wndCBox1.GetCurSel();
	if (nIndex == CB_ERR) return;
	if (m_bConnected)
		userdb_get(m_dbConnectionData);
	if (userdb_list().size() > 0)
	{
		m_wndCBox2.ResetContent();
		m_sarrDBInCBox2.RemoveAll();
		for (UINT i = 0;i < userdb_list().size();i++)
		{
			CHmsUserDBRec rec = userdb_list()[i];
			// Don't add the selected DB to CBOX2
			if (i != nIndex)
			{
				sCBText.Format(_T("%s - %s"),rec.getDBName(),rec.getNotes());
				m_wndCBox2.AddString(sCBText);
				m_sarrDBInCBox2.Add(rec.getDBName());
			}
		}	// for (UINT i = 0;i < userdb_list().size();i++)
	} // if (userdb_list().size() > 0)
	m_sFromDB = userdb_list()[nIndex].getDBName();

	// Condition for TRUE is that there's selctions in From and To Database; 081008 p�d
	m_wndBtnDoImport.EnableWindow(m_wndCBox1.GetCurSel() > CB_ERR && 
																m_wndCBox2.GetCurSel() > CB_ERR &&
																isTableSelected());
	// Reset bold column; 081028 p�d
	CTableImportDataRec *pRec = NULL;
	CXTPReportRows *pRows = m_wndReport.GetRows();
	if (pRows != NULL)
	{
		for (int i = 0;i < pRows->GetCount();i++)
		{
			pRec = (CTableImportDataRec*)pRows->GetAt(i)->GetRecord();
			if (pRec != NULL)
			{
				pRec->setColumnBold(1,FALSE);
				m_wndReport.Populate();
				m_wndReport.UpdateWindow();
			}
		}	// for (int i = 0;i < pRows->GetCount();i++)
	}	// if (pRows != NULL)


}

void CTableImportFormView::OnCbnSelchangeCombo2()
{
	TCHAR szUserDB[127];
	int nIndex = m_wndCBox2.GetCurSel();
	if (nIndex == CB_ERR) return;
	// Check if user's selected the ACTIVE database.
	// If so, tell him that it's not possible to
	// import into ACVTIVE database; 081008 p�d
	if (nIndex < m_sarrDBInCBox2.GetCount())
	{
		// Get database set in registry; 081008 p�d
		GetUserDBInRegistry(szUserDB);
		if (m_sarrDBInCBox2[nIndex].CompareNoCase(szUserDB) == 0)
		{
			::MessageBox(this->GetSafeHwnd(),(m_sActiveDBMsg),(m_sMsgCap),MB_ICONEXCLAMATION | MB_OK);
			m_wndCBox2.SetCurSel(CB_ERR);
			return;
		}	// if (rec.getDBName(szUserDB) == 0)
	}	// if (nIndex < userdb_list().size())
	m_sToDB = m_sarrDBInCBox2[nIndex];
	// Condition for TRUE is that there's selctions in From and To Database; 081008 p�d
	m_wndBtnDoImport.EnableWindow(m_wndCBox1.GetCurSel() > CB_ERR && 
																m_wndCBox2.GetCurSel() > CB_ERR &&
																isTableSelected());

	// Reset bold column; 081028 p�d
	CTableImportDataRec *pRec = NULL;
	CXTPReportRows *pRows = m_wndReport.GetRows();
	if (pRows != NULL)
	{
		for (int i = 0;i < pRows->GetCount();i++)
		{
			pRec = (CTableImportDataRec*)pRows->GetAt(i)->GetRecord();
			if (pRec != NULL)
			{
				pRec->setColumnBold(1,FALSE);
				m_wndReport.Populate();
				m_wndReport.UpdateWindow();
			}
		}	// for (int i = 0;i < pRows->GetCount();i++)
	}	// if (pRows != NULL)

}


BOOL CTableImportFormView::areThereChildWindows(void)
{
	BOOL bWindowsOpen = FALSE;
  CString sDocName, sMsg, sWindows;
  CDocTemplate *pTemplate = NULL;
  CWinApp *pApp = AfxGetApp();

	// Loop through doc templates
  POSITION pos = pApp->GetFirstDocTemplatePosition();
  while(pos)
  {
		pTemplate = pApp->GetNextDocTemplate(pos);
    pTemplate->GetDocString(sDocName, CDocTemplate::docName);

    // Check for instances of this document (ignore database manager document)
    POSITION posDOC = pTemplate->GetFirstDocPosition();
    if (posDOC && sDocName.CompareNoCase(_T("Module600")) != 0)
    {
			CDocument* pDoc = (CDocument*)pTemplate->GetNextDoc(posDOC);
      sWindows += _T(" - ") + pDoc->GetTitle() + _T("\n");
      bWindowsOpen = TRUE;
    }
  }
  if( bWindowsOpen )
  {
		// Display list of open child windows
     sMsg.Format(_T("%s\n%s\n%s\n"),m_sMsgChildWindows1,sWindows,m_sMsgChildWindows2);
     ::MessageBox(this->GetSafeHwnd(),sMsg,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
     return TRUE;
  }
  return FALSE;
}

BOOL CTableImportFormView::copyTable(LPCTSTR szTbl)
{
	BOOL bRet = TRUE;

	// kopiera data i en tabell till en annan
	CUMUserDB *pDB = new CUMUserDB(m_dbConnectionData);
	if (pDB == NULL) return FALSE;

	// h�mta ut databasnamn
	CString csOriginalDB;
	pDB->getDataBase(csOriginalDB);

	// h�mta ut alla kolumner som ska kopieras
	std::vector<CString> vCols;
	if( pDB->getTableColumns(m_sFromDB, szTbl, vCols) == TRUE )
	{
		// t�m destinationstabellen
		if( pDB->clearTable(m_sToDB, szTbl) == TRUE)
		{
			// kopiera data fr�n k�ll- till destinationstabellen
			if( !pDB->copyTable(m_sFromDB, m_sToDB, szTbl, szTbl, vCols) == TRUE)
			{
				bRet = FALSE;
			}
		}
		else
		{
			bRet = FALSE;
		}
	}
	else
	{
		bRet = FALSE;
	}

	// anv�nd gamla databasen
	pDB->setDataBase(csOriginalDB);

	delete pDB;

	return bRet;
}

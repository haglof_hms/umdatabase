// GetSereversDlg.cpp : implementation file
//

#include "stdafx.h"
#include "UMDataBase.h"
#include "GetSereversDlg.h"

#include "ResLangFileReader.h"

// CGetSereversDlg dialog

IMPLEMENT_DYNAMIC(CGetSereversDlg, CDialog)


BEGIN_MESSAGE_MAP(CGetSereversDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CGetSereversDlg::OnBnClickedOk)
END_MESSAGE_MAP()


CGetSereversDlg::CGetSereversDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CGetSereversDlg::IDD, pParent)
{
	m_bInitialized = FALSE;;
}

CGetSereversDlg::~CGetSereversDlg()
{
}

void CGetSereversDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDC_LIST1, m_wndServersLB);
	DDX_Control(pDX, IDOK, m_wndBtnOK);
	DDX_Control(pDX, IDCANCEL, m_wndBtnCancel);
	//}}AFX_DATA_MAP
}

BOOL CGetSereversDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	if (!m_bInitialized)
	{

		m_wndServersLB.ResetContent();
		if (m_sarrServers.GetCount() > 0)
		{
			for (int i = 0;i < m_sarrServers.GetCount();i++)
			{
				m_wndServersLB.AddString(m_sarrServers.GetAt(i));
			}
		}

	// Setup language filename; 051214 p�d
	CString sLangFN;
	sLangFN.Format(_T("%s%s%s%s"),getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);
	if (fileExists(sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(sLangFN))
		{
			SetWindowText((xml->str(IDS_STRING151)));
			m_wndBtnOK.SetWindowText((xml->str(IDS_STRING152)));
			m_wndBtnCancel.SetWindowText((xml->str(IDS_STRING153)));
		}
		delete xml;
	}

		m_bInitialized = TRUE;
	}	// if (!m_bInitialized)

	return FALSE;
	              
}
// CGetSereversDlg message handlers

void CGetSereversDlg::OnBnClickedOk()
{

	int nIdx = m_wndServersLB.GetCurSel();
	// Get selected Database server; 081015 p�d
	if (nIdx != LB_ERR)
	{
		m_wndServersLB.GetText(nIdx,m_sSelectedDBServer);
	}

	OnOK();
}

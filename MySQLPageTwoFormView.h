/*
			FormView; set up a new company (database)
*/

#pragma once

#include "DBHandler.h"
#include "Resource.h"
#include "ProgressDialog.h"


/////////////////////////////////////////////////////////////////////////////
// CPageTwoDataRec
class CPageTwoDataRec
{
//private:
	CString m_sCol1;
	CString m_sCol2;
	CString m_sCol3;
public:
	CPageTwoDataRec(void)
	{
		m_sCol1	= _T("");
		m_sCol2	= _T("");
		m_sCol3	= _T("");
	}
	CPageTwoDataRec(LPCTSTR col1,LPCTSTR col2,LPCTSTR col3)
	{
		m_sCol1	= col1;
		m_sCol2 = col2;
		m_sCol3 = col3;
	}
	CPageTwoDataRec(const CPageTwoDataRec &c)
	{
		*this = c;
	}

	CString getCol1(void)			{ return m_sCol1; }
	CString getCol2(void)			{ return m_sCol2; }
	CString getCol3(void)			{ return m_sCol3; }
};

/////////////////////////////////////////////////////////////////////////////
// CPageTwoReportUserDataRec

class CPageTwoReportUserDataRec : public CXTPReportRecord
{
	int m_nID;
protected:
	class CTextItem : public CXTPReportRecordItemText
	{
	//private:
		CString m_sText;
	public:
		CTextItem(CString sValue) : CXTPReportRecordItemText(sValue)
		{
			m_sText = sValue;
		}
		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_sText = szText;
				SetValue(m_sText);
		}

		CString getTextItem(void)	{ return m_sText; }
		void setTextItem(LPCTSTR text)	
		{ 
			m_sText = text; 
			SetValue(m_sText);
		}
	};

public:

	CPageTwoReportUserDataRec(void)
	{
		m_nID = -1;
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		this->GetItem(0)->SetEditable(TRUE);	
		this->GetItem(0)->SetBackgroundColor(WHITE);		
	}

	CPageTwoReportUserDataRec(CPageTwoDataRec rec)
	{
		AddItem(new CTextItem(rec.getCol1()));
		AddItem(new CTextItem(rec.getCol2()));
		AddItem(new CTextItem(rec.getCol3()));

		if (!rec.getCol1().IsEmpty())
		{
			this->GetItem(0)->SetEditable(FALSE);	
			this->GetItem(0)->SetBackgroundColor(LTCYAN);		
		}
		else
		{
			this->GetItem(0)->SetEditable(TRUE);	
			this->GetItem(0)->SetBackgroundColor(WHITE);		
		}
	}
	
	CString getColumnText(int item)	
	{ 
		return ((CTextItem*)GetItem(item))->getTextItem();
	}
	
	void setColumnText(int item,LPCTSTR text)	
	{ 
		((CTextItem*)GetItem(item))->setTextItem(text);
	}

};

// CSetupCompanyFormView form view

class CSetupCompanyFormView : public CXTResizeFormView,public CDBHandleAdmin
{
	DECLARE_DYNCREATE(CSetupCompanyFormView)

// private:
	BOOL m_bInitialized;
	BOOL m_bShowStatus;
	BOOL m_bShowStatus_profile;
	CString	m_sLangAbrev;
	CString m_sLangFN;

	CString m_sStatusBarOK;
	CString m_sStatusBarCreateDB;
	CString m_sStatusBarCreateTable;
	CString m_sStatusBarText1;
	CString m_sStatusBarText2;

	CString m_sCapMsg;
	CString m_sConnectionErrorMsg;
	CString m_sConnectionOKMsg;
	CString m_sNoDBServerMsg;
	CString m_sCheckSettingsMsg;
	CString m_sUserDBNotExistsMsg;

	CString m_sAddDataToTablesMsg;
	CString m_sPostnumberDataFilesMissingMsg;
	CString m_sCMPDataFilesMissingMsg;
	CString m_sUpdateTableMsg;
	CString m_sFilterText;
	
	BOOL valueChanged(UINT num,CHmsUserDBRec check_rec);

	BOOL m_bIsAdminDataBaseEmpty;

	int m_nAuthentication;

	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;

	CProgressDialog m_wndProgressDlg;

	BOOL checkSettings(void);

protected:
	CSetupCompanyFormView();           // protected constructor used by dynamic creation
	virtual ~CSetupCompanyFormView();

	CXTResizeGroupBox m_wndGroup1;
	CXTResizeGroupBox m_wndGroup2;

	CMyExtStatic m_wndLbl1;
	CMyExtStatic m_wndLbl2;
	CMyExtStatic m_wndLbl3;
	CMyExtStatic m_wndLbl4;
	CMyExtStatic m_wndLbl5;

	CMyExtEdit m_wndUserLogin;
	CMyExtEdit m_wndUserLoginPsw;

	CButton m_wndBtnSearchLocal;
	CButton m_wndBtnSearchServer;
	CButton m_wndBtnSetDB;
	CButton m_wndBtnCheckConnection;
	CButton m_wndBtnCreateDBProfile;
	CButton m_wndBtnImportDBProfile;
	// My data members
	CMyReportCtrl m_wndReport1;

	CMyComboBox m_wndCBoxServerType;
	CMyComboBox m_wndCBoxAuthentication;
	CMyComboBoxHistory m_wndCBoxServer;

	CRect m_rectLbl3Origin;
	CRect m_rectSearchLocalOrigin;
	CRect m_rectSearchServerOrigin;
	CRect m_rectSetDBOrigin;
	CRect m_rectCheckConnectionOrigin;
	CRect m_rectCBoxServerOrigin;
	CRect m_rectBtnCreateDBProfile;
	CRect m_rectBtnImportDBProfile;

	CRect m_rectLblUserLoginOrigin;
	CRect m_rectEditUserLoginOrigin;
	CRect m_rectLblUserLoginPswOrigin;
	CRect m_rectEditUserLoginPswOrigin;

	vecHmsUserDBs m_vecHmsUserDBs;

	CStringArray m_sarrAuthentication;

	BOOL m_bCheckProfile;
	// My methods
	void setResize(CWnd *,int x,int y,int w,int h,BOOL use_winpos = FALSE);
	BOOL setupReport1(void);
	
	BOOL populateReport();
	BOOL clearReport(void);

	BOOL addHmsUserDB(BOOL do_add_tables,CString new_name = _T(""));		// From hms_user_db table
	BOOL deleteHmsUserDB(void);	// From hms_user_db table

	CMDISetupCompanyFrame *m_pFrame;
	void setIniData();

	void setServerTypesInCBox1(void);

	void moveWndOnAuthentication(BOOL move);

	CStringArray m_sarrDBServersLocal;
	CStringArray m_sarrDBServersNetwork;
	BOOL getDBServers(BOOL only_local);
	vecADMIN_INI_DATABASES m_vecAdminIni;

	int checkConnection(void);

	void updateConnection(void);

	CString m_sCurrentUserServer;
	CString m_sMsgChildWindows1;
	CString m_sMsgChildWindows2;
	BOOL areThereChildWindows(void);

	CString m_sMsgDBAlreadyExist;

public:
	BOOL wasAdminDBEmpty(void)	{ return m_bIsAdminDataBaseEmpty; }
	int getNumOfItemsInReport(void);

public:

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSetupStandReportView)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

	enum { IDD = IDD_FORMVIEW1 };
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

	//{{AFX_MSG(CMDIDBFormFrame)
	afx_msg void OnDestroy(void);
	afx_msg LRESULT OnSuiteMessage(WPARAM,LPARAM);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButton1();
	afx_msg void OnCbnSelchangeCombo1();
	afx_msg void OnCbnSelchangeCombo2();
	afx_msg void OnBnClickedButton2();
	afx_msg void OnBnClickedButton3();
	afx_msg void OnBnClickedButton4();
	afx_msg void OnCbnSelchangeCombo3();
	afx_msg void OnReportChanged(NMHDR *pNotifyStruct, LRESULT *pResult);
	afx_msg void OnCreateDBProfile();
	afx_msg void OnImportDBProfile();
};

#if !defined(AFX_DBHANDLER_H)
#define AFX_DBHANDLER_H

#include "StdAfx.h"
#include "HmsAdminRecords.h"
#include "DBBaseClass_SQLApi.h"
#include "DBBaseClass_ADODirect.h"

#include "DBBaseClass_SQLApi.h"	// ... in DBTransaction_lib

//////////////////////////////////////////////////////////////////////////////////
// CDBAdmin; Handle administrator database server transactions; 060228 p�d

class CDBAdmin : public CDBBaseClass_SQLApi
{
public:
	// Data members

	// Methods
	BOOL userdb_Exists(CHmsUserDBRec &rec);
	BOOL database_Exists(CHmsUserDBRec &rec);
	BOOL table_Exists(CHmsUserDBRec &rec,LPCTSTR table_name);

	// This method's used (for now) in SQL Server
	BOOL user_Exists(CHmsUserDBRec &rec,LPCTSTR user_name);

public:
	CDBAdmin(void);
	CDBAdmin(SAClient_t client,LPCTSTR db_name,LPCTSTR user_name,LPCTSTR psw);
	CDBAdmin(DB_CONNECTION_DATA &db_connection);


	// Handle user db table; 060301 p�d
	BOOL userdb_NewUpd(CHmsUserDBRec &rec);						// Adds or updates a userdb in db
	BOOL userdb_Remove(CHmsUserDBRec &rec);						// Removes a userdb from db
	BOOL userdb_Get(vecHmsUserDBs &vec);						// Get all items in userdb table

	// Create user database method; 060302 p�d
	BOOL create_UserDatabase(CHmsUserDBRec &rec);
	BOOL delete_UserDatabase(CHmsUserDBRec &rec);
	
};

//////////////////////////////////////////////////////////////////////////////////
// CDBHandleAdmin; methods handling ALL transaction, based on methods in CDBAdmin
// clas. E.g. getHmsUserTypes, Insert or Update etc; 060228 p�d
// I'll try to add this class as friend

class CDBHandleAdmin
{
	//private:
	vecHmsUserDBs m_vecHmsUserDBs;
public:
	CDBHandleAdmin(void);

	// DB (userdb); 060301 p�d
	BOOL userdb_get(DB_CONNECTION_DATA &db_conn);
	vecHmsUserDBs &userdb_list(void);
	BOOL userdb_new_upd(DB_CONNECTION_DATA &db_conn,vecHmsUserDBs &vec);
	BOOL userdb_remove(DB_CONNECTION_DATA &db_conn,CHmsUserDBRec &rec);

	// Create user database method; 060302 p�d
	BOOL create_userDatabase(DB_CONNECTION_DATA &db_conn,vecHmsUserDBs &vec);
	BOOL delete_userDatabase(vecHmsUserDBs &vec);
};

// class handler for XML; 2005-03-24 p�d
class XMLHandler
{
private:  // Data members
	TCHAR buff[255];
	MSXML2::IXMLDOMDocument2Ptr pXMLDoc;
	CString m_sModulePath;

private:  // Methods
	CString m_sLangSet;

public:
	// Defualt constructor
	XMLHandler();
	// Constructor
	XMLHandler(LPCTSTR module_path,HINSTANCE hinst);
	// Destructor
	~XMLHandler();

	BOOL load(LPCTSTR xml_fn);
	BOOL save(LPCTSTR xml_fn);

	BOOL getDBInfoFromAdministration(vecADMIN_INI_DATABASES &);

};


class CUMUserDB : public CDBBaseClass_SQLApi //CDBHandlerBase
{
//private:
public:
	CUMUserDB(void);
	CUMUserDB(SAClient_t client,LPCTSTR db_name = _T(""),LPCTSTR user_name = _T(""),LPCTSTR psw = _T(""));
	CUMUserDB(DB_CONNECTION_DATA &db_connection);
	// External database item.
	BOOL getProperties(CString sql,vecTransactionProperty &vec);
	BOOL addProperty(CString sql,CTransaction_property &);

	BOOL getContacts(CString sql,vecTransactionContacts &);
	BOOL addContact(CString sql,CTransaction_contacts &);

	BOOL getCategories(CString sql,vecTransactionCategory &);
	BOOL addCategory(CString sql,CTransaction_category &);

	BOOL clearTable(LPCTSTR db,LPCTSTR table);

	BOOL getTableColumns(LPCTSTR /*szDb*/, LPCTSTR /*szTable*/, std::vector<CString> &/*vCols*/);	/* H�mta ut alla kolumnnamnen fr�n en tabell. */
	BOOL copyTable(LPCTSTR /*szSrcDB*/, LPCTSTR /*szDstDB*/, LPCTSTR /*szSrcTbl*/, LPCTSTR /*szDstTbl*/, std::vector<CString> &/*vCols*/);	/* Kopiera tabell fr�n en databas till en annan. Parametrar: K�lldatabas, destinationsdatabas, k�lltabell, destinationstabell, std::vector<CString> med kolumnnamn i k�lltabell */
	BOOL getDataBase(CString& /*szDB*/);	/* H�mta namn p� nuvarande databas. */
	BOOL setDataBase(CString& /*szDB*/); /* S�tt aktiv databas. */
};

#endif
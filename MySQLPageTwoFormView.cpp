// MySQLPageTwoFormView.cpp : implementation file
//

#include "stdafx.h"
#include "DBHandler.h"
#include "UMDataBase.h"
#include "MDIDataBaseFrame.h"
#include "MySQLPageTwoFormView.h"
#include "GetSereversDlg.h"
#include "NewDBDlg.h"

#include <initguid.h>
#include "sqldmoid.h"
#include "sqldmo.h"

#include "ResLangFileReader.h"

inline void CHECK( HRESULT _hr ) 
{ 
  if FAILED(_hr) throw(_hr); 
}

// CSetupCompanyFormView

IMPLEMENT_DYNCREATE(CSetupCompanyFormView, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CSetupCompanyFormView, CXTResizeFormView)
	ON_WM_CLOSE()
	ON_WM_SIZE()
	ON_WM_COPYDATA()
	ON_WM_DESTROY()
	ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessage)
	ON_BN_CLICKED(IDC_BUTTON1, &CSetupCompanyFormView::OnBnClickedButton1)
	ON_CBN_SELCHANGE(IDC_COMBO1, &CSetupCompanyFormView::OnCbnSelchangeCombo1)
	ON_CBN_SELCHANGE(IDC_COMBO2, &CSetupCompanyFormView::OnCbnSelchangeCombo2)
	ON_BN_CLICKED(IDC_BUTTON2, &CSetupCompanyFormView::OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON3, &CSetupCompanyFormView::OnBnClickedButton3)
	ON_BN_CLICKED(IDC_BUTTON4, &CSetupCompanyFormView::OnBnClickedButton4)
	ON_CBN_SELCHANGE(IDC_COMBO3, &CSetupCompanyFormView::OnCbnSelchangeCombo3)
	ON_NOTIFY(XTP_NM_REPORT_VALUECHANGED, IDC_PAGETWO_REPORT, OnReportChanged)
	ON_COMMAND(ID_TBBTN_EXPORT, OnCreateDBProfile)
	ON_COMMAND(ID_TBBTN_IMPORT, OnImportDBProfile)
END_MESSAGE_MAP()

CSetupCompanyFormView::CSetupCompanyFormView()
	: CXTResizeFormView(CSetupCompanyFormView::IDD)
{
	m_bInitialized = FALSE;
	m_bShowStatus = FALSE;
	m_bShowStatus_profile = TRUE;
	m_bIsAdminDataBaseEmpty = TRUE;
	m_pFrame = NULL;
	m_bCheckProfile = FALSE;

}

CSetupCompanyFormView::~CSetupCompanyFormView()
{

	m_sarrAuthentication.RemoveAll();

}

void CSetupCompanyFormView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDC_GROUP1, m_wndGroup1);
	DDX_Control(pDX, IDC_GROUP2, m_wndGroup2);

	DDX_Control(pDX, IDC_LBL1, m_wndLbl1);
	DDX_Control(pDX, IDC_LBL2, m_wndLbl2);
	DDX_Control(pDX, IDC_LBL3, m_wndLbl3);

	DDX_Control(pDX, IDC_LBL4, m_wndLbl4);
	DDX_Control(pDX, IDC_LBL5, m_wndLbl5);
	DDX_Control(pDX, IDC_EDIT1, m_wndUserLogin);
	DDX_Control(pDX, IDC_EDIT2, m_wndUserLoginPsw);

	DDX_Control(pDX, IDC_COMBO1, m_wndCBoxServerType);
	DDX_Control(pDX, IDC_COMBO2, m_wndCBoxAuthentication);
	DDX_Control(pDX, IDC_COMBO3, m_wndCBoxServer);
	
	DDX_Control(pDX, IDC_BUTTON1, m_wndBtnSetDB);
	DDX_Control(pDX, IDC_BUTTON2, m_wndBtnSearchLocal);
	DDX_Control(pDX, IDC_BUTTON3, m_wndBtnSearchServer);
	DDX_Control(pDX, IDC_BUTTON4, m_wndBtnCheckConnection);
	//DDX_Control(pDX, IDC_BUTTON5, m_wndBtnCreateDBProfile);
	//DDX_Control(pDX, IDC_BUTTON6, m_wndBtnImportDBProfile);
	//}}AFX_DATA_MAP
}

BOOL CSetupCompanyFormView::PreCreateWindow(CREATESTRUCT& cs)
{
	if (!CXTResizeFormView::PreCreateWindow(cs))
		return FALSE;

	cs.dwExStyle |= WS_EX_STATICEDGE;
	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;

	return TRUE;

}

void CSetupCompanyFormView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

	//SetScaleToFitSize(CSize(90, 1));

	if (! m_bInitialized )
	{
		// Setup language filename; 051214 p�d
		m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

		setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

		m_pFrame = (CMDISetupCompanyFrame*)getFormViewByID(IDD_FORMVIEW1)->GetParent();

		m_wndCBoxServerType.SetDisabledColor(BLACK,INFOBK);
		m_wndCBoxServerType.SetEnabledColor(BLACK,INFOBK);

		m_wndCBoxAuthentication.SetDisabledColor(BLACK,INFOBK);
		m_wndCBoxAuthentication.SetEnabledColor(BLACK,INFOBK);

		// Setup Origin of components; 081014 p�d
		m_wndLbl3.GetWindowRect(&m_rectLbl3Origin);
		m_wndBtnSearchLocal.GetWindowRect(&m_rectSearchLocalOrigin);
		m_wndBtnSearchServer.GetWindowRect(&m_rectSearchServerOrigin);
		m_wndBtnSetDB.GetWindowRect(&m_rectSetDBOrigin);
		m_wndBtnCheckConnection.GetWindowRect(&m_rectCheckConnectionOrigin);
		m_wndCBoxServer.GetWindowRect(&m_rectCBoxServerOrigin);
		m_wndCBoxServer.LimitText(50);	// 2009-08-26: Changed limit on how big the string can be. (AG)

		ScreenToClient(&m_rectLbl3Origin);
		ScreenToClient(&m_rectSearchLocalOrigin);
		ScreenToClient(&m_rectSearchServerOrigin);
		ScreenToClient(&m_rectSetDBOrigin);
		ScreenToClient(&m_rectCheckConnectionOrigin);
		ScreenToClient(&m_rectCBoxServerOrigin);

		// Label and edit-boxes for entering Username and Password
		// on "Server login" selected; 081014 p�d
		m_wndLbl4.GetWindowRect(&m_rectLblUserLoginOrigin);
		m_wndUserLogin.GetWindowRect(&m_rectEditUserLoginOrigin);
		m_wndLbl5.GetWindowRect(&m_rectLblUserLoginPswOrigin);
		m_wndUserLoginPsw.GetWindowRect(&m_rectEditUserLoginPswOrigin);

		ScreenToClient(&m_rectLblUserLoginOrigin);
		ScreenToClient(&m_rectEditUserLoginOrigin);
		ScreenToClient(&m_rectLblUserLoginPswOrigin);
		ScreenToClient(&m_rectEditUserLoginPswOrigin);

		setResize(&m_wndLbl4,m_rectLblUserLoginOrigin.left,m_rectLbl3Origin.top,m_rectLblUserLoginOrigin.Width(),m_rectLblUserLoginOrigin.Height());
		setResize(&m_wndUserLogin,m_rectEditUserLoginOrigin.left,m_rectLbl3Origin.top+15,m_rectEditUserLoginOrigin.Width(),m_rectEditUserLoginOrigin.Height());	
		setResize(&m_wndLbl5,m_rectLblUserLoginPswOrigin.left,m_rectLbl3Origin.top+40,m_rectLblUserLoginPswOrigin.Width(),m_rectLblUserLoginPswOrigin.Height());
		setResize(&m_wndUserLoginPsw,m_rectEditUserLoginPswOrigin.left,m_rectLbl3Origin.top+55,m_rectEditUserLoginPswOrigin.Width(),m_rectEditUserLoginPswOrigin.Height());

		m_wndLbl4.ShowWindow( SW_HIDE );
		m_wndLbl5.ShowWindow( SW_HIDE );
		m_wndUserLogin.ShowWindow( SW_HIDE );
		m_wndUserLoginPsw.ShowWindow( SW_HIDE );

		setupReport1();

		// Clear list of DB servers; 081015 p�d
		m_sarrDBServersLocal.RemoveAll();
		m_sarrDBServersNetwork.RemoveAll();
		getDBServers(TRUE);	// TRUE = Only local servers; 081015 p�d

		m_wndCBoxServer.LoadHistory(_T("ServerSettings"),_T("History"));
		setIniData();

		m_bInitialized = TRUE;

	}
}

void CSetupCompanyFormView::OnDestroy(void)
{
	m_wndCBoxServer.SaveHistory();

	m_pFrame = NULL;

	CXTResizeFormView::OnDestroy();
}

BOOL CSetupCompanyFormView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.admin_conn->isConnected();

		if( m_bShowStatus && m_bShowStatus_profile )
		{
			// Let user know if connect succeeded or not; 090527 Peter
			if( m_dbConnectionData.admin_conn->isConnected() )
			{
				::MessageBox(this->GetSafeHwnd(),m_sConnectionOKMsg,m_sCapMsg,MB_ICONASTERISK | MB_OK);
			}
			else
			{
				::MessageBox(this->GetSafeHwnd(),m_sConnectionErrorMsg,m_sCapMsg,MB_ICONASTERISK | MB_OK);
			}
		}
	}
	return CXTResizeFormView::OnCopyData(pWnd, pData);
}

void CSetupCompanyFormView::OnSize(UINT nType,int cx,int cy)
{
	CXTResizeFormView::OnSize(nType, cx, cy);
	RECT rect;
	GetClientRect(&rect);

	if (m_wndGroup1.GetSafeHwnd())
		setResize(&m_wndGroup1,1,2,210,rect.bottom-4);

	if (m_wndGroup2.GetSafeHwnd())
		setResize(&m_wndGroup2,215,2,rect.right - 220,rect.bottom-4,TRUE);

	if (m_wndReport1.GetSafeHwnd())
		setResize(&m_wndReport1,225,20,rect.right - 240,rect.bottom - 40);

}

// Catch message sent from HMSShell (WM_USER_MSG_SUITE), by MDIChildWnd; 060215 p�d
LRESULT CSetupCompanyFormView::OnSuiteMessage(WPARAM wParam,LPARAM lParam)
{
	switch (wParam)
	{
		case ID_NEW_ITEM :
		{
			// Bring up new database dialog; 090609 Peter
			CNewDBDlg dlg;
			if( dlg.DoModal() == IDOK )
			{
				// create new database with specified name
				addHmsUserDB(TRUE, dlg.GetName());
			}
			break;
		}	// case ID_NEW_ITEM :

		case ID_DELETE_ITEM :
		{
			if (!areThereChildWindows())
			{
				if (deleteHmsUserDB())
				{
					populateReport();
				}
			}
			break;
		}	// case ID_SAVE_ITEM :

		case ID_UPDATE_ITEM :
		{
			if (lParam == 0)
			{
				populateReport();
			}
			break;
		}	// case ID_SAVE_ITEM :
	}	// switch (wParam)

	return 0L;
}

BOOL CSetupCompanyFormView::valueChanged(UINT num,CHmsUserDBRec check_rec)
{
	if (num > userdb_list().size() - 1 || userdb_list().size() == 0) 
		return TRUE;

	if (userdb_list().size() > 0)
	{
		for (UINT i = 0;i < userdb_list().size();i++)
		{
			CHmsUserDBRec rec = userdb_list()[i];

				if (_tcscmp(check_rec.getDBName(),rec.getDBName()) != 0)	return TRUE;
				if (_tcscmp(check_rec.getUserName(),rec.getUserName()) != 0)	return TRUE;
				if (_tcscmp(check_rec.getNotes(),rec.getNotes()) != 0)	return TRUE;
		}	// for (UINT i = 0;i < usertype_list().size();i++)
	}	// if (usertype_list().size() > 0)
	return FALSE;
}

// PROTECTED METHODS

void CSetupCompanyFormView::setResize(CWnd *wnd,int x,int y,int w,int h,BOOL use_winpos)
{
	CWnd *pWnd = wnd;
	if (pWnd)
	{
		if (!use_winpos)
		{
			pWnd->MoveWindow(x,y,w,h);
		}
		else
		{
			pWnd->SetWindowPos(&CWnd::wndBottom, x, y, w, h,SWP_NOACTIVATE);
		}
	}
}

BOOL CSetupCompanyFormView::setupReport1(void)
{
	vecNETWORK_USERS vec;
	if (m_wndReport1.GetSafeHwnd() == 0)
	{
		if (!m_wndReport1.Create(this,IDC_PAGETWO_REPORT))
		{
			return FALSE;
		}
	}

	if (m_wndReport1.GetSafeHwnd() == NULL)
	{
		return FALSE;
	}
	else
	{	
		m_wndReport1.ShowWindow(SW_NORMAL);

		if (fileExists(m_sLangFN))
		{
			RLFReader *xml = new RLFReader;
			if (xml->Load(m_sLangFN))
			{

				m_wndGroup1.SetWindowText((xml->str(IDS_STRING140)));
				m_wndGroup2.SetWindowText((xml->str(IDS_STRING158)));

				m_wndLbl1.SetWindowText((xml->str(IDS_STRING136)));
				m_wndLbl2.SetWindowText((xml->str(IDS_STRING137)));
				m_wndLbl3.SetWindowText((xml->str(IDS_STRING138)));

				m_wndLbl4.SetWindowText((xml->str(IDS_STRING110)));
				m_wndLbl5.SetWindowText((xml->str(IDS_STRING111)));

				m_wndBtnSearchLocal.SetWindowText((xml->str(IDS_STRING148)));
				m_wndBtnSearchServer.SetWindowText((xml->str(IDS_STRING149)));
				m_wndBtnSetDB.SetWindowText((xml->str(IDS_STRING130)));
				m_wndBtnCheckConnection.SetWindowText((xml->str(IDS_STRING150)));
//				m_wndBtnCreateDBProfile.SetWindowText((xml->str(IDS_STRING180)));
//				m_wndBtnImportDBProfile.SetWindowText((xml->str(IDS_STRING181)));

				m_sarrAuthentication.Add((xml->str(IDS_STRING141)));
				m_sarrAuthentication.Add((xml->str(IDS_STRING142)));

				m_sStatusBarOK = (xml->str(IDS_STRING144));
				m_sStatusBarCreateDB = (xml->str(IDS_STRING145));
				m_sStatusBarCreateTable = (xml->str(IDS_STRING146));
				m_sStatusBarText1 = (xml->str(IDS_STRING154));
				m_sStatusBarText2 = (xml->str(IDS_STRING155));

				m_sCapMsg = (xml->str(IDS_STRING132));
				m_sConnectionErrorMsg.Format(_T("%s\n\n%s\n- %s\n- %s\n- %s\n\n"),
					(xml->str(IDS_STRING156)),
					(xml->str(IDS_STRING173)),
					(xml->str(IDS_STRING174)),
					(xml->str(IDS_STRING175)),
					(xml->str(IDS_STRING157)));
				m_sConnectionOKMsg.Format(_T("%s\n\n%s\n%s\n"),
					(xml->str(IDS_STRING159)),
					(xml->str(IDS_STRING160)),
					(xml->str(IDS_STRING161)));

				m_sNoDBServerMsg.Format(_T("%s\n%s\n"),
					(xml->str(IDS_STRING166)),
					(xml->str(IDS_STRING167)));
				
				m_sCheckSettingsMsg = xml->str(IDS_STRING176);
				
				m_sUserDBNotExistsMsg = xml->str(IDS_STRING182);
				
				m_sAddDataToTablesMsg.Format(_T("%s\n%s\n%s\n\n%s\n"),
					(xml->str(IDS_STRING1080)),
					(xml->str(IDS_STRING1081)),
					(xml->str(IDS_STRING1082)),
					(xml->str(IDS_STRING1083)));
				m_sPostnumberDataFilesMissingMsg = xml->str(IDS_STRING1084);
				m_sCMPDataFilesMissingMsg = xml->str(IDS_STRING1085);
				m_sUpdateTableMsg = xml->str(IDS_STRING1086);

				m_sMsgChildWindows1.Format(_T("%s\n%s\n\n%s\n"),
					xml->str(IDS_STRING169),
					xml->str(IDS_STRING170),
					xml->str(IDS_STRING171));
				m_sMsgChildWindows2 = xml->str(IDS_STRING172);

				m_sFilterText = xml->str(IDS_STRING179);

				m_sMsgDBAlreadyExist = xml->str(IDS_STRING178);

				CXTPReportColumn *pCol = NULL;
				pCol = m_wndReport1.AddColumn(new CXTPReportColumn(0, (xml->str(IDS_STRING113)), 50));		
				pCol->GetEditOptions()->m_nMaxLength = 24;
				pCol = m_wndReport1.AddColumn(new CXTPReportColumn(1, (xml->str(IDS_STRING114)), 50));
				pCol->GetEditOptions()->m_nMaxLength = 24;
				m_wndReport1.AddColumn(new CXTPReportColumn(2, (xml->str(IDS_STRING104)), 120));

				m_wndReport1.GetReportHeader()->AllowColumnRemove(FALSE);
				m_wndReport1.SetMultipleSelection( FALSE );
				m_wndReport1.SetGridStyle( TRUE, xtpReportGridSolid );

				// Read hms_user_types table in hms_administrator scheme(database); 060216 p�d
				populateReport();

				// Need to set size of Report control; 051219 p�d
				RECT rect;
				GetClientRect(&rect);

				if (m_wndGroup2.GetSafeHwnd())
					setResize(&m_wndGroup2,215,2,rect.right - 220,rect.bottom-4,TRUE);
				if (m_wndReport1.GetSafeHwnd())
					setResize(&m_wndReport1,225,20,rect.right - 240,rect.bottom - 40);

				m_wndReport1.AllowEdit(TRUE);
				m_wndReport1.FocusSubItems(TRUE);
				m_wndReport1.SetFocus();
			}
			delete xml;
		}	// if (fileExists(sLangFN))
	}


	return TRUE;

}

BOOL CSetupCompanyFormView::populateReport()
{
	CString S;
	BOOL bDBExists = FALSE;
	BOOL bReturn = FALSE;
	TCHAR szDBSelected[32];
	CXTPReportRecord *pSelRec = NULL;
	m_wndReport1.ResetContent();
	// Add User types to report; 060217 p�d
	
	if (m_bConnected)
	{
		userdb_get(m_dbConnectionData);
		if (m_bCheckProfile)
		{
			// Set client foe SQLApi
			GetUserDBInRegistry(szDBSelected);
		}
	}
	
	if (m_bCheckProfile)
	{
		// Check if database in profile exists (maybe deleted!); 2011-10-28 p�d
		// make sure we've got the lastest information.
		if (userdb_list().size() > 0)
		{
			for (UINT i = 0;i < userdb_list().size();i++)
			{
				CHmsUserDBRec rec = userdb_list()[i];
				if (rec.getDBName().CompareNoCase(szDBSelected) == 0)
				{
					bDBExists = TRUE;
					break;
				}
			}
		}

		if (!bDBExists)
		{
			::MessageBox(this->GetSafeHwnd(),m_sUserDBNotExistsMsg,m_sCapMsg,MB_ICONASTERISK | MB_OK);
		}
	}
	if (userdb_list().size() > 0)
	{

		for (UINT i = 0;i < userdb_list().size();i++)
		{
			CHmsUserDBRec rec = userdb_list()[i];
			if (rec.getDBName().CompareNoCase(szDBSelected) == 0)
			{
				pSelRec = m_wndReport1.AddRecord(new CPageTwoReportUserDataRec(CPageTwoDataRec(rec.getDBName().MakeLower(),
																																											 rec.getUserName(),
		 																																									 rec.getNotes())));
			}
			else
			{
				m_wndReport1.AddRecord(new CPageTwoReportUserDataRec(CPageTwoDataRec(rec.getDBName().MakeLower(),
																																						 rec.getUserName(),
		 																																				 rec.getNotes())));
			}

		}	// for (UINT i = 0;i < m_vecUserDB.size();i++)

		m_wndReport1.Populate();
	
		if (pSelRec)
		{
			CXTPReportRow *pRow = m_wndReport1.GetRows()->Find(pSelRec);
			if (pRow)
			{
				m_wndReport1.SetFocusedRow(pRow);
			}
		}
		bReturn = TRUE;

	} // if (userdb_list().size() > 0)

	m_wndReport1.Populate();
	m_wndReport1.UpdateWindow();

	m_bCheckProfile = FALSE;

	return bReturn;
}

// Save data in report (adds and/or updates); 060301 p�d
BOOL CSetupCompanyFormView::addHmsUserDB(BOOL do_add_tables,CString new_name)
{
	BOOL ret = FALSE;
	vecHmsUserDBs vec;
	CHmsUserDBRec rec;
	TCHAR old_name[256];

	::SetCursor(::LoadCursor(NULL,IDC_WAIT));

	// Put this new record in a vector
	rec = CHmsUserDBRec(new_name, _T(""), _T(""));
	vec.push_back(rec);

	// Add or update data; 060228 p�d
	if (m_pFrame != NULL) 
		m_pFrame->setStatusBarText(1,m_sStatusBarCreateDB);

	if (m_bConnected)
	{
		setIsDBConSet(1);	// Set connected in Registry; 090611 p�d

		CDBAdmin db(m_dbConnectionData);
		if( db.database_Exists(rec) )
		{
			AfxMessageBox(m_sMsgDBAlreadyExist);
		}
		else if( create_userDatabase(m_dbConnectionData,vec) )	// Create database
		{
			if( userdb_new_upd(m_dbConnectionData, vec) )		// Add record to hms_administrator
			{
				// Add tables. HACKHACK: make sure selected db is stored in registry first; 091103 Peter
				GetUserDBInRegistry(old_name);
				WriteUserDBToRegistry(new_name);
				if( do_add_tables )
				{
					// Also, after creating the Database, try to add tables to the database; 081001 p�d
					m_wndProgressDlg.Create(CProgressDialog::IDD,this);
					if(m_wndProgressDlg.GetSafeHwnd())	m_wndProgressDlg.ShowWindow(SW_NORMAL);
					runDoDatabaseTablesInSuitesModules(new_name,m_pFrame->getStatusBar(),m_wndProgressDlg,m_sStatusBarCreateTable);
					if (m_wndProgressDlg.GetSafeHwnd()) m_wndProgressDlg.DestroyWindow();
				}
				WriteUserDBToRegistry(old_name); // We also need to restore this value after tables has been created (or this new db will become active when app is restarted); 100120 Peter

				// Add row to report control
				m_wndReport1.AddRecord(new CPageTwoReportUserDataRec(CPageTwoDataRec(new_name, _T(""), _T(""))));
				m_wndReport1.Populate();
				m_wndReport1.UpdateWindow();

				// Put in use it immediately; 120124 Peter (redmine #2779)
				m_wndReport1.SetFocusedRow(m_wndReport1.GetRows()->GetAt(m_wndReport1.GetRows()->GetCount()-1));
				OnBnClickedButton1();

				ret = TRUE;
			}
		}
	}

	::SetCursor(::LoadCursor(NULL,IDC_ARROW));

	// Add or update data; 060228 p�d
	if (m_pFrame != NULL) m_pFrame->setStatusBarText(1,m_sStatusBarOK);

	return TRUE;
}

// Handle database in the TabControl derived class; 060216 p�d
BOOL CSetupCompanyFormView::deleteHmsUserDB(void)
{
	BOOL bDoDelete = FALSE;

	CHmsUserDBRec recUserDB;

	CString sMsg;
	TCHAR szUserDB[127];
	
	// Get the active database set in Regsitry and compare
	// to the database to be deleted, because the active
	// database can not be deleted; 081007 p�d
	GetUserDBInRegistry(szUserDB);

	CXTPReportRow *pRow = m_wndReport1.GetFocusedRow();
	if (pRow)
	{
		RLFReader *xml = new RLFReader;
		CPageTwoReportUserDataRec *pRec = (CPageTwoReportUserDataRec*)pRow->GetRecord();
		if (pRec)
		{
			recUserDB = CHmsUserDBRec(pRec->getColumnText(0),pRec->getColumnText(1),pRec->getColumnText(2));
			if (fileExists(m_sLangFN))
			{
				if (xml->Load(m_sLangFN))
				{			
					// Ask user if he realy want's to delete the database
					sMsg.Format(_T("%s\n\n>>> %s <<<\n\n%s\n\n%s"),
							xml->str(IDS_STRING120),
							pRec->getColumnText(0),
							xml->str(IDS_STRING121),
							xml->str(IDS_STRING122));
					bDoDelete = (::MessageBox(this->GetSafeHwnd(),sMsg,(xml->str(IDS_STRING132)),
												MB_YESNO | MB_ICONEXCLAMATION | MB_DEFBUTTON2) == IDYES);
				}	// if (xml->Load(m_sLangFN))
			}	// if (fileExists(m_sLangFN))
			if (bDoDelete)
			{

				if (m_bConnected)
				{
					if (userdb_get(m_dbConnectionData))
					{
						if( userdb_remove(m_dbConnectionData,recUserDB) )
						{
							// Compare database to be deleted to active database; 081007 p�d
							if (recUserDB.getDBName().CompareNoCase(szUserDB) == 0)
							{
								// Set selected user database in registry; 081008 p�d
								WriteUserDBToRegistry(_T(""));
								setIsDBConSet(0);	
								// Change name of active database; 081008 p�d

								// Send a ID_DO_SOMETHING_IN_SHELL message to the Shell
								// with ID_LPARAM_COMMAND1, telling Shell to run the
								// setServerInfoOnStatusBar() method; 081008 p�d
								AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,ID_DO_SOMETHING_IN_SHELL,ID_LPARAM_COMMAND1);
							}

							// Delete any GIS project files for this databas
							TCHAR szPath[MAX_PATH];
							CString str, tmp;
							SHGetFolderPath(NULL, CSIDL_APPDATA, NULL, 0, szPath);
							tmp = m_sCurrentUserServer;
							tmp.Replace('\\', '_');
							str.Format(_T("%s\\HMS\\GIS\\%s-%s.ttkgp"), szPath, tmp, recUserDB.getDBName());
							DeleteFile(str);
							str.Format(_T("%s\\HMS\\GIS\\%s-%s.~ttkgp"), szPath, tmp, recUserDB.getDBName());
							DeleteFile(str);
						}
					}
				}

			}	// if (bDoDelete)
			
			return TRUE;
		} // if (pRec)
		delete xml;
	}	// if (pRow)
	return FALSE;

}

void CSetupCompanyFormView::setIniData()
{
	CString sDBStr,S;
	CString sAdminPath;
	CString sDBLocation;
	TCHAR szDBLocation[128];
	TCHAR szDBServer[128];
	TCHAR szDBUser[32];
	TCHAR szDBPsw[32];
	TCHAR szDSNName[32];
	TCHAR szDBSelected[32];
	TCHAR szDBClient[32];
	vecDBSERVERS vecDBServers;
	int nSelIndex = -1;

	sAdminPath.Format(_T("%s\\%s"),getShellDataDir(),SHELLDATA_AdministrationFN);

	if (fileExists(sAdminPath))
	{
		XMLHandler *xml = new XMLHandler();
		if (xml)
		{
			if (xml->load(sAdminPath))
			{
				xml->getDBInfoFromAdministration(m_vecAdminIni);
			}
			delete xml;
		}
		GetSupportedServers(m_vecAdminIni,vecDBServers);
	}

	// Save private profile data to admin.ini file; 060216 p�d
	GetAdminIniData(szDBServer,szDBLocation,szDBUser,szDBPsw,szDSNName,szDBClient,&m_nAuthentication);
	// Set client foe SQLApi
	GetUserDBInRegistry(szDBSelected);

	if (vecDBServers.size() > 0)
	{
		m_wndCBoxServerType.ResetContent();
		for (UINT i = 0;i < vecDBServers.size();i++)
		{
			_dbservers rec = vecDBServers[i];
			m_wndCBoxServerType.AddString(rec.sDBServerName);
		}	// for (UINT i = 0;i < vecDBServers.size();i++)
		m_wndCBoxServerType.SetCurSel(m_wndCBoxServerType.FindString(-1,szDBServer));
		if( m_wndCBoxServerType.GetCurSel() < 0 )
		{
			m_wndCBoxServerType.SetCurSel(0); // Set default value; 090605 Peter
			OnCbnSelchangeCombo1(); // Store registry settings; 090608 Peter
		}
	}	// if (vecDBServers.size() > 0)
	
	int nFindIdx = m_wndCBoxServer.FindString(-1,sDBLocation.MakeUpper());
	m_sCurrentUserServer = szDBLocation;
	if (nFindIdx >= 0)
		m_wndCBoxServer.SetCurSel(nFindIdx);
	else
		m_wndCBoxServer.AddString(m_sCurrentUserServer.MakeUpper());

	// Added 2008-01-30 p�d
	// Combobox for authentication on login
	// Can be: Windows Authentication
	//				 Server Authentication
	m_wndCBoxAuthentication.ResetContent();
	for (int i = 0;i < m_sarrAuthentication.GetCount();i++)
		m_wndCBoxAuthentication.AddString(m_sarrAuthentication.GetAt(i));
	if (m_nAuthentication >= 0 && m_nAuthentication < m_sarrAuthentication.GetCount())
	{
		m_wndCBoxAuthentication.SetCurSel(m_nAuthentication);
	}

	// Add information on User login and Password; 081015 p�d
	m_wndUserLogin.SetWindowText((szDBUser));
	m_wndUserLoginPsw.SetWindowText((szDBPsw));

	// Check authentication. If 'Server login', then we'll
	// also display User login information; 081015 p�d
	moveWndOnAuthentication(m_nAuthentication == 1);	// 1 = Server login

}


// Get DBServers (SQL Servers); 080704 p�d
BOOL CSetupCompanyFormView::getDBServers(BOOL only_local)
{
	BOOL bReturn = FALSE;
	CString sDBServer;
	if (!only_local)
	{
		if (m_sarrDBServersNetwork.GetCount() == 0)
			getDBServersList(m_sarrDBServersNetwork,FALSE);
		CGetSereversDlg *pDlg = new CGetSereversDlg();
		pDlg->setServers(m_sarrDBServersNetwork);
		if (pDlg->DoModal() == IDOK)
		{
			UpdateWindow();
			sDBServer = pDlg->getSelectedDBServer();
			m_wndCBoxServer.AddString(sDBServer.MakeUpper());
			m_wndCBoxServer.SetCurSel(m_wndCBoxServer.FindString(0,sDBServer));
			bReturn = TRUE;
			// Also set this server as the selected one in ComboBox; 081015 p�d
		}	// if (pDlg->DoModal() == IDOK)
		delete pDlg;
	}	// if (!only_local)
	else	// Add lokal
	{	
		getDBServersList(m_sarrDBServersLocal,TRUE);
		// Add to Combobox; 080704 p�d
		if (m_sarrDBServersLocal.GetCount() > 0)
		{
			m_wndCBoxServer.ResetContent();
			for (int i = 0;i < m_sarrDBServersLocal.GetCount();i++)
			{
				sDBServer = m_sarrDBServersLocal.GetAt(i);
				m_wndCBoxServer.AddString(sDBServer.MakeUpper());
			}
			bReturn = TRUE;
		}
	}
	return bReturn;
}

// Check if connection to database is ok; 070329 p�d
int CSetupCompanyFormView::checkConnection(void)
{
	CString sAdminPath;
	//CString sDBServer;
	TCHAR szDBUser[32];
	TCHAR szDBPsw[32];
	TCHAR szDBLocation_old[128];
	TCHAR szDBServerClient_old[128];
	TCHAR szDBUser_old[32];
	TCHAR szDBPsw_old[32];
	TCHAR szDSNName_old[32];
	TCHAR szDBClient_old[32];

	int nIsWindowsAuthentication = -1;
	int nReturn = -1;

	CString S;
	// Get private profile data; 081030 p�d
	GetAdminIniData(szDBServerClient_old, szDBLocation_old, szDBUser_old, szDBPsw_old, szDSNName_old, szDBClient_old, &nIsWindowsAuthentication);

	// Get DB server selected
	int nIndex = m_wndCBoxServer.GetCurSel();
	if (nIndex != LB_ERR)
		m_wndCBoxServer.GetLBText(nIndex,m_sCurrentUserServer);
	else
		m_wndCBoxServer.GetWindowText(m_sCurrentUserServer);

	_tcscpy(szDBUser,m_wndUserLogin.getText());
	_tcscpy(szDBPsw,m_wndUserLoginPsw.getText());
	int nAuthentication = m_wndCBoxAuthentication.GetCurSel();
	if (nAuthentication == 0)
	{
		if (connectToDB(nAuthentication,m_sCurrentUserServer))
		{
			WriteAdminIniData(szDBServerClient_old, m_sCurrentUserServer, _T(""), _T(""), szDSNName_old, szDBClient_old, nAuthentication);
			// Remove any userdatabase (company), from registry on changing DB Server; 081014 p�d
			//WriteUserDBToRegistry(_T(""));
			return TRUE;
		}
	}
	else if (nAuthentication == 1)
	{
		if (connectToDB(nAuthentication,m_sCurrentUserServer,szDBUser,szDBPsw))
		{
			WriteAdminIniData(szDBServerClient_old, m_sCurrentUserServer,szDBUser, szDBPsw, szDSNName_old, szDBClient_old, nAuthentication);
			// Remove any userdatabase (company), from registry on changing DB Server; 081014 p�d
			//WriteUserDBToRegistry(_T(""));
			return TRUE;
		}
	}

	// Remove item from ComboBox
	m_wndCBoxServer.DeleteString(m_wndCBoxServer.FindString(0,m_sCurrentUserServer));


	return FALSE;
}

void CSetupCompanyFormView::updateConnection(void)
{
	// stop ID_TIMER2 in HMSShell, so it won't try to reconnect before the connection is set up properly
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,ID_DO_SOMETHING_IN_SHELL, ID_LPARAM_COMMAND_DBTIMER_STOP);

	if (checkConnection())
	{
		// We only do this if there's only one item added to list; 080421 p�d
		m_bShowStatus = TRUE;
		AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,ID_DO_SOMETHING_IN_SHELL, ID_LPARAM_COMMAND1);
		setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

		populateReport();

		// Make sure to enable add, remove toolbar buttons; 100120 Peter
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,TRUE);

		// start ID_TIMER2 in HMSShell, so it can check the connection status
		AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,ID_DO_SOMETHING_IN_SHELL, ID_LPARAM_COMMAND_DBTIMER_START);
	}
	else
	{
		// Tell user, that connedction failed; 081015 p�d
		::MessageBox(this->GetSafeHwnd(),m_sConnectionErrorMsg,m_sCapMsg,MB_ICONASTERISK | MB_OK);
	}
}

BOOL CSetupCompanyFormView::areThereChildWindows(void)
{
	BOOL bWindowsOpen = FALSE;
  CString sDocName, sMsg, sWindows;
  CDocTemplate *pTemplate = NULL;
  CWinApp *pApp = AfxGetApp();

	// Loop through doc templates
  POSITION pos = pApp->GetFirstDocTemplatePosition();
  while(pos)
  {
		pTemplate = pApp->GetNextDocTemplate(pos);
    pTemplate->GetDocString(sDocName, CDocTemplate::docName);

    // Check for instances of this document (ignore database manager document)
    POSITION posDOC = pTemplate->GetFirstDocPosition();
    if (posDOC && sDocName.CompareNoCase(_T("Module601")) != 0)
    {
			CDocument* pDoc = (CDocument*)pTemplate->GetNextDoc(posDOC);
      sWindows += _T(" - ") + pDoc->GetTitle() + _T("\n");
      bWindowsOpen = TRUE;
    }
  }
  if( bWindowsOpen )
  {
		// Display list of open child windows
     sMsg.Format(_T("%s\n%s\n%s\n"),m_sMsgChildWindows1,sWindows,m_sMsgChildWindows2);
     ::MessageBox(this->GetSafeHwnd(),sMsg,m_sCapMsg,MB_ICONEXCLAMATION | MB_OK);
     return TRUE;
  }
  return FALSE;
}

void CSetupCompanyFormView::moveWndOnAuthentication(BOOL move)
{
	if (move)
	{
		setResize(&m_wndBtnCheckConnection,m_rectCheckConnectionOrigin.left,m_rectCheckConnectionOrigin.top + 90,m_rectCheckConnectionOrigin.Width(),m_rectCheckConnectionOrigin.Height());
		setResize(&m_wndBtnSetDB,m_rectSetDBOrigin.left,m_rectSetDBOrigin.top + 90,m_rectSetDBOrigin.Width(),m_rectSetDBOrigin.Height());
		setResize(&m_wndBtnSearchLocal,m_rectSearchLocalOrigin.left,m_rectSearchLocalOrigin.top + 90,m_rectSearchLocalOrigin.Width(),m_rectSearchLocalOrigin.Height());
		setResize(&m_wndBtnSearchServer,m_rectSearchServerOrigin.left,m_rectSearchServerOrigin.top + 90,m_rectSearchServerOrigin.Width(),m_rectSearchServerOrigin.Height());
		setResize(&m_wndCBoxServer,m_rectCBoxServerOrigin.left,m_rectCBoxServerOrigin.top + 90,m_rectCBoxServerOrigin.Width(),m_rectCBoxServerOrigin.Height());
		setResize(&m_wndLbl3,m_rectLbl3Origin.left,m_rectLbl3Origin.top + 90,m_rectLbl3Origin.Width(),m_rectLbl3Origin.Height());
		// lagt till f�r databas-profil
		//setResize(&m_wndBtnCreateDBProfile,m_rectBtnCreateDBProfile.left,m_rectBtnCreateDBProfile.top + 90,m_rectBtnCreateDBProfile.Width(),m_rectBtnCreateDBProfile.Height());

		// Show Userlogin and Password on "Server authentication"; 081014 p�d
		m_wndLbl4.ShowWindow( SW_NORMAL );
		m_wndLbl5.ShowWindow( SW_NORMAL );
		m_wndUserLogin.ShowWindow( SW_NORMAL );
		m_wndUserLoginPsw.ShowWindow( SW_NORMAL );
		// Set tab-order on windows; 081014 p�d
		::SetWindowPos(m_wndCBoxServerType,HWND_TOP,0,0,0,0,SWP_NOSIZE | SWP_NOMOVE );
		::SetWindowPos(m_wndCBoxAuthentication,m_wndCBoxServerType,0,0,0,0,SWP_NOSIZE | SWP_NOMOVE );
		::SetWindowPos(m_wndUserLogin,m_wndCBoxAuthentication,0,0,0,0,SWP_NOSIZE | SWP_NOMOVE );
		::SetWindowPos(m_wndUserLoginPsw,m_wndUserLogin,0,0,0,0,SWP_NOSIZE | SWP_NOMOVE );
		::SetWindowPos(m_wndCBoxServer,m_wndUserLoginPsw,0,0,0,0,SWP_NOSIZE | SWP_NOMOVE );
		::SetWindowPos(m_wndBtnSearchLocal,m_wndCBoxServer,0,0,0,0,SWP_NOSIZE | SWP_NOMOVE );
		::SetWindowPos(m_wndBtnSearchServer,m_wndBtnSearchLocal,0,0,0,0,SWP_NOSIZE | SWP_NOMOVE );
		::SetWindowPos(m_wndBtnSetDB,m_wndBtnSearchServer,0,0,0,0,SWP_NOSIZE | SWP_NOMOVE );
		::SetWindowPos(m_wndBtnCheckConnection,m_wndBtnSetDB,0,0,0,0,SWP_NOSIZE | SWP_NOMOVE );
		//::SetWindowPos(m_wndBtnCreateDBProfile,m_wndBtnCheckConnection,0,0,0,0,SWP_NOSIZE | SWP_NOMOVE );
	}
	else
	{
		setResize(&m_wndBtnCheckConnection,m_rectCheckConnectionOrigin.left,m_rectCheckConnectionOrigin.top,m_rectCheckConnectionOrigin.Width(),m_rectCheckConnectionOrigin.Height());
		setResize(&m_wndBtnSetDB,m_rectSetDBOrigin.left,m_rectSetDBOrigin.top,m_rectSetDBOrigin.Width(),m_rectSetDBOrigin.Height());
		setResize(&m_wndBtnSearchLocal,m_rectSearchLocalOrigin.left,m_rectSearchLocalOrigin.top,m_rectSearchLocalOrigin.Width(),m_rectSearchLocalOrigin.Height());
		setResize(&m_wndBtnSearchServer,m_rectSearchServerOrigin.left,m_rectSearchServerOrigin.top,m_rectSearchServerOrigin.Width(),m_rectSearchServerOrigin.Height());
		setResize(&m_wndCBoxServer,m_rectCBoxServerOrigin.left,m_rectCBoxServerOrigin.top,m_rectCBoxServerOrigin.Width(),m_rectCBoxServerOrigin.Height());
		setResize(&m_wndLbl3,m_rectLbl3Origin.left,m_rectLbl3Origin.top,m_rectLbl3Origin.Width(),m_rectLbl3Origin.Height());
		// lagt till f�r databas-profil
		//setResize(&m_wndBtnCreateDBProfile,m_rectBtnCreateDBProfile.left,m_rectBtnCreateDBProfile.top,m_rectBtnCreateDBProfile.Width(),m_rectBtnCreateDBProfile.Height());
	
		// Set tab-order on windows; 081014 p�d
		::SetWindowPos(m_wndCBoxServerType,HWND_TOP,0,0,0,0,SWP_NOSIZE | SWP_NOMOVE );
		::SetWindowPos(m_wndCBoxAuthentication,m_wndCBoxServerType,0,0,0,0,SWP_NOSIZE | SWP_NOMOVE );
		::SetWindowPos(m_wndCBoxServer,m_wndCBoxAuthentication,0,0,0,0,SWP_NOSIZE | SWP_NOMOVE );
		::SetWindowPos(m_wndBtnSearchLocal,m_wndCBoxServer,0,0,0,0,SWP_NOSIZE | SWP_NOMOVE );
		::SetWindowPos(m_wndBtnSearchServer,m_wndBtnSearchLocal,0,0,0,0,SWP_NOSIZE | SWP_NOMOVE );
		::SetWindowPos(m_wndBtnSetDB,m_wndBtnSearchServer,0,0,0,0,SWP_NOSIZE | SWP_NOMOVE );
		::SetWindowPos(m_wndBtnCheckConnection,m_wndBtnSetDB,0,0,0,0,SWP_NOSIZE | SWP_NOMOVE );
		//::SetWindowPos(m_wndBtnCreateDBProfile,m_wndBtnCheckConnection,0,0,0,0,SWP_NOSIZE | SWP_NOMOVE );

		// Hide Userlogin and Password on "Windows authentication"; 081014 p�d
		m_wndLbl4.ShowWindow( SW_HIDE );
		m_wndLbl5.ShowWindow( SW_HIDE );
		m_wndUserLogin.ShowWindow( SW_HIDE );
		m_wndUserLoginPsw.ShowWindow( SW_HIDE );
	}
	m_wndLbl3.Invalidate();
	m_wndBtnSearchLocal.Invalidate();
	m_wndBtnSearchServer.Invalidate();
	m_wndBtnSetDB.Invalidate();
	m_wndBtnCheckConnection.Invalidate();
}

void CSetupCompanyFormView::OnCbnSelchangeCombo2()
{
	int nIndex = m_wndCBoxAuthentication.GetCurSel();
	if (nIndex != CB_ERR)
	{
		moveWndOnAuthentication(nIndex == 1);
		if (nIndex == 1)
		{
			// Set focus on User Login; 081014 p�d
			m_wndUserLogin.SetFocus();
		}	// if (nIndex == 1)
	}	// if (nIndex != CB_ERR)
}

void CSetupCompanyFormView::OnCbnSelchangeCombo1()
{
	int nIndex = m_wndCBoxServerType.GetCurSel();

	if (areThereChildWindows()) return;

	ADMININIDATABASES recAdminIni;
	CString sServerType;
	CString sAdminPath;
	if (nIndex != CB_ERR)
	{
		m_wndCBoxServerType.GetWindowText(sServerType);
		regSetStr(REG_ROOT,REG_KEY_DATABASE,REG_STR_DBSERVER,sServerType);

		if (nIndex < m_vecAdminIni.size())
		{
			recAdminIni = m_vecAdminIni[nIndex];
		}	// if (nIndex < m_vecAdminIni.size())
	}	// if (nIndex != CB_ERR)
}

void CSetupCompanyFormView::setServerTypesInCBox1(void)
{
}

void CSetupCompanyFormView::OnBnClickedButton1()
{
	CString sSelDB;

	if (!checkSettings()) return;

	if (areThereChildWindows()) return;

	CXTPReportRow *pRow = m_wndReport1.GetFocusedRow();
	if (pRow != NULL)
	{
		CPageTwoReportUserDataRec *pRec = (CPageTwoReportUserDataRec*)pRow->GetRecord();
		if (pRec)
		{
			sSelDB = pRec->getColumnText(0);
			// Set selected user database in registry; 081008 p�d
			WriteUserDBToRegistry(sSelDB);
			// Change name of active database; 081008 p�d

			// Send a ID_DO_SOMETHING_IN_SHELL message to the Shell
			// with ID_LPARAM_COMMAND1, telling Shell to run the
			// setServerInfoOnStatusBar() method; 081008 p�d
			AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,ID_DO_SOMETHING_IN_SHELL,ID_LPARAM_COMMAND1);
			if( m_dbConnectionData.conn->isConnected() )
			{
				m_wndProgressDlg.Create(CProgressDialog::IDD,this);
				if(m_wndProgressDlg.GetSafeHwnd())	m_wndProgressDlg.ShowWindow(SW_NORMAL);

				// make sure that all tables get created when switching database
				runDoDatabaseTablesInSuitesModules(sSelDB, m_pFrame->getStatusBar(), m_wndProgressDlg, m_sStatusBarCreateTable);

				// Check for alterications for slected table; 090525 p�d
				runDoAlterTablesInSuitesModules(sSelDB);

				if (m_wndProgressDlg.GetSafeHwnd()) m_wndProgressDlg.DestroyWindow();

				if (m_pFrame != NULL) m_pFrame->setStatusBarText(1,m_sStatusBarOK);
			}
		}
	}
}
// Local server(s); 081015 p�d
void CSetupCompanyFormView::OnBnClickedButton2()
{
	if (!checkSettings()) return;

	if (areThereChildWindows()) return;

	m_wndCBoxServer.ClearHistory();
	m_wndCBoxServer.ResetContent();
	::SetCursor(::LoadCursor(NULL,IDC_WAIT));
	if (!getDBServers(TRUE))
	{
		m_wndCBoxServer.ClearHistory();
		m_wndCBoxServer.LoadHistory(_T("ServerSettings"),_T("History"));
		// Also clear Report; 081030 p�d
		m_wndReport1.ResetContent();
		m_wndReport1.Populate();
		m_wndReport1.UpdateWindow();
		// Also, tell user; 081030 p�d	
		::MessageBox(this->GetSafeHwnd(),m_sNoDBServerMsg,m_sCapMsg,MB_ICONASTERISK | MB_OK);

	}	// if (!getDBServers(TRUE))
	else
		m_wndCBoxServer.ShowDropDown();
	::SetCursor(::LoadCursor(NULL,IDC_ARROW));
}
// Search for network server(s); 081015 p�d
void CSetupCompanyFormView::OnBnClickedButton3()
{
	if (!checkSettings()) return;

	if (areThereChildWindows()) return;

	if (m_pFrame != NULL) m_pFrame->setStatusBarText(1,m_sStatusBarText1);
	::SetCursor(::LoadCursor(NULL,IDC_WAIT));
	if (getDBServers(FALSE))
	{
		if (m_pFrame != NULL) m_pFrame->setStatusBarText(1,m_sStatusBarText2);
		::SetCursor(::LoadCursor(NULL,IDC_WAIT));
		// Also clear Report; 081030 p�d
		m_wndReport1.ResetContent();
		m_wndReport1.Populate();
		m_wndReport1.UpdateWindow();
		updateConnection();
	}
	::SetCursor(::LoadCursor(NULL,IDC_ARROW));
	if (m_pFrame != NULL) m_pFrame->setStatusBarText(1,m_sStatusBarOK);
}

void CSetupCompanyFormView::OnBnClickedButton4()
{
	if (!checkSettings()) return;
	if (areThereChildWindows()) return;

	::SetCursor(::LoadCursor(NULL,IDC_WAIT));
	// Set selected user database in registry; 081008 p�d
	WriteUserDBToRegistry(_T(""));
	m_bCheckProfile = FALSE;
	updateConnection();
	::SetCursor(::LoadCursor(NULL,IDC_ARROW));
}

void CSetupCompanyFormView::OnCbnSelchangeCombo3()
{
	if (areThereChildWindows()) 
	{
		// Reset selection of Server; 090319 p�d
		m_wndCBoxServer.SetCurSel(m_wndCBoxServer.FindString(0,m_sCurrentUserServer));
		return;
	}

	::SetCursor(::LoadCursor(NULL,IDC_WAIT));
	// Set selected user database in registry; 081008 p�d
	WriteUserDBToRegistry(_T(""));
	m_bCheckProfile = FALSE;
	updateConnection();
	::SetCursor(::LoadCursor(NULL,IDC_ARROW));
}

void CSetupCompanyFormView::OnReportChanged(NMHDR *pNotifyStruct, LRESULT *pResult)
{
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;

	// Automatically save changes made in report rows
	if( pItemNotify )
	{
		CXTPReportRecordItem *pItem = pItemNotify->pItem;
		if( pItem )
		{
			CXTPReportRecord *pRec = pItem->GetRecord();

			CHmsUserDBRec rec( ((CXTPReportRecordItemText*)pRec->GetItem(0))->GetValue(),
							   ((CXTPReportRecordItemText*)pRec->GetItem(1))->GetValue(),
							   ((CXTPReportRecordItemText*)pRec->GetItem(2))->GetValue() );
			vecHmsUserDBs vec;
			vec.push_back(rec);
			userdb_new_upd(m_dbConnectionData, vec);
		}
	}
}

void CSetupCompanyFormView::OnCreateDBProfile()
{
	CString sTmp = L"",sXML = L"";
	TCHAR szDBLocation[128];
	TCHAR szDBServer[128];
	TCHAR szDBUser[32];
	TCHAR szDBPsw[32];
	TCHAR szDSNName[32];
	TCHAR szDBSelected[32];
	TCHAR szDBClient[32];
	int nAutentication = -1;
	CString sFilter = L"";
	CString sFileName = L"";
	CString sLocation = L"";

	if (!checkSettings()) return;

//	if (areThereChildWindows()) return;

	// Save private profile data to admin.ini file; 060216 p�d
	GetAdminIniData(szDBServer,szDBLocation,szDBUser,szDBPsw,szDSNName,szDBClient,&nAutentication);
	// Set client foe SQLApi
	GetUserDBInRegistry(szDBSelected);

	sFilter.Format(_T("%s (*.xml)|*.xml|"),m_sFilterText);

	// Setup and suggest a filename to user
	sLocation = szDBLocation;
	sLocation.Replace(L"\\",L"_");
	sFileName.Format(L"%s_%s.xml",sLocation,szDBSelected);

	CFileDialog dlg( FALSE, L"xml",sFileName, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT |  OFN_EXPLORER ,sFilter, this);

	if (dlg.DoModal() == IDOK)
	{

		MSXML2::IXMLDOMDocumentPtr pDomDoc = NULL;
		
		CHECK(CoInitialize(NULL));

		// Create MSXML2 DOM Document
		pDomDoc.CreateInstance("Msxml2.DOMDocument.3.0");
		// Set parser in NON async mode
		pDomDoc->async	= VARIANT_FALSE;
		// Validate during parsing
		pDomDoc->validateOnParse = VARIANT_TRUE;


		sXML = TAG_DBPROF_FIRST_ROW;
		sXML += TAG_DBPROF_START;
		sTmp.Format(TAG_DBPROF_LOCATION,szDBLocation);
		sXML += sTmp;
		sTmp.Format(TAG_DBPROF_SELECTED,szDBSelected);
		sXML += sTmp;
		sTmp.Format(TAG_DBPROF_SERVER,szDBServer);
		sXML += sTmp;
		sTmp.Format(TAG_DBPROF_AUTENTICATION,nAutentication);
		sXML += sTmp;
		sTmp.Format(TAG_DBPROF_PSW,szDBPsw);
		sXML += sTmp;
		sTmp.Format(TAG_DBPROF_USERNAME,szDBUser);
		sXML += sTmp;
		sXML += TAG_DBPROF_END;

		if (pDomDoc->loadXML((LPCTSTR)sXML))
		{
			pDomDoc->save((LPCTSTR)dlg.GetPathName());
		}

		CoUninitialize();
	
	}

}

void CSetupCompanyFormView::OnImportDBProfile()
{
	CString sTmp = L"",sXML = L"";
	TCHAR szDBLocation[128];
	TCHAR szDBServer[128];
	TCHAR szDBUser[32];
	TCHAR szDBPsw[32];
	TCHAR szDSNName[32];
	TCHAR szDBSelected[32];
	TCHAR szDBClient[32];
	TCHAR szDBAutentication[32];
	int nAutentication = -1;
	CString sFilter = L"";

	if (!checkSettings()) return;

	if (areThereChildWindows()) return;

	// Save private profile data to admin.ini file; 060216 p�d
	GetAdminIniData(szDBServer,szDBLocation,szDBUser,szDBPsw,szDSNName,szDBClient,&nAutentication);
	// Set client foe SQLApi
	GetUserDBInRegistry(szDBSelected);

	sFilter.Format(_T("%s (*.xml)|*.xml|"),m_sFilterText);

	// Setup and suggest a filename to user

	CFileDialog dlg( TRUE, L"xml",NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT |  OFN_EXPLORER ,sFilter, this);

	if (dlg.DoModal() == IDOK)
	{
		this->UpdateWindow();

		MSXML2::IXMLDOMDocumentPtr pDomDoc = NULL;
		
		CHECK(CoInitialize(NULL));

		// Create MSXML2 DOM Document
		pDomDoc.CreateInstance("Msxml2.DOMDocument.3.0");
		// Set parser in NON async mode
		pDomDoc->async	= VARIANT_FALSE;
		// Validate during parsing
		pDomDoc->validateOnParse = VARIANT_TRUE;

		if (pDomDoc->load((LPCTSTR)dlg.GetPathName()))
		{
			CComBSTR bstrData;
			MSXML2::IXMLDOMNodePtr pNode = NULL;
			MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;
			if ((pNode = pRoot->selectSingleNode(TAG_DBPROF_LOCATION2) ) != NULL)
			{	
				pNode->get_text(&bstrData);
				_tcscpy(szDBLocation,_bstr_t(bstrData));
			}

			if ((pNode = pRoot->selectSingleNode(TAG_DBPROF_SELECTED2)) != NULL)
			{	
				pNode->get_text(&bstrData);
				_tcscpy(szDBSelected,_bstr_t(bstrData));
			}

			if ((pNode = pRoot->selectSingleNode(TAG_DBPROF_SERVER2)) != NULL)
			{	
				pNode->get_text(&bstrData);
				_tcscpy(szDBServer,_bstr_t(bstrData));
			}

			if ((pNode = pRoot->selectSingleNode(TAG_DBPROF_AUTENTICATION2)) != NULL)
			{	
				pNode->get_text(&bstrData);
				_tcscpy(szDBAutentication,_bstr_t(bstrData));
			}

			if ((pNode = pRoot->selectSingleNode(TAG_DBPROF_PSW2)) != NULL)
			{	
				pNode->get_text(&bstrData);
				_tcscpy(szDBPsw,_bstr_t(bstrData));
			}

			if ((pNode = pRoot->selectSingleNode(TAG_DBPROF_USERNAME2)) != NULL)
			{	
				pNode->get_text(&bstrData);
				_tcscpy(szDBUser,_bstr_t(bstrData));
			}

			CoUninitialize();

			WriteAdminIniData(szDBServer,szDBLocation,szDBUser,szDBPsw,szDSNName,szDBClient,_tstoi(szDBAutentication));
			WriteUserDBToRegistry(szDBSelected);

			m_bShowStatus_profile = FALSE;

			setIniData();

			m_bCheckProfile = TRUE;

			updateConnection();
		
			// Try to find row for selected userdatabase in profile; 2011-10-24 p�d
			BOOL bFound = FALSE;
			CPageTwoReportUserDataRec *pRec = NULL;
			CXTPReportRows *pRows = m_wndReport1.GetRows();
			if (pRows != NULL)
			{
				for (int i = 0; i < pRows->GetCount();i++)
				{
					pRec = (CPageTwoReportUserDataRec*)pRows->GetAt(i)->GetRecord();
					if (pRec != NULL)
					{
						if (pRec->getColumnText(0).CompareNoCase(szDBSelected) == 0)
						{
							m_wndReport1.SetFocusedRow(pRows->GetAt(i));
							bFound = TRUE;
							break;
						}
					}	// if (pRec != NULL)
				}	// for (int i = 0; i < pRows->GetCount();i++)

			}	// if (pRows != NULL)
			

			if (bFound)
				OnBnClickedButton1();

		}

		m_bShowStatus_profile = TRUE;
	}
}

int CSetupCompanyFormView::getNumOfItemsInReport(void)
{
	CXTPReportRows *pRows = m_wndReport1.GetRows();
	if (pRows != NULL) return pRows->GetCount();

	return 0;	// No rows, nothing; 081030 p�d
}


BOOL CSetupCompanyFormView::checkSettings(void)
{
	if (m_wndCBoxServerType.GetCurSel() == CB_ERR ||
		m_wndCBoxAuthentication.GetCurSel() == CB_ERR)
	{
		::MessageBox(this->GetSafeHwnd(),m_sCheckSettingsMsg,m_sCapMsg,MB_ICONASTERISK | MB_OK);
		return FALSE;
	}
	else
		return TRUE;
}


// MDIDataBaseFrame.cpp : implementation file
//

#include "stdafx.h"
#include "Resource.h"
#include "MDIDataBaseFrame.h"
#include "MySQLPageTwoFormView.h"

#include "ResLangFileReader.h"


static UINT indicators[] =
{
	ID_SEPARATOR,	          // status line indicator
	ID_SEPARATOR	          // status line indicator
};


/////////////////////////////////////////////////////////////////////////////
// CMDIFrameDoc

IMPLEMENT_DYNCREATE(CMDIFrameDoc, CDocument)

BEGIN_MESSAGE_MAP(CMDIFrameDoc, CDocument)
	//{{AFX_MSG_MAP(CMDIFrameDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMDIFrameDoc construction/destruction

CMDIFrameDoc::CMDIFrameDoc()
{
}

CMDIFrameDoc::~CMDIFrameDoc()
{
}

BOOL CMDIFrameDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}


/////////////////////////////////////////////////////////////////////////////
// CMDIFrameDoc serialization

void CMDIFrameDoc::Serialize(CArchive& ar)
{
}

/////////////////////////////////////////////////////////////////////////////
// CMDIFrameDoc diagnostics

#ifdef _DEBUG
void CMDIFrameDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CMDIFrameDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

// CMDIFrameDoc commands


/////////////////////////////////////////////////////////////////////////////
// CMDITableImportFrame

IMPLEMENT_DYNCREATE(CMDITableImportFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CMDITableImportFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CMDITableImportFrame)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_SETFOCUS()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CMDITableImportFrame::CMDITableImportFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_FORMICON);
}

CMDITableImportFrame::~CMDITableImportFrame()
{
}

void CMDITableImportFrame::OnDestroy(void)
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_TABLE_IMPORT_KEY);
	SavePlacement(this, csBuf);
	m_bFirstOpen = TRUE;
}

int CMDITableImportFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (m_hIcon)
	{
		SetIcon(m_hIcon,TRUE);
		SetIcon(m_hIcon,FALSE);
	}

	// Setup language filename; 051214 p�d
	CString sLangFN;
	sLangFN.Format(_T("%s%s%s%s"),getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);

	if (!m_wndStatusBar.Create(this) || !m_wndStatusBar.SetIndicators(indicators,sizeof(indicators)/sizeof(UINT)))
	{
		return -1;      // fail to create
	}

	if (m_wndStatusBar.GetSafeHwnd())
	{
		m_wndStatusBar.SetPaneWidth(0, 40);
		m_wndStatusBar.SetPaneStyle(0, SBPS_NOBORDERS);
		m_wndStatusBar.SetPaneStyle(1, SBPS_STRETCH);
		if (fileExists(sLangFN))
		{
			RLFReader xml;
			if (xml.Load(sLangFN))
			{
				m_wndStatusBar.SetPaneText(0,(xml.str(IDS_STRING143)));
				m_wndStatusBar.SetPaneText(1,(xml.str(IDS_STRING144)));
			}
			xml.clean();
		}
	}

	m_bFirstOpen = TRUE;
	return 0; // creation ok
}

// load the placement in OnShowWindow()
void CMDITableImportFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_TABLE_IMPORT_KEY);
		LoadPlacement(this, csBuf);
  }
}

void CMDITableImportFrame::OnSetFocus(CWnd*)
{
	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);

}

BOOL CMDITableImportFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

// CMDITableImportFrame diagnostics

#ifdef _DEBUG
void CMDITableImportFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CMDITableImportFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG


void CMDITableImportFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{

	lpMMI->ptMinTrackSize.x = MIN_X_SIZE_TABLE_IMPORT;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE_TABLE_IMPORT;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CMDITableImportFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

void CMDITableImportFrame::OnSize(UINT nType,int cx,int cy)
{
	CMDIChildWnd::OnSize(nType, cx, cy);
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CMDITableImportFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{

	CDocument *pDoc = GetActiveDocument();
	if (pDoc != NULL)
	{
		POSITION pos = pDoc->GetFirstViewPosition();
		while (pos != NULL)
		{
			CView *pView = pDoc->GetNextView(pos);
			pView->SendMessage(MSG_IN_SUITE,wParam);
		}	// while (pos != NULL)
	}	// if (pDoc != NULL)

	return 0L;
}

// MY METHODS


// CMDITableImportFrame message handlers


/////////////////////////////////////////////////////////////////////////////
// CMDISetupCompanyFrame

IMPLEMENT_DYNCREATE(CMDISetupCompanyFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CMDISetupCompanyFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CMDISetupCompanyFrame)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_CLOSE()
	ON_WM_PAINT()
	ON_WM_SETFOCUS()
	ON_WM_SYSCOMMAND()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CMDISetupCompanyFrame::CMDISetupCompanyFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_FORMICON);
	m_bAlreadySaidOkToClose = FALSE;
}

CMDISetupCompanyFrame::~CMDISetupCompanyFrame()
{
}

void CMDISetupCompanyFrame::OnDestroy(void)
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_SETUPCOMPANY_KEY);
	SavePlacement(this, csBuf);
	m_bFirstOpen = TRUE;
}

void CMDISetupCompanyFrame::OnClose(void)
{

	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);

	if (m_bAlreadySaidOkToClose)
		CMDIChildWnd::OnClose();
	else if (isOkToClose())
		CMDIChildWnd::OnClose();
}

void CMDISetupCompanyFrame::OnSysCommand(UINT nID,LPARAM lParam)
{
	if ((nID & 0xFFF0) == SC_CLOSE)
	{
		// Only if it's OK to Close; 081013 p�d
		if (isOkToClose())
		{
			m_bAlreadySaidOkToClose = TRUE;
			CMDIChildWnd::OnSysCommand(nID,lParam);
		}
	}
	else if ((nID & 0xFFF0) != SC_CLOSE)
		CMDIChildWnd::OnSysCommand(nID,lParam);
}

BOOL CMDISetupCompanyFrame::isOkToClose(void)
{
	TCHAR szDBSelected[127];
	int nNumOfItemsInReport = 0;	
	GetUserDBInRegistry(szDBSelected);

	// If there's no items in report, we can close anyway; 081030 p�d
	CSetupCompanyFormView *pView = (CSetupCompanyFormView*)getFormViewByID(IDD_FORMVIEW1);
	if (pView != NULL)
	{
		nNumOfItemsInReport = pView->getNumOfItemsInReport();
	}
	pView = NULL;

	if (_tcslen(szDBSelected) == 0 && nNumOfItemsInReport > 0)
	{
		::MessageBox(this->GetSafeHwnd(),(m_sSelectDatabaseMsg),(m_sMsgCap),MB_ICONINFORMATION | MB_OK);
		return FALSE;
	}

	return TRUE;
}

int CMDISetupCompanyFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (m_hIcon)
	{
		SetIcon(m_hIcon,TRUE);
		SetIcon(m_hIcon,FALSE);
	}

	// Setup language filename; 051214 p�d
	CString sLangFN;
	sLangFN.Format(_T("%s%s%s%s"),getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);

	if (!m_wndStatusBar.Create(this) || !m_wndStatusBar.SetIndicators(indicators,sizeof(indicators)/sizeof(UINT)))
	{
		return -1;      // fail to create
	}

	// Create and Load toolbar; 090107 p�d
	m_wndToolBar.CreateToolBar(WS_TABSTOP|WS_VISIBLE|WS_CHILD|CBRS_TOOLTIPS, this);
	m_wndToolBar.LoadToolBar(IDR_TOOLBAR1);
	m_wndToolBar.EnableDocking(xtpFlagAlignTop|xtpFlagStretched);

	if (m_wndStatusBar.GetSafeHwnd())
	{
		m_wndStatusBar.SetPaneWidth(0, 40);
		m_wndStatusBar.SetPaneStyle(0, SBPS_NOBORDERS);
		m_wndStatusBar.SetPaneStyle(1, SBPS_STRETCH);
		if (fileExists(sLangFN))
		{
			RLFReader xml;
			if (xml.Load(sLangFN))
			{
				m_wndStatusBar.SetPaneText(0,(xml.str(IDS_STRING143)));
				m_wndStatusBar.SetPaneText(1,(xml.str(IDS_STRING144)));

				m_sMsgCap = (xml.str(IDS_STRING132));
				m_sSelectDatabaseMsg.Format(_T("%s\n\n%s\n%s\n"),
					(xml.str(IDS_STRING162)),
					(xml.str(IDS_STRING163)),
					(xml.str(IDS_STRING164)));

				HICON hIcon = NULL;
				CXTPControl *pCtrl = NULL;
				CString sTBResFN = getToolBarResourceFN();

				// Setup commandbars and manues; 051114 p�d
				CXTPToolBar* pToolBar = &m_wndToolBar;
				if (pToolBar->IsBuiltIn())
				{
					if (pToolBar->GetType() != xtpBarTypeMenuBar)
					{

						UINT nBarID = pToolBar->GetBarID();
						pToolBar->LoadToolBar(nBarID, FALSE);
						CXTPControls *p = pToolBar->GetControls();

						// Setup icons on toolbars, using resource dll; 051208 p�d
						if (nBarID == IDR_TOOLBAR1)
						{		
							setToolbarBtnIcon(sTBResFN,p->GetAt(0),RES_TB_EXPORT,xml.str(IDS_STRING180));	//
							setToolbarBtnIcon(sTBResFN,p->GetAt(1),RES_TB_IMPORT,xml.str(IDS_STRING181));	//
					}	// if (nBarID == IDR_TOOLBAR1)
					}	// if (pToolBar->GetType() != xtpBarTypeMenuBar)
				}	// if (pToolBar->IsBuiltIn())


			}
			xml.clean();
		}
	}
	m_bFirstOpen = TRUE;
	return 0; // creation ok
}

// load the placement in OnShowWindow()
void CMDISetupCompanyFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_SETUPCOMPANY_KEY);
		LoadPlacement(this, csBuf);
  }
}

void CMDISetupCompanyFrame::OnSetFocus(CWnd*)
{
	TCHAR srv[256];
	TCHAR location[256];
	TCHAR user[256];
	TCHAR psw[256];
	TCHAR dsn[256];
	TCHAR client[256];
	int idx;

	GetAdminIniData(srv, location, user, psw, dsn, client, &idx);

	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM, location[0] ? TRUE : FALSE); // Add, remove only enabled when connected to server; 100120 Peter
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM, location[0] ? TRUE : FALSE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);

}

BOOL CMDISetupCompanyFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

// CMDISetupCompanyFrame diagnostics

#ifdef _DEBUG
void CMDISetupCompanyFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CMDISetupCompanyFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG


void CMDISetupCompanyFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{

	lpMMI->ptMinTrackSize.x = MIN_X_SIZE_SETUP_COMPANY;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE_SETUP_COMPANY;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CMDISetupCompanyFrame::OnPaint()
{
	
	CMDIChildWnd::OnPaint();
}

void CMDISetupCompanyFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

void CMDISetupCompanyFrame::OnSize(UINT nType,int cx,int cy)
{
	CMDIChildWnd::OnSize(nType, cx, cy);
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CMDISetupCompanyFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{

	CDocument *pDoc = GetActiveDocument();
	if (pDoc != NULL)
	{
		POSITION pos = pDoc->GetFirstViewPosition();
		while (pos != NULL)
		{
			CView *pView = pDoc->GetNextView(pos);
			pView->SendMessage(MSG_IN_SUITE,wParam);
		}	// while (pos != NULL)
	}	// if (pDoc != NULL)

	return 0L;
}

// MY METHODS


// CMDISetupCompanyFrame message handlers

/////////////////////////////////////////////////////////////////////////////
// CMDISetupUserFrame

IMPLEMENT_DYNCREATE(CMDISetupUserFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CMDISetupUserFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CMDISetupUserFrame)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_PAINT()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CMDISetupUserFrame::CMDISetupUserFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_FORMICON);
}

CMDISetupUserFrame::~CMDISetupUserFrame()
{
}

void CMDISetupUserFrame::OnDestroy(void)
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_SETUPUSER_KEY);
	SavePlacement(this, csBuf);
	m_bFirstOpen = TRUE;
}

int CMDISetupUserFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (m_hIcon)
	{
		SetIcon(m_hIcon,TRUE);
		SetIcon(m_hIcon,FALSE);
	}

	// Setup language filename; 051214 p�d
	CString sLangFN;
	sLangFN.Format(_T("%s%s%s%s"),getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);

	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	m_bFirstOpen = TRUE;
	return 0; // creation ok
}

// load the placement in OnShowWindow()
void CMDISetupUserFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_SETUPUSER_KEY);
		LoadPlacement(this, csBuf);
  }
}
BOOL CMDISetupUserFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}


// CMDISetupUserFrame diagnostics

#ifdef _DEBUG
void CMDISetupUserFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CMDISetupUserFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG


void CMDISetupUserFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{

	lpMMI->ptMinTrackSize.x = MIN_X_SIZE_TABLE_IMPORT;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE_TABLE_IMPORT;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CMDISetupUserFrame::OnPaint()
{

	CMDIChildWnd::OnPaint();
}

void CMDISetupUserFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

void CMDISetupUserFrame::OnSize(UINT nType,int cx,int cy)
{
	CMDIChildWnd::OnSize(nType, cx, cy);
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CMDISetupUserFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{

	CDocument *pDoc = GetActiveDocument();
	if (pDoc != NULL)
	{
		POSITION pos = pDoc->GetFirstViewPosition();
		while (pos != NULL)
		{
			CView *pView = pDoc->GetNextView(pos);
			pView->SendMessage(MSG_IN_SUITE,wParam);
		}	// while (pos != NULL)
	}	// if (pDoc != NULL)

	return 0L;
}

// MY METHODS


// CMDISetupUserFrame message handlers




/////////////////////////////////////////////////////////////////////////////
// CMDISetupUserPermissionsFrame

IMPLEMENT_DYNCREATE(CMDISetupUserPermissionsFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CMDISetupUserPermissionsFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CMDISetupUserPermissionsFrame)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_PAINT()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CMDISetupUserPermissionsFrame::CMDISetupUserPermissionsFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_FORMICON);
}

CMDISetupUserPermissionsFrame::~CMDISetupUserPermissionsFrame()
{
}

void CMDISetupUserPermissionsFrame::OnDestroy(void)
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_SETUPUSERPERMISSIONS_KEY);
	SavePlacement(this, csBuf);
	m_bFirstOpen = TRUE;
}

int CMDISetupUserPermissionsFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (m_hIcon)
	{
		SetIcon(m_hIcon,TRUE);
		SetIcon(m_hIcon,FALSE);
	}

	// Setup language filename; 051214 p�d
	CString sLangFN;
	sLangFN.Format(_T("%s%s%s%s"),getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);

	m_bFirstOpen = TRUE;
	return 0; // creation ok
}

// load the placement in OnShowWindow()
void CMDISetupUserPermissionsFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_SETUPUSERPERMISSIONS_KEY);
		LoadPlacement(this, csBuf);
  }
}
BOOL CMDISetupUserPermissionsFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CMDISetupUserPermissionsFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{

	lpMMI->ptMinTrackSize.x = MIN_X_SIZE_TABLE_IMPORT;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE_TABLE_IMPORT;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CMDISetupUserPermissionsFrame::OnPaint()
{
	CMDIChildWnd::OnPaint();
}

void CMDISetupUserPermissionsFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

void CMDISetupUserPermissionsFrame::OnSize(UINT nType,int cx,int cy)
{
	CMDIChildWnd::OnSize(nType, cx, cy);
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CMDISetupUserPermissionsFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{

	CDocument *pDoc = GetActiveDocument();
	if (pDoc != NULL)
	{
		POSITION pos = pDoc->GetFirstViewPosition();
		while (pos != NULL)
		{
			CView *pView = pDoc->GetNextView(pos);
			pView->SendMessage(MSG_IN_SUITE,wParam);
		}	// while (pos != NULL)
	}	// if (pDoc != NULL)

	return 0L;
}

// ProgressDialog.cpp : implementation file
//

#include "stdafx.h"
#include "UMDataBase.h"
#include "ProgressDialog.h"

#include "ResLangFileReader.h"

// CProgressDialog dialog

IMPLEMENT_DYNAMIC(CProgressDialog, CDialog)

BEGIN_MESSAGE_MAP(CProgressDialog, CDialog)
END_MESSAGE_MAP()


CProgressDialog::CProgressDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CProgressDialog::IDD, pParent)
{
	m_bInitialized = FALSE;
}

CProgressDialog::~CProgressDialog()
{
}

void CProgressDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDC_PROGRESS1, m_wndProgressCtrl);
	//}}AFX_DATA_MAP

}

BOOL CProgressDialog::OnInitDialog()
{
	CDialog::OnInitDialog();

	if (!m_bInitialized)
	{

		// Setup language filename; 051214 p�d
		CString sLangFN;
		sLangFN.Format(_T("%s%s%s%s"),getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);
		if (fileExists(sLangFN))
		{
			RLFReader xml;
			if (xml.Load(sLangFN))
			{
				SetWindowText((xml.str(IDS_STRING168)));
			}
			xml.clean();
		}

		// initialize progress control.
		m_wndProgressCtrl.SetRange (0, 100);
		m_wndProgressCtrl.SetPos (0);
		m_wndProgressCtrl.SetStep (10);

		m_bInitialized = TRUE;
	}	// if (!m_bInitialized)

	return FALSE;
	              
}

// CProgressDialog message handlers

/*
		classes for records used in the specie table; 060210 p�d
*/

#if !defined(AFX_HMSADMINRECORDS_H)
#define AFX_HMSADMINRECORDS_H

#include "StdAfx.h"

//////////////////////////////////////////////////////////////////////////////
//	CHmsUserDBRec; transaction class for data in hms_user_types table; 060216 p�d

class CHmsUserDBRec
{
//private:
	CString m_sDBName;								// Name of database in server
	CString m_sUserName;							// User name of datbase (Company or whatever)
	CString m_sNotes;									// Notes 
	CString m_sCreatedBy;							// Name of creator
	CString m_sCreated;								// Date/Time; created/updated
public:
	CHmsUserDBRec(void);
	CHmsUserDBRec(LPCTSTR db_name,
						    LPCTSTR user_name,
							  LPCTSTR notes,
						    LPCTSTR created_by = _T(""),
							  LPCTSTR created = _T(""));

	CHmsUserDBRec(const CHmsUserDBRec &c)
	{
		*this = c;
	}
	// Public methods

	CString getDBName(void)				{ return m_sDBName;	}
	CString getUserName(void)			{ return m_sUserName; }
	CString getNotes(void)				{ return m_sNotes;	}
	CString getCreatedBy(void)		{ return m_sCreatedBy;	}
	CString getCreated(void)			{ return m_sCreated; }
};

typedef std::vector<CHmsUserDBRec> vecHmsUserDBs;


//////////////////////////////////////////////////////////////////////////////
// CReportSpeciesRec; this class is used to add data to a CXTPReportControl; 060210 p�d

#endif
#pragma once

#include "Resource.h"

// CNewDBDlg dialog

class CNewDBDlg : public CDialog
{
	DECLARE_DYNAMIC(CNewDBDlg)

public:
	CNewDBDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CNewDBDlg();

	BOOL OnInitDialog();

	CString GetName() { m_strName.Remove('.'); return m_strName; }

// Dialog Data
	enum { IDD = IDD_DIALOG3 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	void OnOK();

	CEdit m_name;
	CString m_strName;

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnEnChangeEdit1();
};

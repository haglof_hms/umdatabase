#pragma once

///////////////////////////////////////////////////////////////////////////////////////////
// CMDIFrameDoc

class CMDIFrameDoc : public CDocument
{
protected: // create from serialization only
	CMDIFrameDoc();
	DECLARE_DYNCREATE(CMDIFrameDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMDIFrameDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMDIFrameDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CMDIFrameDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

///////////////////////////////////////////////////////////////////////////////////////////

#define CChildFrameBase CXTPFrameWndBase<CMDIChildWnd>

///////////////////////////////////////////////////////////////////////////////////////////
// CMDITableImportFrame frame

class CMDITableImportFrame : public CChildFrameBase //CMDIChildWnd
{
	DECLARE_DYNCREATE(CMDITableImportFrame)

//private:
	CXTPDockingPaneManager m_paneManager;

	CXTPStatusBar m_wndStatusBar;

protected:


	CXTPDockingPaneManager* GetDockingPaneManager() 
	{	
		return &m_paneManager; 
	}
	void SetDockingPaneTheme(XTPDockingPanePaintTheme theme)
	{
		GetDockingPaneManager()->SetTheme(theme);
	}

	BOOL m_bFirstOpen;

	WINDOWPLACEMENT m_wpPlacement;
	HICON m_hIcon;
public:

	CMDITableImportFrame();           // protected constructor used by dynamic creation
	virtual ~CMDITableImportFrame();

	static XTPDockingPanePaintTheme m_themeCurrent;

// Overrides
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

	inline void setStatusBarText(int pane_id,LPCTSTR text)	{ m_wndStatusBar.SetPaneText(pane_id,text);	}
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	//{{AFX_MSG(CMDIStandEntryFormFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnDestroy(void);
	afx_msg LRESULT OnMessageFromShell( WPARAM wParam, LPARAM lParam );
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd);
	afx_msg	void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnSetFocus(CWnd*);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


///////////////////////////////////////////////////////////////////////////////////////////
// CMDISetupCompanyFrame frame

class CMDISetupCompanyFrame : public CChildFrameBase //CMDIChildWnd
{
	DECLARE_DYNCREATE(CMDISetupCompanyFrame)

//private:
	CXTPDockingPaneManager m_paneManager;

	CXTPStatusBar m_wndStatusBar;

	CString m_sMsgCap;
	CString m_sSelectDatabaseMsg;

	BOOL m_bAlreadySaidOkToClose;

	CXTPToolBar m_wndToolBar;

protected:

	void setupToolBarIcons(void);

	CXTPDockingPaneManager* GetDockingPaneManager() 
	{	
		return &m_paneManager; 
	}
	void SetDockingPaneTheme(XTPDockingPanePaintTheme theme)
	{
		GetDockingPaneManager()->SetTheme(theme);
	}

	BOOL m_bFirstOpen;

	WINDOWPLACEMENT m_wpPlacement;
	HICON m_hIcon;

	BOOL isOkToClose(void);
public:

	CMDISetupCompanyFrame();           // protected constructor used by dynamic creation
	virtual ~CMDISetupCompanyFrame();

	static XTPDockingPanePaintTheme m_themeCurrent;

// Overrides
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

	inline CXTPStatusBar &getStatusBar(void)	{ return m_wndStatusBar; }
	inline void setStatusBarText(int pane_id,LPCTSTR text)	{ m_wndStatusBar.SetPaneText(pane_id,text);	}
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	//{{AFX_MSG(CMDIStandEntryFormFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnDestroy(void);
	afx_msg void OnClose(void);
	afx_msg void OnPaint(void);
	afx_msg void OnSysCommand(UINT nID,LPARAM lParam);
	afx_msg LRESULT OnMessageFromShell( WPARAM wParam, LPARAM lParam );
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd);
	afx_msg	void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnSetFocus(CWnd*);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};



///////////////////////////////////////////////////////////////////////////////////////////
// CMDISetupUserFrame frame

class CMDISetupUserFrame : public CChildFrameBase //CMDIChildWnd
{
	DECLARE_DYNCREATE(CMDISetupUserFrame)

//private:
	CXTPDockingPaneManager m_paneManager;

protected:

	void setupToolBarIcons(void);

	CXTPDockingPaneManager* GetDockingPaneManager() 
	{	
		return &m_paneManager; 
	}
	void SetDockingPaneTheme(XTPDockingPanePaintTheme theme)
	{
		GetDockingPaneManager()->SetTheme(theme);
	}

	BOOL m_bFirstOpen;

	WINDOWPLACEMENT m_wpPlacement;
	HICON m_hIcon;
public:

	CMDISetupUserFrame();           // protected constructor used by dynamic creation
	virtual ~CMDISetupUserFrame();

	static XTPDockingPanePaintTheme m_themeCurrent;

// Overrides
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	//{{AFX_MSG(CMDIStandEntryFormFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnDestroy(void);
	afx_msg void OnPaint(void);
	afx_msg LRESULT OnMessageFromShell( WPARAM wParam, LPARAM lParam );
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd);
	afx_msg	void OnShowWindow(BOOL bShow, UINT nStatus);

	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};



///////////////////////////////////////////////////////////////////////////////////////////
// CMDISetupUserPermissionsFrame frame

class CMDISetupUserPermissionsFrame : public CChildFrameBase //CMDIChildWnd
{
	DECLARE_DYNCREATE(CMDISetupUserPermissionsFrame)

//private:
	CXTPDockingPaneManager m_paneManager;

protected:

	void setupToolBarIcons(void);

	CXTPDockingPaneManager* GetDockingPaneManager() 
	{	
		return &m_paneManager; 
	}
	void SetDockingPaneTheme(XTPDockingPanePaintTheme theme)
	{
		GetDockingPaneManager()->SetTheme(theme);
	}

	BOOL m_bFirstOpen;

	WINDOWPLACEMENT m_wpPlacement;
	HICON m_hIcon;
public:

	CMDISetupUserPermissionsFrame();           // protected constructor used by dynamic creation
	virtual ~CMDISetupUserPermissionsFrame();

	static XTPDockingPanePaintTheme m_themeCurrent;

// Overrides
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	//{{AFX_MSG(CMDIStandEntryFormFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnDestroy(void);
	afx_msg void OnPaint(void);
	afx_msg LRESULT OnMessageFromShell( WPARAM wParam, LPARAM lParam );
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd);
	afx_msg	void OnShowWindow(BOOL bShow, UINT nStatus);

	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

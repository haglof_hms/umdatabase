/*
			FormView; select company (database) to be used
*/

#pragma once

#include "DBHandler.h"
#include "Resource.h"

class CTableImportDataRec : public CXTPReportRecord
{
	//private:
protected:
	class CTextItem : public CXTPReportRecordItemText
	{
	//private:
		CString m_sText;
	public:
		CTextItem(CString sValue) : CXTPReportRecordItemText(sValue)
		{
			m_sText = sValue;
			SetBold(FALSE);
		}

		void setBold(BOOL bold)
		{
			SetBold(bold);
		}
	};
	//////////////////////////////////////////////////////////////////////////
	// Customized record item, used for displaying checkboxes.
	class CCheckItem : public CXTPReportRecordItem
	{
	public:
		// Constructs record item with the initial checkbox value.
		CCheckItem(BOOL bCheck)
		{
			HasCheckbox(TRUE);
			SetChecked(bCheck);
		}

		virtual BOOL getChecked(void)
		{
			return IsChecked()? TRUE: FALSE;
		}

		virtual void setChecked(BOOL bCheck)
		{
			SetChecked(bCheck);
		}
	};

public:
	CTableImportDataRec(void)
	{
		m_nIndex = -1;
		AddItem(new CCheckItem(FALSE));
		AddItem(new CTextItem(_T("")));
	}

	CTableImportDataRec(BOOL set_cb,LPCTSTR text, int nIndex)	 
	{
		AddItem(new CCheckItem(set_cb));
		AddItem(new CTextItem((text)));
		m_nIndex = nIndex;
	}

	BOOL getColumnCheck(int item)
	{
		return ((CCheckItem*)GetItem(item))->getChecked();
	}

	void setColumnCheck(int item,BOOL bChecked)
	{
		((CCheckItem*)GetItem(item))->setChecked(bChecked);
	}

	void setColumnBold(int item,BOOL bold)
	{
		((CTextItem*)GetItem(item))->setBold(bold);
	}

	int getColumnIndex()	// get the internal index set on creation
	{
		return m_nIndex;
	}
};

typedef struct _table_struct
{
	CString m_sText;
	CString m_sTable;
	CString m_sExtraTable;
	CXTPReportRecord *m_pRecord;
	_table_struct(void)
	{
		m_sText.Empty();
		m_sTable.Empty();
		m_sExtraTable.Empty();
		m_pRecord = NULL;
	}
	_table_struct(LPCTSTR text,LPCTSTR table,LPCTSTR extra_table = _T(""))
	{
		m_sText = (text);
		m_sTable = table;
		m_sExtraTable = (extra_table);
		m_pRecord = NULL;
	}

} TABLE_STRUCT;

typedef std::vector<TABLE_STRUCT> vecTableStruct;
// CTableImportFormView form view

class CTableImportFormView : public CXTResizeFormView ,public CDBHandleAdmin
{
	DECLARE_DYNCREATE(CTableImportFormView)

// private:
	BOOL m_bInitialized;
	CString	m_sLangAbrev;
	CString m_sLangFN;

	CString m_sMsgCap;
	CString m_sActiveDBMsg;
	CString m_sPropContactMsg;
	CString m_sContactCategoryMsg;
	CString m_sDoImportMsg;
	CString m_sImportFrom;
	CString m_sSBarOKMsg;
	CString m_sImportCompletedMsg;

	CXTResizeGroupBox m_wndGroup1;

	CMyExtStatic m_wndLbl1;
	CMyExtStatic m_wndLbl2;

	CComboBox m_wndCBox1;
	CComboBox m_wndCBox2;

	CButton m_wndBtnCheckAll;
	CButton m_wndBtnUnCheckAll;
	CButton m_wndBtnDoImport;
	// My data members
	CMyReportCtrl m_wndReport;
	CImageList m_ilIcons;

	CStringArray m_sarrDBInCBox2;
	vecTableStruct m_vecTableStruct;

	CString m_sFromDB;
	CString m_sToDB;
	CMDITableImportFrame *m_pFrame;
	BOOL m_bIsPropAndContactSelected;
	BOOL m_bIsContactAndCategoriesSelected;

	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;

protected:
	CTableImportFormView();           // protected constructor used by dynamic creation
	virtual ~CTableImportFormView();

	CString m_sMsgChildWindows1;
	CString m_sMsgChildWindows2;

	BOOL areThereChildWindows(void);

	// My data members
	// My methods
	BOOL setupData(void);

	BOOL isTableSelected(void);

	BOOL isPropAndContactsSelected(void);
	BOOL isContactsAndCategoriesSelected(void);

	BOOL scriptPricelists(void);
	BOOL scriptCostTemplates(void);
	BOOL scriptTmplTemplates(void);
	BOOL scriptPostNumbers(void);
	BOOL scriptCMP(void);
	BOOL scriptSpecies(void);
	BOOL scriptProperties(void);
	BOOL scriptContacts(void);
	BOOL scriptCategories(void);
	BOOL scriptElvTemplates(void);
	BOOL scriptElvDocuments(void);
	BOOL scriptEstiTraktTypes(void);
	BOOL scriptEstiOrigin(void);
	BOOL scriptEstiInpdata(void);
	BOOL scriptEstiCutclas(void);
	BOOL scriptEstiHgtcls(void);
	BOOL scriptDynamicStatus(void);

	BOOL copyTable(LPCTSTR szTbl);
public:
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSetupStandReportView)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

	enum { IDD = IDD_FORMVIEW };
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

	//{{AFX_MSG(CMDIDBFormFrame)
	afx_msg LRESULT OnSuiteMessage(WPARAM,LPARAM);
	afx_msg void OnDestory(void);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	afx_msg void OnReportItemClick(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButton2();
	afx_msg void OnBnClickedButton3();
	afx_msg void OnCbnSelchangeCombo1();
	afx_msg void OnCbnSelchangeCombo2();
};



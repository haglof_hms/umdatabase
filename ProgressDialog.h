#pragma once

#include "Resource.h"

// CProgressDialog dialog

class CProgressDialog : public CDialog
{
	DECLARE_DYNAMIC(CProgressDialog)

	BOOL m_bInitialized;

	CProgressCtrl m_wndProgressCtrl;
public:
	CProgressDialog(CWnd* pParent = NULL);   // standard constructor
	virtual ~CProgressDialog();

// Dialog Data
	enum { IDD = IDD_DIALOG2 };

	inline void stepProgress(int step)
	{
		if (m_wndProgressCtrl.GetSafeHwnd() != NULL)
		{
			m_wndProgressCtrl.SetPos(step);
		}
	}

	inline void setProgressRange(int range)
	{
		if (m_wndProgressCtrl.GetSafeHwnd() != NULL)
		{
			m_wndProgressCtrl.SetRange(0,range);
		}
	}


protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();

	DECLARE_MESSAGE_MAP()
};

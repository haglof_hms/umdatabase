#include "StdAfx.h"
#include "DBHandler.h"


CDBAdmin::CDBAdmin(void) 
	: CDBBaseClass_SQLApi()
{
}

CDBAdmin::CDBAdmin(SAClient_t client,LPCTSTR db_name,LPCTSTR user_name,LPCTSTR psw)
	: CDBBaseClass_SQLApi(client,db_name,user_name,psw)
{
}

CDBAdmin::CDBAdmin(DB_CONNECTION_DATA &db_connection)
	: CDBBaseClass_SQLApi(db_connection,2)
{
}

BOOL CDBAdmin::userdb_Exists(CHmsUserDBRec &rec)
{
	CString sSQL;
	sSQL.Format(_T("select * from %s where db_name='%s'"),
						HMS_ADMINISTRATOR_TABLE,
						rec.getDBName());

	return CDBBaseClass_SQLApi::exists(sSQL);
}

BOOL CDBAdmin::database_Exists(CHmsUserDBRec &rec)
{
	return CDBBaseClass_SQLApi::db_exists((rec.getDBName()));
}

BOOL CDBAdmin::table_Exists(CHmsUserDBRec &rec,LPCTSTR table_name)
{
	return CDBBaseClass_SQLApi::table_exists((rec.getDBName()),table_name);
}

BOOL CDBAdmin::userdb_NewUpd(CHmsUserDBRec &rec)
{
	CString sSQL;
	try
	{
		// Check if post already exists; 060210 p�d
		if (!userdb_Exists(rec))
		{

			sSQL.Format(_T("insert into %s (db_name,user_name,notes,created_by) values(:1,:2,:3,:4)"),
										HMS_ADMINISTRATOR_TABLE);

			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsString()	= rec.getDBName();
			m_saCommand.Param(2).setAsString()	= rec.getUserName();
			m_saCommand.Param(3).setAsString()	= rec.getNotes();
			m_saCommand.Param(4).setAsString()	= getUserName();
			m_saCommand.Execute();

		} // if (!exists(rec.getSpcID())
		else	// Update
		{
			sSQL.Format(_T("update %s set user_name=:1,notes=:2,created_by=:3 where db_name =:4"),
										HMS_ADMINISTRATOR_TABLE);

			m_saCommand.setCommandText((SAString)sSQL);

			m_saCommand.Param(1).setAsString()	= rec.getUserName();
			m_saCommand.Param(2).setAsString()	= rec.getNotes();
			m_saCommand.Param(3).setAsString()	= getUserName();
			m_saCommand.Param(4).setAsString()	= rec.getDBName();
			m_saCommand.Execute();
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CDBAdmin::userdb_Remove(CHmsUserDBRec &rec)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("delete from %s where db_name=:1"),
							HMS_ADMINISTRATOR_TABLE);

		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsString()	= rec.getDBName();
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

// Get all items in userprofile table
BOOL CDBAdmin::userdb_Get(vecHmsUserDBs &vec)
{
	CString sSQL;
	try
	{
		vec.clear();
		
		sSQL.Format(_T("select * from %s"),HMS_ADMINISTRATOR_TABLE);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
		
			vec.push_back(CHmsUserDBRec((LPCTSTR)m_saCommand.Field(1).asString(),
																	(LPCTSTR)m_saCommand.Field(2).asString(),
																	(LPCTSTR)m_saCommand.Field(3).asString() ));

		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}


	return TRUE;
}

//------------------------------------------------------------------------------------
// Create user database method; 060302 p�d
BOOL CDBAdmin::create_UserDatabase(CHmsUserDBRec &rec)
{

	CString sSQL;
	try
	{
		if (!database_Exists(rec))
		{
			sSQL.Format(_T("CREATE DATABASE \"%s\""),rec.getDBName());
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Execute();
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

// Delete user database method; 060302 p�d
BOOL CDBAdmin::delete_UserDatabase(CHmsUserDBRec &rec)
{
	CString sSQL;
	AfxGetApp()->BeginWaitCursor();
	try
	{
		if (database_Exists(rec))
		{
			// Works both for MySQL and SQL Server; 060313 p�d
			sSQL.Format(_T("drop database \"%s\";"),rec.getDBName());
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Execute();
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}
	AfxGetApp()->EndWaitCursor();

	return TRUE;
}


//////////////////////////////////////////////////////////////////////////////////
// CDBHandleAdmin; methods handling ALL transaction, based on methods in CDBAdmin
// clas. E.g. getHmsUserTypes, Insert or Update etc; 060228 p�d
// I'll try to add this class as friend

CDBHandleAdmin::CDBHandleAdmin(void)
{
}


//-------------------------------------------------------------------------
// UserDBs
//-------------------------------------------------------------------------
BOOL CDBHandleAdmin::userdb_get(DB_CONNECTION_DATA &db_conn)
{
	BOOL bReturn = FALSE;
	CDBAdmin *db = new CDBAdmin(db_conn);
	if (db)
	{
		bReturn =	db->userdb_Get(m_vecHmsUserDBs);
		delete db;
	}
	return bReturn;
}

vecHmsUserDBs &CDBHandleAdmin::userdb_list(void) 
{ 
	return m_vecHmsUserDBs;		// Return private data member; 060228 p�d
}

BOOL CDBHandleAdmin::userdb_new_upd(DB_CONNECTION_DATA &db_conn,vecHmsUserDBs &vec)
{
	BOOL bReturn = FALSE;
	CDBAdmin *db = new CDBAdmin(db_conn);
	if (db && vec.size() > 0)
	{
		for (UINT i = 0;i < vec.size();i++)
		{
			CHmsUserDBRec rec = vec[i];
			bReturn = db->userdb_NewUpd(rec);
		}	// for (int i = 0;i < pRecs->GetCount();i++)
		delete db;
	}	// if (db)

	return bReturn;
}

BOOL CDBHandleAdmin::userdb_remove(DB_CONNECTION_DATA &db_conn,CHmsUserDBRec &rec)
{
	BOOL bReturn = FALSE;
	CDBAdmin *db = new CDBAdmin(db_conn);
	if (db)
	{
		if (db->delete_UserDatabase(rec))	// Remove the actual database from Server; 060906 p�d
		{
			if (db->userdb_Remove(rec)) // Remove items from HMS_USER_DB table; 060906 p�d
			{
				bReturn = TRUE;
			} // if (db->delete_UserDatabase(rec))
		}	// if (db->userdb_Remove(rec))
		delete db;
	}	// if (db)

	return bReturn;
}


//-------------------------------------------------------------------------
// Create user database method; 060302 p�d
//-------------------------------------------------------------------------
BOOL CDBHandleAdmin::create_userDatabase(DB_CONNECTION_DATA &db_conn,vecHmsUserDBs &vec)
{
	BOOL bReturn = FALSE;
	CDBAdmin *db = new CDBAdmin(db_conn);
	if (db && vec.size() > 0)
	{
		for (UINT i = 0;i < vec.size();i++)
		{
			CHmsUserDBRec rec = vec[i];
			bReturn = db->create_UserDatabase(rec);
		}	// for (int i = 0;i < pRecs->GetCount();i++)
		delete db;
	}	// if (db)

	return bReturn;
}

//-------------------------------------------------------------------------
// Delete user database method; 060302 p�d
//-------------------------------------------------------------------------
BOOL CDBHandleAdmin::delete_userDatabase(vecHmsUserDBs &vec)
{
	TCHAR szDBServerPath[128];
	TCHAR szDBUser[32];
	TCHAR szDBPsw[32];
	SAClient_t client;
	BOOL bReturn;
	int nAuthentication;
	CDBAdmin *db = NULL;
	if(getDBInfo(HMS_ADMINISTRATOR_DBNAME,szDBServerPath,szDBUser,szDBPsw,&client,&nAuthentication))
	{
		if (nAuthentication == 0)
			db = new CDBAdmin(client,szDBServerPath,_T(""),_T(""));
		else if (nAuthentication == 1)
			db = new CDBAdmin(client,szDBServerPath,(szDBUser),(szDBPsw));
		if (db->connectToDataBase() && vec.size() > 0)
		{
			for (UINT i = 0;i < vec.size();i++)
			{
				CHmsUserDBRec rec = vec[i];
				bReturn =	db->delete_UserDatabase(rec);
			}	// for (int i = 0;i < pRecs->GetCount();i++)
		}	// if (db->connectToDataBase())
		delete db;
		return bReturn;
	} // if(GetDatabaseInfo(HMS_ADMINISTRATOR_DBNAME,&client,szDBServerPath,szDBUser,szDBPsw))
	return FALSE;
}




/////////////////////////////////////////////////////////////////////////////
// class XMLHandler

inline void CHECK( HRESULT _hr ) 
{ 
  if FAILED(_hr) throw(_hr); 
}

XMLHandler::XMLHandler()
{
	CHECK(CoInitialize(NULL));
  m_sModulePath	= _T("");
  pXMLDoc = NULL;
	m_sLangSet = getLangSet();
}

XMLHandler::XMLHandler(LPCTSTR module_path,HINSTANCE hinst)
{
	CHECK(CoInitialize(NULL));
  m_sModulePath	  = (module_path);
  pXMLDoc		  = NULL;
}

XMLHandler::~XMLHandler()
{
  CoUninitialize();
  m_sModulePath	  = _T("");
  pXMLDoc = NULL;
}

BOOL XMLHandler::load(LPCTSTR xml_fn)
{
  try
  {
		
		// Create MSXML2 DOM Document
		CHECK(pXMLDoc.CreateInstance(_T("Msxml2.DOMDocument.3.0")));

		// Set parser property settings
		pXMLDoc->async = VARIANT_FALSE;

		// Validate during parsing
		pXMLDoc->validateOnParse = VARIANT_FALSE;

    // Load the sample XML file
    if (pXMLDoc->load(xml_fn) != VARIANT_TRUE)
	  return false;
  }
  catch(...)
  {
		AfxMessageBox(_T("Error on XMLHandler::Start()"));
  }
  m_sModulePath	  = xml_fn;
	m_sLangSet			= getLangSet();

	return true;
}

BOOL XMLHandler::save(LPCTSTR xml_fn)
{
	return pXMLDoc->save(xml_fn);
}

BOOL XMLHandler::getDBInfoFromAdministration(vecADMIN_INI_DATABASES &vec)
{
	CString sFileName;
	CString sName;
	CString sDSN;
	CString sClient;
	MSXML2::IXMLDOMNodePtr ptrNodeSuite = NULL;
	MSXML2::IXMLDOMNodePtr ptrNodeUM = NULL;
	MSXML2::IXMLDOMNodePtr ptrItem = NULL;

	// Get Root node
	MSXML2::IXMLDOMNodePtr ptrRoot = pXMLDoc->documentElement;
	
	MSXML2::IXMLDOMNodeListPtr ptrChildList = ptrRoot->selectNodes( ADMIN_SUITE_TAG_WC );
	if (ptrChildList)
	{
		for (long i = 0;i < ptrChildList->length;i++)
		{
			MSXML2::IXMLDOMNodePtr ptrChild = ptrChildList->item[i];
			if (ptrChild)
			{
				sName = (BSTR)ptrChild->selectSingleNode(_T("name"))->text;
				sDSN = (BSTR)ptrChild->selectSingleNode(_T("dsn"))->text;
				sClient = (BSTR)ptrChild->selectSingleNode(_T("client"))->text;
				vec.push_back(_admin_ini_databases(sName,_T(""),sDSN,sClient));
			}
		}
	}

	return TRUE;
}


//////////////////////////////////////////////////////////////////////////////////
// CUMUserDB; Handle ALL transactions for Forrest suite specifics; 061116 p�d

CUMUserDB::CUMUserDB(void)
	: CDBBaseClass_SQLApi(SA_Client_NotSpecified)
{
}

CUMUserDB::CUMUserDB(SAClient_t client,LPCTSTR db_name,LPCTSTR user_name,LPCTSTR psw)
	: CDBBaseClass_SQLApi(client,db_name,user_name,psw)
{
}

CUMUserDB::CUMUserDB(DB_CONNECTION_DATA &db_connection)
	: CDBBaseClass_SQLApi(db_connection,1)
{
}

// PUBLIC

BOOL CUMUserDB::getProperties(CString sql,vecTransactionProperty &vec)
{
	CString sSQL = sql;
	try
	{
		vec.clear();
		
		m_saCommand.setCommandText((SAString)sql);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
	
			vec.push_back(CTransaction_property(m_saCommand.Field(1).asLong(),
																					 (LPCTSTR)m_saCommand.Field(2).asString(),
																					 (LPCTSTR)m_saCommand.Field(3).asString(),
																					 (LPCTSTR)m_saCommand.Field(4).asString(),
																					 (LPCTSTR)m_saCommand.Field(5).asString(),
																					 (LPCTSTR)m_saCommand.Field(6).asString(),
																					 (LPCTSTR)m_saCommand.Field(7).asString(),
																					 (LPCTSTR)m_saCommand.Field(8).asString(),
																					 (LPCTSTR)m_saCommand.Field(9).asString(),
																					 (LPCTSTR)m_saCommand.Field(10).asString(),
																					 (LPCTSTR)m_saCommand.Field(11).asString(),
																					 m_saCommand.Field(12).asDouble(),
																					 m_saCommand.Field(13).asDouble(),
																					 (LPCTSTR)m_saCommand.Field(14).asString(),
																					 (LPCTSTR)m_saCommand.Field(15).asString(),
																					 (LPCTSTR)m_saCommand.Field(16).asString(),
																					 (LPCTSTR)m_saCommand.Field(18).asString(),
																					 (LPCTSTR)m_saCommand.Field(17).asString()) );
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMUserDB::addProperty(CString sql,CTransaction_property &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
			m_saCommand.setCommandText((SAString)sql);
			m_saCommand.Param(1).setAsLong()	= rec.getID();
			m_saCommand.Param(2).setAsString()	= rec.getCountyCode();
			m_saCommand.Param(3).setAsString()	= rec.getMunicipalCode();
			m_saCommand.Param(4).setAsString()	= rec.getParishCode();
			m_saCommand.Param(5).setAsString()	= rec.getCountyName();
			m_saCommand.Param(6).setAsString()	= rec.getMunicipalName();
			m_saCommand.Param(7).setAsString()	= rec.getParishName();
			m_saCommand.Param(8).setAsString()	= rec.getPropertyNum();
			m_saCommand.Param(9).setAsString()	= rec.getPropertyName();
			m_saCommand.Param(10).setAsString()	= rec.getBlock();
			m_saCommand.Param(11).setAsString()	= rec.getUnit();
			m_saCommand.Param(12).setAsDouble()	= rec.getAreal();
			m_saCommand.Param(13).setAsDouble()	= rec.getArealMeasured();
			m_saCommand.Param(14).setAsString()	= getUserName();
			m_saCommand.Param(15).setAsString()	= rec.getObjectID();
			m_saCommand.Execute();
			bReturn = TRUE;
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return bReturn;
}

BOOL CUMUserDB::getContacts(CString sql,vecTransactionContacts &vec)
{
	try
	{
		vec.clear();
		
		m_saCommand.setCommandText((SAString)sql);
		m_saCommand.Execute();

		
		while(m_saCommand.FetchNext())
		{	
			vec.push_back(CTransaction_contacts(m_saCommand.Field(_T("id")).asLong(),
				(LPCTSTR)(m_saCommand.Field(_T("pnr_orgnr")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("name_of")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("company")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("address")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("co_address")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("post_num")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("post_address")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("county")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("country")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("phone_work")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("phone_home")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("fax_number")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("mobile")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("e_mail")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("web_site")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("notes")).asLongChar()),
				(LPCTSTR)(m_saCommand.Field(_T("created_by")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("created")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("vat_number")).asString()), 
				m_saCommand.Field(_T("connect_id")).asLong(),
				m_saCommand.Field(_T("type_of")).asLong(),
				(LPCTSTR)(m_saCommand.Field(_T("bankgiro")).asString()), 
				(LPCTSTR)(m_saCommand.Field(_T("plusgiro")).asString()), 
				(LPCTSTR)(m_saCommand.Field(_T("bankkonto")).asString()), 
				(LPCTSTR)(m_saCommand.Field(_T("levnummer")).asString()) ));
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMUserDB::addContact(CString sql,CTransaction_contacts &rec)
{
	BOOL bReturn = FALSE;
	try
	{
			m_saCommand.setCommandText((SAString)sql);
			m_saCommand.Param(1).setAsLong()		= rec.getID();
			m_saCommand.Param(1).setAsLong()		= rec.getID();
			m_saCommand.Param(2).setAsString()	= rec.getPNR_ORGNR();
			m_saCommand.Param(3).setAsString()	= rec.getName();
			m_saCommand.Param(4).setAsString()	= rec.getCompany();
			m_saCommand.Param(5).setAsString()	= rec.getAddress();
			m_saCommand.Param(6).setAsString()	= rec.getPostNum();
			m_saCommand.Param(7).setAsString()	= rec.getPostAddress();
			m_saCommand.Param(8).setAsString()	= rec.getCounty();
			m_saCommand.Param(9).setAsString()	= rec.getCountry();
			m_saCommand.Param(10).setAsString()	= rec.getPhoneWork();
			m_saCommand.Param(11).setAsString()	= rec.getPhoneHome();
			m_saCommand.Param(12).setAsString()	= rec.getFaxNumber();
			m_saCommand.Param(13).setAsString() = rec.getMobile();
			m_saCommand.Param(14).setAsString() = rec.getEMail();
			m_saCommand.Param(15).setAsString() = rec.getWebSite();
			m_saCommand.Param(16).setAsLongChar()		= rec.getNotes();
			m_saCommand.Param(17).setAsString() = getUserName();
			m_saCommand.Execute();
			bReturn = TRUE;
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return bReturn;
}


BOOL CUMUserDB::getCategories(CString sql,vecTransactionCategory &vec)
{
	try
	{
		vec.clear();	
		m_saCommand.setCommandText((SAString)sql);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
	
			vec.push_back(CTransaction_category(m_saCommand.Field(1).asLong(),
																					(LPCTSTR)m_saCommand.Field(2).asString(),
																					(LPCTSTR)m_saCommand.Field(3).asLongChar(),
																					_T("")));
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;

}

BOOL CUMUserDB::addCategory(CString sql,CTransaction_category &rec)
{
	BOOL bReturn = FALSE;
	try
	{
			m_saCommand.setCommandText((SAString)sql);
			m_saCommand.Param(1).setAsLong()	= rec.getID();
			m_saCommand.Param(2).setAsString()	= rec.getCategory();
			m_saCommand.Param(3).setAsLongChar()= rec.getNotes();
			m_saCommand.Execute();
			bReturn = TRUE;
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return bReturn;
}

BOOL CUMUserDB::clearTable(LPCTSTR db,LPCTSTR table)
{
	CString sMsg;
	CString sSQL;
	try
	{

			sSQL.Format(_T("USE \"%s\";delete from %s"),db,table);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMUserDB::getTableColumns(LPCTSTR szDB, LPCTSTR szTable, std::vector<CString> &vCols)
{
	CString csSql;

	try
	{
		// h�mta ut alla kolumner i tabellen
		csSql.Format(_T("select name from [%s].sys.syscolumns where id = (select object_id from [%s].sys.tables where name = '%s')"),
			szDB, szDB, szTable);

		m_saCommand.setCommandText((SAString)csSql);
		m_saCommand.Execute();

		while( m_saCommand.FetchNext() )
		{
			vCols.push_back( CString(m_saCommand.Field(1).asString()) );
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMUserDB::copyTable(LPCTSTR szSrcDB, LPCTSTR szDstDB, LPCTSTR szSrcTbl, LPCTSTR szDstTbl, std::vector<CString> &vCols)
{
	CString csSql;

	try
	{
		// kopiera fr�n k�ll- till destinationstabell
		CString csTmp;
		CString csCols;
		for(int nCol=0; nCol<vCols.size(); nCol++)
		{
			csTmp.Format(_T("%s,"), vCols.at(nCol));
			csCols += csTmp;
		}
		if(csCols.Right(1) == _T(",")) csCols.Truncate(csCols.GetLength()-1);

		// kolla om det finns IDENTITY-f�lt, i s� fall fixa s� att vi kan l�gga in s�nt
		// kopiera sedan alla kolumner fr�n k�llan till destinationen
		csSql.Format(_T("USE \"%s\"; ")
			_T("IF (SELECT COUNT(*) FROM sys.columns WHERE object_id = OBJECT_ID('%s', 'U') AND is_identity = 1) > 0 SET IDENTITY_INSERT [dbo].[%s] ON;  ")
			_T("INSERT INTO [%s]..[%s] ( %s ) ")
			_T("SELECT %s FROM [%s]..[%s]; "),
			szDstDB,
			szDstTbl,szDstTbl,
			szDstDB, szDstTbl, csCols,
			csCols, szSrcDB, szSrcTbl
			);

		m_saCommand.setCommandText((SAString)csSql);
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMUserDB::getDataBase(CString &szDB)
{
	CString csSql;

	try
	{
		// h�mta ut namn p� nuvarande databas
		csSql.Format(_T("SELECT DB_NAME()"));

		m_saCommand.setCommandText((SAString)csSql);
		m_saCommand.Execute();

		while( m_saCommand.FetchNext() )
		{
			szDB = ( CString(m_saCommand.Field(1).asString()) );
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMUserDB::setDataBase(CString &szDB)
{
	CString csSql;

	try
	{
		// s�tt vilken databas som ska anv�ndas
		csSql.Format(_T("USE \"%s\""), szDB);

		m_saCommand.setCommandText((SAString)csSql);
		m_saCommand.Execute();
	}
	catch(SAException &e)
	{
		AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

// NewDBDlg.cpp : implementation file
//

#include "stdafx.h"
#include "UMDataBase.h"
#include "NewDBDlg.h"
#include "ResLangFileReader.h"


// CNewDBDlg dialog

IMPLEMENT_DYNAMIC(CNewDBDlg, CDialog)

CNewDBDlg::CNewDBDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CNewDBDlg::IDD, pParent)
{

}

CNewDBDlg::~CNewDBDlg()
{
}

void CNewDBDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT1, m_name);
}


BEGIN_MESSAGE_MAP(CNewDBDlg, CDialog)
	ON_EN_CHANGE(IDC_EDIT1, &CNewDBDlg::OnEnChangeEdit1)
END_MESSAGE_MAP()

BOOL CNewDBDlg::OnInitDialog()
{
	if( !CDialog::OnInitDialog() )
		return FALSE;

	m_name.LimitText(24);
	m_strName.Empty();

	

	CString lang;
	RLFReader *xml = new RLFReader;
	lang.Format(_T("%s%s%s%s"),getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);
	if (xml->Load(lang))
	{
		SetWindowText(xml->str(IDS_STRING177));
		GetDlgItem(IDOK)->SetWindowText(xml->str(IDS_STRING144));
		GetDlgItem(IDCANCEL)->SetWindowText(xml->str(IDS_STRING153));
	}
	delete xml;

	return TRUE;
}

void CNewDBDlg::OnOK()
{
	m_name.GetWindowText(m_strName);

	CDialog::OnOK();
}

//#5036 PH 2016-08-05 Kontrollera s� inte databasnamnet inneh�ller otill�tna tecken.
void CNewDBDlg::OnEnChangeEdit1()
{
	m_name.GetWindowText(m_strName);
	CString newString;
	int c;

	//Skapa en ny str�ng med bara dom till�tna tecknen
	for(int i=0;i<m_strName.GetLength();i++) {
		c=m_strName.GetAt(i);
		if(((c<58&&c>47)||(c<91&&c>64)||(c<123&&c>96)||(c==95)||(c==45))) {
			newString.AppendChar((char)c);
		}
	}

	//Kolla att den nya str�ngen och den gamla �r likadana
	//Annars byt ut den gamla mot den nya.
	if(m_strName.Compare(newString)!=0) {
		m_name.SetWindowTextW(newString);

		//Flytta till mark�ren till slutet av str�ngen
		m_name.SetSel(0,-1);
		m_name.SetSel(-1);
	}
}

//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by UMDataBase.rc
//
#define IDD_FORMVIEW                    600
#define IDD_FORMVIEW1                   601
#define IDD_FORMVIEW2                   602
#define IDD_FORMVIEW3                   603
#define IDD_DIALOGBAR                   604
#define IDD_DIALOG2                     15031
#define IDC_PROGRESS1                   15031
#define IDD_DIALOG3                     15032
#define IDC_BUTTON5                     15033
#define IDC_BUTTON6                     15034
#define IDI_FORMICON                    15100
#define IDC_EDIT1                       15101
#define IDR_TOOLBAR1                    15102
#define IDB_BITMAP1                     15103
#define IDB_TAB_ICONS                   15104
#define IDC_CHECK1                      15105
#define IDB_BITMAP2                     15106
#define IDC_CHECK2                      15107
#define IDC_BUTTON1                     15108
#define IDD_DIALOG1                     15109
#define IDC_CHECK3                      15110
#define IDC_BUTTON4                     15111
#define IDC_LABEL1                      15112
#define IDC_BUTTON2                     15113
#define IDC_CHECK4                      15114
#define IDC_BUTTON3                     15115
#define IDC_CHECK5                      15116
#define IDC_LBL1                        15117
#define IDC_LBL2                        15118
#define IDC_LBL3                        15119
#define IDC_LBL4                        15120
#define IDC_LBL5                        15121
#define IDC_GROUP1                      15122
#define IDC_COMBO1                      15123
#define IDC_COMBO2                      15124
#define IDC_FROM_LBL                    15125
#define IDC_COMBO3                      15126
#define IDC_TO_LBL                      15127
#define IDC_EDIT2                       15128
#define IDC_LIST1                       15129
#define IDC_GROUP2                      15130
#define ID_RUN_SCRIPT                   32771
#define ID_TBBTN_EXPORT                 32771
#define ID_BUTTON32773                  32773
#define ID_TBBTN_IMPORT                 32773

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        15033
#define _APS_NEXT_COMMAND_VALUE         32774
#define _APS_NEXT_CONTROL_VALUE         15036
#define _APS_NEXT_SYMED_VALUE           15004
#endif
#endif

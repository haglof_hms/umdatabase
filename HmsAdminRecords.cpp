#include "StdAfx.h"
#include "HmsAdminRecords.h"


//////////////////////////////////////////////////////////////////////////////
//	CHmsUserDBRec; transaction class for data in specie table; 060210 p�d

CHmsUserDBRec::CHmsUserDBRec(void)
{
	m_sDBName				= _T("");	// Username of database (company)
	m_sUserName			= _T("");	// Location
	m_sNotes				= _T("");	// Notes 
	m_sCreatedBy		= _T("");	// Name of creator
	m_sCreated			= _T("");	// Date/Time; created/updated
}

CHmsUserDBRec::CHmsUserDBRec(LPCTSTR db_name,
														 LPCTSTR user_name,
														 LPCTSTR notes,
														 LPCTSTR created_by,
														 LPCTSTR created)

{
	m_sDBName				= (db_name);					// Name of database on server
	m_sUserName			= (user_name);				// Username of datbase (Company or whatever)
	m_sNotes				= (notes);						// Notes 
	m_sCreatedBy		= (created_by);				// Name of creator
	m_sCreated			= (created);					// Date/Time; created/updated
}

#if !defined(AFX_UMDATABASE_H)
#define AFX_UMDATABASE_H

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define __BUILD

#ifdef __BUILD
#define DLL_BUILD __declspec(dllexport)
#else
#define DLL_BUILD __declspec(dllimport)
#endif

#endif
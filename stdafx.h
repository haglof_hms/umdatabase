// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently

#pragma once

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers
#endif


#define WINVER 0x0500		// Target Windows 2000
#define _WIN32_WINNT 0x0500

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS	// some CString constructors will be explicit

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions
#include <winver.h>

#ifndef _AFX_NO_OLE_SUPPORT
#include <afxole.h>         // MFC OLE classes
#include <afxodlgs.h>       // MFC OLE dialog classes
#include <afxdisp.h>        // MFC Automation classes
#endif // _AFX_NO_OLE_SUPPORT

#ifndef _AFX_NO_DB_SUPPORT
#include <afxdb.h>			// MFC ODBC database classes
#endif // _AFX_NO_DB_SUPPORT

#ifndef _AFX_NO_DAO_SUPPORT
//#include <afxdao.h>			// MFC DAO database classes
#endif // _AFX_NO_DAO_SUPPORT

#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT


#include <vector>
#include <map>

// XML handling
#import <msxml3.dll> //named_guids
#include <msxml2.h>

#include <SQLAPI.h> // main SQLAPI++ header

#if (_MSC_VER > 1310) // VS2005
#pragma comment(linker, "\"/manifestdependency:type='Win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='X86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif

#include <XTToolkitPro.h> // Xtreme Toolkit MFC extensions

#include "pad_hms_miscfunc.h"
#include "pad_transaction_classes.h"

#include "ProgressDialog.h"


/////////////////////////////////////////////////////////////////////
// My own includes

#define WM_USER_MSG_SUITE		(WM_USER + 2)		// Used for messages sent from a Suite/usermodule

#define USER_MSG_SHELL_TREE 	(WM_USER + 3)		// This is a identifer used when communication from a


#define MSG_IN_SUITE			(WM_USER + 10)		// This identifer's used to send messages internally

/////////////////////////////////////////////////////////////////////
// Index of pages (tabs) in CMyTabControl
#define TABCTRL_PAGE1								0
#define TABCTRL_PAGE2								1
#define TABCTRL_PAGE3								2
#define TABCTRL_PAGE4								3

/////////////////////////////////////////////////////////////////////
// String id
#define IDS_STRING100								100
#define IDS_STRING101								101
#define IDS_STRING102								102
#define IDS_STRING103								103
#define IDS_STRING104								104
#define IDS_STRING105								105
#define IDS_STRING106								106
#define IDS_STRING107								107
#define IDS_STRING108								108
#define IDS_STRING109								109
#define IDS_STRING110								110
#define IDS_STRING111								111
#define IDS_STRING112								112
#define IDS_STRING113								113
#define IDS_STRING114								114
#define IDS_STRING115								115
#define IDS_STRING116								116
#define IDS_STRING117								117
#define IDS_STRING118								118
#define IDS_STRING119								119
#define IDS_STRING120								120
#define IDS_STRING121								121
#define IDS_STRING122								122
#define IDS_STRING123								123
#define IDS_STRING124								124
#define IDS_STRING125								125
#define IDS_STRING126								126
#define IDS_STRING127								127
#define IDS_STRING128								128
#define IDS_STRING129								129
#define IDS_STRING130								130
#define IDS_STRING131								131
#define IDS_STRING132								132
#define IDS_STRING133								133
#define IDS_STRING134								134
#define IDS_STRING135								135
#define IDS_STRING136								136
#define IDS_STRING137								137
#define IDS_STRING138								138
#define IDS_STRING139								139
#define IDS_STRING140								140
#define IDS_STRING141								141
#define IDS_STRING142								142
#define IDS_STRING143								143
#define IDS_STRING144								144
#define IDS_STRING145								145
#define IDS_STRING146								146
#define IDS_STRING147								147
#define IDS_STRING148								148
#define IDS_STRING149								149
#define IDS_STRING150								150
#define IDS_STRING151								151
#define IDS_STRING152								152
#define IDS_STRING153								153
#define IDS_STRING154								154
#define IDS_STRING155								155
#define IDS_STRING156								156
#define IDS_STRING157								157
#define IDS_STRING158								158
#define IDS_STRING159								159
#define IDS_STRING160								160
#define IDS_STRING161								161
#define IDS_STRING162								162
#define IDS_STRING163								163
#define IDS_STRING164								164
#define IDS_STRING165								165
#define IDS_STRING166								166
#define IDS_STRING167								167
#define IDS_STRING168								168

#define IDS_STRING169								169
#define IDS_STRING170								170
#define IDS_STRING171								171
#define IDS_STRING172								172

#define IDS_STRING173								173
#define IDS_STRING174								174
#define IDS_STRING175								175
#define IDS_STRING176								176
#define IDS_STRING177								177
#define IDS_STRING178								178
#define IDS_STRING179								179
#define IDS_STRING180								180
#define IDS_STRING181								181
#define IDS_STRING182								182

#define IDS_STRING1001							1001
#define IDS_STRING1002							1002
#define IDS_STRING1003							1003
#define IDS_STRING1004							1004
#define IDS_STRING1005							1005
#define IDS_STRING1006							1006
#define IDS_STRING1007							1007
#define IDS_STRING1008							1008
#define IDS_STRING1009							1009
#define IDS_STRING1010							1010

#define IDS_STRING1050							1050
#define IDS_STRING1051							1051
#define IDS_STRING1052							1052
#define IDS_STRING1053							1053
#define IDS_STRING1054							1054
#define IDS_STRING1055							1055
#define IDS_STRING1056							1056
#define IDS_STRING1057							1057
#define IDS_STRING1058							1058
#define IDS_STRING1059							1059
#define IDS_STRING1060							1060
#define IDS_STRING1061							1061
#define IDS_STRING1062							1062
#define IDS_STRING1063							1063
#define IDS_STRING1064							1064
#define IDS_STRING1065							1065
#define IDS_STRING1066							1066
#define IDS_STRING1070							1070
#define IDS_STRING1071							1071
#define IDS_STRING1072							1072
#define IDS_STRING1073							1073

#define IDS_STRING1080							1080
#define IDS_STRING1081							1081
#define IDS_STRING1082							1082
#define IDS_STRING1083							1083
#define IDS_STRING1084							1084
#define IDS_STRING1085							1085
#define IDS_STRING1086							1086

/////////////////////////////////////////////////////////////////////
// Componants (not in resource) ID's; 060215 p�d

#define IDC_PAGEONE_REPORT						0x8003
#define IDC_PAGETWO_REPORT						0x8004
#define IDC_PAGETHREE_REPORT					0x8005
#define IDC_PAGEFOUR_REPORT						0x8006


// Buffer
#define BUFFER_SIZE		1024*500		// 500 kb buffer


/////////////////////////////////////////////////////////////////////
// Registry settingd; 060209 p�d
const LPCTSTR PROGRAM_NAME						= _T("UMDataBase");					// Used for Languagefile, registry entries etc.; 051114 p�d

const LPCTSTR MYSQL_NAME						= _T("MySQLFrame");				// Used for Languagefile, registry entries etc.; 051114 p�d
const LPCTSTR SQLSERV_NAME						= _T("SQLServerFrame");				// Used for Languagefile, registry entries etc.; 051114 p�d

//////////////////////////////////////////////////////////////////////////////////////////
//	Main resource dll, for toolbar icons; 051219 p�d

const LPCTSTR TOOLBAR_RES_DLL					= _T("HMSIcons.icl");		// Resource dll, holds icons for e.g. toolbars; 051208 p�d

const int RES_TB_IMPORT						= 19;
const int RES_TB_EXPORT						= 9;

//////////////////////////////////////////////////////////////////////////////////////////
// Tables in hms_administrator database; 060303 p�d

//const LPCTSTR HMS_ADMINISTRATOR_DBNAME		= _T("hms_administrator");	// Administrator database name; 060303 p�d
//const LPCTSTR HMS_USER_DB_TABLE_NAME			= _T("HMS_USER_DB");		// Name of table in database; 060228 p�d

const LPCTSTR SHELLDATA_AdminFN					= _T("Administration.xml");
const LPCTSTR ADMIN_SUITE_TAG_WC				= _T("DBConnection/*");

//////////////////////////////////////////////////////////////////////////////////////////
// Name of database-tables (some of them,some are declared in HMSFuncLib); 081008 p�d
const LPCTSTR TBL_PRICELIST						= _T("prn_pricelist_table");
const LPCTSTR TBL_COST_TEMPLATE					= _T("cost_template_table");
const LPCTSTR TBL_TMPL_TEMPLATE					= _T("tmpl_template_table");
const LPCTSTR TBL_POSTNUMBERS					= _T("fst_postnumber_table");
const LPCTSTR TBL_CMP							= _T("fst_county_municipal_parish_table");
const LPCTSTR TBL_SPECIES						= _T("fst_species_table");
const LPCTSTR TBL_PROPERTIES					= _T("fst_property_table");
const LPCTSTR TBL_CONTACTS						= _T("fst_contacts_table");
const LPCTSTR TBL_PROP_OWNER					= _T("fst_prop_owner_table");
const LPCTSTR TBL_CATEGORIES					= _T("fst_category_table");
const LPCTSTR TBL_CATEGORIES_FOR_CONTACT		= _T("fst_categories_for_contacts_table");
const LPCTSTR TBL_ELV_TEMPLATES					= _T("elv_template_table");
const LPCTSTR TBL_ELV_DOCUMENTS					= _T("elv_document_template_table");
const LPCTSTR TBL_ESTI_TRAKT_TYPE				= _T("esti_trakt_type_table");
const LPCTSTR TBL_ESTI_ORIGIN					= _T("esti_origin_table");
const LPCTSTR TBL_ESTI_INPDATA					= _T("esti_inpdata_table");
const LPCTSTR TBL_ESTI_CUTCLS					= _T("esti_cutcls_table");
const LPCTSTR TBL_ESTI_HGTCLS					= _T("esti_hgtcls_table");
const LPCTSTR TBL_ELV_PROP_STATUS 				= _T("elv_property_status_table");

const int NUM_OF_TABLES_TO_COPY							= 16;
//////////////////////////////////////////////////////////////////////////////////////////
// SQL Script file Open and Save dialog filter; 061114 p�d

const LPCTSTR SQL_SCRIPT_FILEFILTER				= _T("SQL Script (*.sql)|*.sql||");

//////////////////////////////////////////////////////////////////////////////////////////
// SQL Script file, create and setup hms_administrator database; 061113 p�d

const LPCTSTR HMS_ADMIN_TABLE					= _T("Create_admin_db_table.sql");

// This table holds information on County(L�n),Municipal(Kommun) and Parish(Socken); 070115 p�d
const LPCTSTR TBL_COUNTY_MUNICIPAL_PARISH		= _T("fst_county_municipal_parish_table");
const LPCTSTR TBL_POSTNUMBER					= _T("fst_postnumber_table");

// Exported function in Suite/Module, to create Tables in Active database; 080421 p�d
const LPCSTR EXPORTED_DO_DATABASE_TABLES		= "DoDatabaseTables";
const LPCSTR EXPORTED_DO_ALTER_TABLES			= "DoAlterTables";

const LPCTSTR SUITES_FN_WC						= _T("*.dll");			// Wildcard for Suites files; 051115 p�d
const LPCTSTR MODULES_FN_WC						= _T("*.dll");			// Wildcard for Module files; 051128 p�d

//////////////////////////////////////////////////////////////////////////////////////////
// Subdirectory path(s); 090109 p�d
const LPCTSTR SUBDIR_SETUP_DATA_FILES			= _T("Setup");

//////////////////////////////////////////////////////////////////////////////////////////
// Filename for semicolon limited postnumber file; 090109 p�d
const LPCTSTR POSTNUMBER_FN						= _T("Postnumber_table.skv");

//////////////////////////////////////////////////////////////////////////////////////////
// Filename for semicolon limited county,municipal and parish file; 090109 p�d
const LPCTSTR COUNTY_MUNICIPAL_PARISH_FN		= _T("County_Municipal_Parish_table.skv");

//////////////////////////////////////////////////////////////////////////////////////////
// Defines for minimum size of Window(s); 060209 p�d

const int MIN_X_SIZE_MYSQL			= 620;
const int MIN_Y_SIZE_MYSQL			= 400;

const int MIN_X_SIZE_SQLSERV		= 600;
const int MIN_Y_SIZE_SQLSERV		= 400;

//////////////////////////////////////////////////////////////////////////////////////////
// Defines for minimum size of Window(s); 060904 p�d

const int MIN_X_SIZE_TABLE_IMPORT	= 500;
const int MIN_Y_SIZE_TABLE_IMPORT	= 350;

const int MIN_X_SIZE_SETUP_COMPANY	= 370;
const int MIN_Y_SIZE_SETUP_COMPANY	= 350;

const int MIN_X_SIZE_SQLSCRIPT		= 600;
const int MIN_Y_SIZE_SQLSCRIPT		= 400;


//////////////////////////////////////////////////////////////////////////////////////////
// Window placement registry key; 060904 p�d

const LPCTSTR REG_WP_TABLE_IMPORT_KEY				= _T("UMDatabase\\TableImport\\Placement");

const LPCTSTR REG_WP_SETUPCOMPANY_KEY				= _T("UMDatabase\\SetupCompany\\Placement");

const LPCTSTR REG_WP_SETUPUSER_KEY					= _T("UMDatabase\\SetupUser\\Placement");

const LPCTSTR REG_WP_SETUPUSERPERMISSIONS_KEY		= _T("UMDatabase\\SetupUserPermissions\\Placement");

const LPCTSTR REG_WP_SQLSCRIPT_KEY					= _T("UMDatabase\\SQLScript\\Placement");

//////////////////////////////////////////////////////////////////////////////////////////
// Colors
#define LTCYAN				RGB(  224,  255,  255)

//////////////////////////////////////////////////////////////////////////////////////////
// XML tags for saving/importing database-profiles; 2011-10-24 p�d
const LPCTSTR TAG_DBPROF_FIRST_ROW			= _T("<?xml version=\"1.0\" encoding=\"utf-8\"?>");

const LPCTSTR TAG_DBPROF_START					= _T("<DB_PROFILE>");
const LPCTSTR TAG_DBPROF_END						= _T("</DB_PROFILE>");

const LPCTSTR TAG_DBPROF_LOCATION				= _T("<LOCATION>%s</LOCATION>");
const LPCTSTR TAG_DBPROF_SELECTED				= _T("<SELECTED>%s</SELECTED>");
const LPCTSTR TAG_DBPROF_SERVER					= _T("<SERVER>%s</SERVER>");
const LPCTSTR TAG_DBPROF_AUTENTICATION	= _T("<AUTENTICATION>%d</AUTENTICATION>");
const LPCTSTR TAG_DBPROF_PSW						= _T("<PSW>%s</PSW>");
const LPCTSTR TAG_DBPROF_USERNAME				= _T("<USERNAME>%s</USERNAME>");

const LPCTSTR TAG_DBPROF_LOCATION2			= _T("LOCATION");
const LPCTSTR TAG_DBPROF_SELECTED2			= _T("SELECTED");
const LPCTSTR TAG_DBPROF_SERVER2				= _T("SERVER");
const LPCTSTR TAG_DBPROF_AUTENTICATION2	= _T("AUTENTICATION");
const LPCTSTR TAG_DBPROF_PSW2						= _T("PSW");
const LPCTSTR TAG_DBPROF_USERNAME2			= _T("USERNAME");


//////////////////////////////////////////////////////////////////////////////////////////
// USER MESSAGE STRUCT; Used when communicaton with HMSShell.
// This stucture must be in each Suite/User module when communicating with HMSShell
// on USER_MSG_xxxx_xxxx
struct _report_fn_struct
{
	TCHAR szLanguageFN[MAX_PATH];
	TCHAR szReportFN[MAX_PATH];
	TCHAR szIdentifer[MAX_PATH];		// This datamamber can holds the identifer of e.g. a stand etc. in SQL Select statement; 060119 p�d
	bool bOpenDialog;
};

#define MAX_RET_LENGTH	4824

class CSQLInfoEnumerator  
{
public:
	CSQLInfoEnumerator();
	virtual ~CSQLInfoEnumerator();
	int m_iRetcode;
public:
	CStringArray m_szSQLServersArray;
	CStringArray m_szSQLServerDatabaseArray;
	CStringArray m_szSQLServerLanguageArray;
public:
	BOOL EnumerateDatabase(LPCTSTR pszSQLServer,LPCTSTR pszUserId,LPCTSTR pszPwd);
	BOOL EnumerateDatabaseLanguage(LPCTSTR pszSQLServer,LPCTSTR pszUserId,LPCTSTR pszPwd);
	BOOL EnumerateSQLServers();
protected:
	BOOL RetrieveInformation(LPCTSTR pszInputParam,LPCTSTR pszLookUpKey,CStringArray &szArray);
	BOOL FillupStringArray(LPCTSTR pszData,CStringArray &szArray,TCHAR chSep = ',');
	BOOL ClearAll();
};


//////////////////////////////////////////////////////////////////////////////////////////
// Misc. functions

CString getResStr(UINT id);

CString getToolBarResourceFN(void);

BOOL runSQLScriptFile(LPCTSTR fn,LPCTSTR check_table);
// Create database table from string; 080627 p�d
BOOL runSQLScriptFileEx(LPCTSTR table_name,CString script);

BOOL runSQLScript(LPCTSTR buffer);

BOOL connectToDB(int autentication,LPCTSTR location,LPCTSTR user_name = _T(""),LPCTSTR psw = _T(""));

BOOL getDBServersList(CStringArray &arr,BOOL only_local);

void setupForDBConnection(HWND hWndReciv,HWND hWndSend);

CString getShellDataDir(void);

BOOL getSuites(CStringArray &);

BOOL getModules(CStringArray &str_list);

CView *getFormViewByID(int idd);

BOOL getSuites(CStringArray &str_list,BOOL clear);
BOOL getModules(CStringArray &str_list,BOOL clear);

void runDoDatabaseTablesInSuitesModules(LPCTSTR db_name,CXTPStatusBar &status_bar,CProgressDialog &prg_ctrl,LPCTSTR sbar_text);
void runDoAlterTablesInSuitesModules(LPCTSTR db_name);
void setToolbarBtnIcon(LPCTSTR rsource_dll_fn,CXTPControl *pCtrl,int icon_id,LPCTSTR tool_tip,BOOL show = TRUE);

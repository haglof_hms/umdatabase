#pragma once

#include "Resource.h"

// CGetSereversDlg dialog

class CGetSereversDlg : public CDialog
{
	DECLARE_DYNAMIC(CGetSereversDlg)

	BOOL m_bInitialized;

	CStringArray m_sarrServers;
	CString m_sSelectedDBServer;

	CListBox m_wndServersLB;
	CButton m_wndBtnOK;
	CButton m_wndBtnCancel;
public:
	CGetSereversDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CGetSereversDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG1 };

	void setServers(CStringArray &arr)	{ m_sarrServers.Append(arr); }
	CString getSelectedDBServer(void)		{ return m_sSelectedDBServer; }

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
};

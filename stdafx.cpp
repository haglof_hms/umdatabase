// stdafx.cpp : source file that includes just the standard includes
// UMDataBase.pch will be the pre-compiled header
// stdafx.obj will contain the pre-compiled type information

#include "stdafx.h"

#include "StdioFileEx.h"

#include "SHFolder.h"

#include "DBBaseClass_ADODirect.h"
#include "DBBaseClass_SQLApi.h"

#include <initguid.h>
#include "sqldmoid.h"
#include "sqldmo.h"


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

/*
Author:		Santosh Rao
EMail Id:	SantoshRao@bigfoot.com
Date:		Dec 31st 1999
Comment:	Simple Class to enable enumeration 
			of SQL Servers and their databases and supported 
			languages.
			You may use this code in any way you want,
			You can remove my copyright too.
			But dont sue me in court please, i cant afford it.
*/

CSQLInfoEnumerator::CSQLInfoEnumerator()
{
	ClearAll();
}

CSQLInfoEnumerator::~CSQLInfoEnumerator()
{
	ClearAll();
}

/*
	Clear all internal storing CStringArrays
*/

BOOL CSQLInfoEnumerator::ClearAll()
{
	m_szSQLServersArray.RemoveAll();
	m_szSQLServerDatabaseArray.RemoveAll();
	m_szSQLServerLanguageArray.RemoveAll();
	return TRUE;
}

/*
	Retrieve Information of SQL Servers
	On Success the string contains identifier 
	SERVER:Server= followed by the list of SQL
	Servers
*/

BOOL CSQLInfoEnumerator::EnumerateSQLServers()
{
	//Browse Connect for SQL Server Driver defined servers
	//The return information would contain SERVER:Server= Keyword followed by 
	//{list of Servers} separated by the character ';'
	return RetrieveInformation(_T("Driver={SQL Server}"),_T("SERVER:Server="),m_szSQLServersArray);
}

/*
	Retrieve Information of databases in a SQL Server
	You have to provide the User Id and Password
	On Success the string contains identifier 
	DATABASE:Database= followed by the list of databases
*/
BOOL CSQLInfoEnumerator::EnumerateDatabase(LPCTSTR pszSQLServer, LPCTSTR pszUserId, LPCTSTR pszPwd)
{
	//Browse Connect for SQL Server Driver defined server using the authentication information
	//The return information would contain DATABASE:Database= Keyword followed by 
	//{list of databases} separated by the character ';'
	CString szInputParam;
	szInputParam.Format(_T("Driver={SQL Server};SERVER=%s;UID=%s;PWD=%s"),pszSQLServer,pszUserId,pszPwd);
	return RetrieveInformation(szInputParam,_T("DATABASE:Database="),m_szSQLServerDatabaseArray);
}

/*
	Retrieve Information of languages in a SQL Server
	You have to provide the User Id and Password
	On Success the string contains identifier 
	LANGUAGE:Language= followed by the list of languages
	Character Translation is not done, so you may see
	special characters and question marks in the list of
	languages text
*/

BOOL CSQLInfoEnumerator::EnumerateDatabaseLanguage(LPCTSTR pszSQLServer, LPCTSTR pszUserId, LPCTSTR pszPwd)
{
	CString szInputParam;
	//Browse Connect for SQL Server Driver defined server using the authentication information
	//The return information would contain LANGUAGE:Language= Keyword followed by 
	//{list of languages} separated by the character ';'
	szInputParam.Format(_T("Driver={SQL Server};SERVER=%s;UID=%s;PWD=%s"),pszSQLServer,pszUserId,pszPwd);
	return RetrieveInformation(szInputParam,_T("LANGUAGE:Language="),m_szSQLServerLanguageArray);
}

/*
	This Function Checks for retrieving additional information
	using the initial input information and returns true if the 
	look up key is found and fills ther result set into a string 
	array.
*/

BOOL CSQLInfoEnumerator::RetrieveInformation(LPCTSTR pszInputParam,LPCTSTR pszLookUpKey,CStringArray &szArray)
{
	SQLHENV       hSQLEnv;
	SQLHDBC       hSQLHdbc;
	short		  sConnStrOut;
	BOOL		  bReturn = FALSE;
	//Allocate the environment handle
	m_iRetcode = SQLAllocHandle(SQL_HANDLE_ENV, SQL_NULL_HANDLE, &hSQLEnv);
	if (m_iRetcode == SQL_SUCCESS || m_iRetcode == SQL_SUCCESS_WITH_INFO)
	{
		//Set the environment attribute to SQL_OV_ODBC3
		m_iRetcode = SQLSetEnvAttr(hSQLEnv, SQL_ATTR_ODBC_VERSION, (void *)SQL_OV_ODBC3, 0);
		if (m_iRetcode == SQL_SUCCESS || m_iRetcode == SQL_SUCCESS_WITH_INFO) 
		{
		    //Allocate a connection handle
			m_iRetcode = SQLAllocHandle(SQL_HANDLE_DBC, hSQLEnv, &hSQLHdbc);
			if (m_iRetcode == SQL_SUCCESS || m_iRetcode == SQL_SUCCESS_WITH_INFO) 
			{
				CString szConnStrOut;
				//Call SQLBrowseConnect for additional information
//				m_iRetcode = SQLBrowseConnect(hSQLHdbc, (SQLCHAR *)pszInputParam, SQL_NTS,
//						 (SQLCHAR *)(szConnStrOut.GetBuffer(MAX_RET_LENGTH)), MAX_RET_LENGTH, &sConnStrOut);

				m_iRetcode = SQLBrowseConnect(hSQLHdbc,(SQLWCHAR*)_T("DRIVER={SQL Server};"), SQL_NTS,
						 (wchar_t *)(szConnStrOut.GetBuffer(MAX_RET_LENGTH)), MAX_RET_LENGTH, &sConnStrOut);
				szConnStrOut.ReleaseBuffer();
				//if the look up key is found
				//fill in the result set
				int iFind = szConnStrOut.Find(pszLookUpKey);
				if(iFind != -1)
				{
					CString szLookUpKey = pszLookUpKey;
					szConnStrOut = szConnStrOut.Mid(iFind+szLookUpKey.GetLength());
					iFind = szConnStrOut.Find('{');
					if(iFind != -1)
					{
						szConnStrOut = szConnStrOut.Mid(iFind+1);
						iFind = szConnStrOut.Find('}');
						if(iFind != -1)
						{
							szConnStrOut = szConnStrOut.Left(iFind);
							FillupStringArray(szConnStrOut,szArray);
							bReturn = TRUE;
						}
					}
				}	

				SQLDisconnect(hSQLHdbc);
			}
			SQLFreeHandle(SQL_HANDLE_DBC, hSQLHdbc);
		}
		SQLFreeHandle(SQL_HANDLE_ENV, hSQLEnv);
	}	
	return TRUE;
}

/*Breaks up return information into a CStringArray for easy parsing*/

BOOL CSQLInfoEnumerator::FillupStringArray(LPCTSTR pszData,CStringArray &szArray,TCHAR chSep)
{
	CString szStr = pszData;
	CString szSep(chSep);
	szStr.TrimLeft();
	szStr.TrimRight();
	szArray.RemoveAll();
	int iStrLen = szStr.GetLength(),iSepPos,iSepLength=szSep.GetLength();
	if(iStrLen>0 )
	{
		if(szStr.Right(iSepLength) != szSep)
			szStr += szSep;
		iStrLen = szStr.GetLength();
	}
	else 
		return FALSE;
	
	CString szContentStr;
	while(!szStr.IsEmpty())
	{
		iSepPos = szStr.Find(szSep);
		szContentStr = szStr.Left(iSepPos);
		szContentStr.TrimLeft();
		szContentStr.TrimRight();
		if(!szContentStr.IsEmpty())
			szArray.Add(szContentStr);
		iStrLen  = iStrLen - (iSepPos + iSepLength);
		szStr = szStr.Right(iStrLen);
	}
	return TRUE;
}


CString getToolBarResourceFN(void)
{
	CString sPath;
	CString sToolBarResPath;
	::GetModuleFileName(AfxGetApp()->m_hInstance, sPath.GetBufferSetLength(_MAX_PATH), _MAX_PATH);
	sPath.ReleaseBuffer();

	int nIndex  = sPath.ReverseFind(_T('\\'));
	if (nIndex > 0) sPath = sPath.Left(nIndex + 1); 
	else sPath.Empty();

	sToolBarResPath.Format(_T("%s%s"),sPath,TOOLBAR_RES_DLL);

	return sToolBarResPath;
}


// Create database tables form SQL scriptfiles; 061109 p�d
BOOL runSQLScriptFile(LPCTSTR fn,LPCTSTR check_table)
{
	UINT nFileSize;
	TCHAR sBuffer[BUFFER_SIZE];	// 10 kb buffer

	TCHAR sDB_PATH[127];
	TCHAR sUserName[127];
	TCHAR sPSW[127];
	TCHAR sDSN[127];
	TCHAR sLocation[127];
	TCHAR sDBName[127];
	SAClient_t m_saClient;
	int nAuthentication;

	BOOL bIsOK = FALSE;

	CString sSQL;

	try
	{

		if (fileExists(fn))
		{
			CFile file(fn,CFile::modeRead);
			nFileSize = file.Read(sBuffer,BUFFER_SIZE);
			file.Close();

			sBuffer[nFileSize] = '\0';

			GetAuthentication(&nAuthentication);

			if (getDBUserInfo(sDB_PATH,sUserName,sPSW,sDSN,sLocation,sDBName,&m_saClient))
			{
				if (nAuthentication == 0)	// Windows login
				{
					bIsOK = (_tcscmp(sLocation,_T("")) != 0);
				}
				else if (nAuthentication == 1)	// Server login
				{
					bIsOK = (_tcscmp(sLocation,_T("")) != 0 &&
									 _tcscmp(sDBName,_T("")) != 0 &&
									 _tcscmp(sUserName,_T("")) != 0 &&
									 _tcscmp(sPSW,_T("")) != 0);
				}
				else
					bIsOK = FALSE;
				
				if (bIsOK)
				{
					CSQLServer_ADODirect *pDB = new CSQLServer_ADODirect();
					if (pDB)
					{
						if (pDB->connectToDatabase(nAuthentication,sUserName,sPSW,sLocation))
						{

							// Set Dabasename to hms_administrator; 061113 p�d
							// Check if database exists, I NOT CREATE THE DATABASE; 061109 p�d
							_tcscpy(sDBName,HMS_ADMINISTRATOR_DBNAME);
							if (pDB->existsDatabase(sDBName))
							{
								if (!pDB->existsTableInDB(sDBName,check_table))
								{
									pDB->setDefaultDatabase(sDBName);
									pDB->commandExecute(sBuffer);
								}
							}	// if (!pDB->existsDatabase(sDBName))
							else
							{
								sSQL.Format(_T("create database %s"),sDBName);
								if (pDB->commandExecute(sSQL))
								{
									pDB->setDefaultDatabase(sDBName);
									pDB->commandExecute(sBuffer);
								}
							}
						}	// if (pDB->connectToDatabase(sUserName,sPSW,sLocation))
						delete pDB;
					}	// if (pDB)
				}	// if (bIsOK)
			}	// if (getDBUserInfo(sDB_PATH,sUserName,sPSW,sDSN,sLocation,sDBName,&m_saClient))
		}	// if (fileExists(fn))
	}
	catch(_com_error &e)
	{
		::MessageBox(0,e.Description(),_T("ERROR"),MB_ICONSTOP || MB_OK);
	}
	
	
	return FALSE;
}

// Create database table from string; 080627 p�d
int runSQLScriptFileEx(LPCTSTR table_name,CString script)
{
	TCHAR sDB_PATH[127];
	TCHAR sUserName[127];
	TCHAR sPSW[127];
	TCHAR sDSN[127];
	TCHAR sLocation[127];
	TCHAR sDBName[127];
	int nAuthentication;
	SAClient_t m_saClient;
	int nReturn = 1;	// OK
	BOOL bIsOK = FALSE;

	CString sScript,sSQL;

	try
	{

		if (!script.IsEmpty())
		{
			GetAuthentication(&nAuthentication);

			if (getDBUserInfo(sDB_PATH,sUserName,sPSW,sDSN,sLocation,sDBName,&m_saClient))
			{
				if (nAuthentication == 0)	// Windows login
				{
					bIsOK = (_tcscmp(sLocation,_T("")) != 0);
				}
				else if (nAuthentication == 1)	// Server login
				{
					bIsOK = (_tcscmp(sLocation,_T("")) != 0 &&
									 _tcscmp(sDBName,_T("")) != 0 &&
									 _tcscmp(sUserName,_T("")) != 0 &&
									 _tcscmp(sPSW,_T("")) != 0);
				}
				else
					bIsOK = FALSE;

				if (bIsOK)
				{
					CSQLServer_ADODirect *pDB = new CSQLServer_ADODirect();
					if (pDB)
					{
						if (pDB->connectToDatabase(nAuthentication,sUserName,sPSW,sLocation))
						{
							// Set Dabasename to hms_administrator; 080701 p�d
							// Check if database exists, I NOT CREATE THE DATABASE; 080701 p�d
							_tprintf(sDBName,_T("%s"),HMS_ADMINISTRATOR_DBNAME);
							if (pDB->existsDatabase(sDBName))
							{
								if (!pDB->existsTableInDB(sDBName,table_name))
								{
									pDB->setDefaultDatabase(sDBName);
									sScript.Format(script,table_name);
									pDB->commandExecute(sScript);
									nReturn = -1;	// Had to create the administrator table
								}
							}	// if (pDB->existsDatabase(sDBName))
							else
							{
								sSQL.Format(_T("CREATE DATABASE %s"),sDBName);
								if (pDB->commandExecute(sSQL))
								{
									if (pDB->existsDatabase(sDBName))
									{
										pDB->setDefaultDatabase(sDBName);
										sScript.Format(script,table_name);
										pDB->commandExecute(sScript);
										nReturn = -2; // Had to create the administrator database and table
									}
								}
							}

						}	// if (pDB->connectToDatabase(sUserName,sPSW,sLocation))
						delete pDB;
					}	// if (pDB)
				}	// if (_tcscmp(sLocation,"") != 0 &&
			}	// if (getDBUserInfo(sDB_PATH,sUserName,sPSW,sDSN,sLocation,sDBName,&m_saClient))
		}	// if (fileExists(fn))
	}
	catch(_com_error &e)
	{
		::MessageBox(0,e.Description(),_T("ERROR\n\UMDatabase\nrunSQLScriptFileEx"),MB_ICONSTOP || MB_OK);
		nReturn = 0;
	}
		
	return nReturn;
}


BOOL runSQLScript(LPCTSTR buffer)
{
	TCHAR sDB_PATH[127];
	TCHAR sUserName[127];
	TCHAR sPSW[127];
	TCHAR sDSN[127];
	TCHAR sLocation[127];
	TCHAR sDBName[127];
	SAClient_t m_saClient;
	BOOL bReturn = FALSE;
	int nAuthentication;
	BOOL bIsOK = FALSE;

	try
	{
		GetAuthentication(&nAuthentication);

		if (getDBUserInfo(sDB_PATH,sUserName,sPSW,sDSN,sLocation,sDBName,&m_saClient))
		{
				if (nAuthentication == 0)	// Windows login
				{
					bIsOK = (_tcscmp(sLocation,_T("")) != 0);
				}
				else if (nAuthentication == 1)	// Server login
				{
					bIsOK = (_tcscmp(sLocation,_T("")) != 0 &&
									 _tcscmp(sDBName,_T("")) != 0 &&
									 _tcscmp(sUserName,_T("")) != 0 &&
									 _tcscmp(sPSW,_T("")) != 0);
				}
				else
					bIsOK = FALSE;

			if (bIsOK)
			{
				CSQLServer_ADODirect *pDB = new CSQLServer_ADODirect();
				if (pDB)
				{
					if (pDB->connectToDatabase(nAuthentication,sUserName,sPSW,sLocation))
					{
						if (pDB->existsDatabase(sDBName))
						{
							pDB->setDefaultDatabase(sDBName);
							pDB->commandExecute(buffer);
							bReturn = TRUE;
						}	// if (!pDB->existsDatabase(sDBName))
					}	// if (pDB->connectToDatabase(sUserName,sPSW,sLocation))
					delete pDB;
				}	// if (pDB)
			}	// if (_tcscmp(sLocation,"") != 0 &&
		}	// if (getDBUserInfo(sDB_PATH,sUserName,sPSW,sDSN,sLocation,sDBName,&m_saClient))
	}
	catch(_com_error &e)
	{
		::MessageBox(0,e.Description(),_T("ERROR"),MB_ICONSTOP || MB_OK);
	}
	
	
	return bReturn;
}


BOOL connectToDB(int autentication,LPCTSTR location,LPCTSTR user_name,LPCTSTR psw)
{
	BOOL bReturn = FALSE;
	BOOL bIsOK = FALSE;
	CString sql, table;
	int nReturnAccess;

	try
	{
		if (autentication == 0)	// Windows login
		{
			bIsOK = (_tcscmp(location,_T("")) != 0);
		}
		else if (autentication == 1)	// Server login
		{
			bIsOK = (_tcscmp(location,_T("")) != 0 &&
							 _tcscmp(user_name,_T("")) != 0 &&
							 _tcscmp(psw,_T("")) != 0);
		}
		else
			bIsOK = FALSE;

		if (bIsOK)
		{
			CSQLServer_ADODirect *pDB = new CSQLServer_ADODirect();
			if (pDB)
			{
				if (pDB->connectToDatabase(autentication,user_name,psw,location))
				{
					bReturn = TRUE;
					//-----------------------------------------------------------------------------------
					// Check if logged in user has access to database; 090407 p�d
					sql.Format(_T("SELECT HAS_DBACCESS('%s') AS 'access'"),HMS_ADMINISTRATOR_DBNAME);
					if (pDB->recordsetQuery(sql))
					{
						if (pDB->getNumOf() > 0)
						{
							pDB->firstRec();
							nReturnAccess = pDB->getInt(_T("access"));
						}	// if (lNumOfRec > 0)
						pDB->recordsetClose();
						// Tell user; 090407 p�d
						if (nReturnAccess == 0)
							bReturn = FALSE;
					}
					if (nReturnAccess == 1)
					{
						// Create admin database if not exist; 091007 Peter
						sql.Format( _T("IF DB_ID('%s') IS NULL CREATE DATABASE %s"), HMS_ADMINISTRATOR_DBNAME, HMS_ADMINISTRATOR_DBNAME );
						if( !pDB->connectionExecute(sql) ) bReturn = FALSE;
						table.Format( table_Administrator, CString(HMS_ADMINISTRATOR_DBNAME) + CString(".dbo.") + CString(HMS_ADMINISTRATOR_TABLE) );
						sql.Format( _T("IF NOT EXISTS(SELECT 1 FROM %s..sysobjects WHERE name = '%s') %s"), HMS_ADMINISTRATOR_DBNAME, HMS_ADMINISTRATOR_TABLE, table );
						if( !pDB->connectionExecute(sql) ) bReturn = FALSE;
					}	// if (nReturnAccess == 1)

				}	// if (pDB->connectToDatabase(sUserName,sPSW,sLocation))
				delete pDB;
			}	// if (pDB)
		}	// if (bIsOK)
	}
	catch(_com_error &e)
	{
//		::MessageBox(0,e.Description(),"ERROR",MB_ICONSTOP || MB_OK);
	}
		
	return bReturn;
}

BOOL getDBServersList(CStringArray &arr,BOOL only_local)
{
	CString S;
	CString sServerName;
	CString sDBServerInCB;
	CStringArray sarrLocalSQLServerNames;
	CSQLInfoEnumerator obSQLEnumerate;

	// Always read local SQL Server instance name; 080704 p�d
	regGetSQLServerInstances(sarrLocalSQLServerNames);
	arr.RemoveAll();
	if (!only_local)
	{
		if(obSQLEnumerate.EnumerateSQLServers())
		{
			int iCount = obSQLEnumerate.m_szSQLServersArray.GetSize();
			for(int i = 0; i < iCount; i++)
			{
				sServerName = obSQLEnumerate.m_szSQLServersArray[i];
				if (sServerName.CollateNoCase(_T("(local)")) == 0)
				{
					if (sarrLocalSQLServerNames.GetCount() > 0)
					{
						for (int i1 = 0;i1 < 	sarrLocalSQLServerNames.GetCount();i1++)
						{
//							sDBServerInCB.Format(_T("%s\\%s"),(getNameOfComputer()),(sarrLocalSQLServerNames.GetAt(i1)));	
							sDBServerInCB.Format(_T("%s\\%s"),sServerName,sarrLocalSQLServerNames.GetAt(i1));	
							arr.Add(sDBServerInCB);
						}	// for (int i1 = 0;i1 < 	sarrLocalSQLServerNames.GetCount();i1++)
						// Also add name of computer as an alternative connection to DB Server; 081029 p�d						
						sDBServerInCB.Format(_T("%s"),(getNameOfComputer()));	
						arr.Add(sDBServerInCB);
					}	// if (sarrLocalSQLServerNames.GetCount() > 0)
				}	// if (sServerName.CollateNoCase(_T("(local)")) == 0)
				else
					arr.Add(sServerName);
			}
		}
	}
	else
	{
		if (sarrLocalSQLServerNames.GetCount() > 0)
		{
			for (int i1 = 0;i1 < 	sarrLocalSQLServerNames.GetCount();i1++)
			{
//				sDBServerInCB.Format(_T("%s\\%s"),(getNameOfComputer()),(sarrLocalSQLServerNames.GetAt(i1)));	
				sDBServerInCB.Format(_T("(local)\\%s"),(sarrLocalSQLServerNames.GetAt(i1)));	
				arr.Add(sDBServerInCB);
			}	// for (int i1 = 0;i1 < 	sarrLocalSQLServerNames.GetCount();i1++)
			// Also add name of computer as an alternative connection to DB Server; 081029 p�d						
			sDBServerInCB.Format(_T("%s"),(getNameOfComputer()));	
			arr.Add(sDBServerInCB);
		}	// if (sarrLocalSQLServerNames.GetCount() > 0)
	}

	return TRUE;
}


void setupForDBConnection(HWND hWndReciv,HWND hWndSend)
{
	if (hWndReciv != NULL)
	{
		DB_CONNECTION_DATA data;
    COPYDATASTRUCT HSData;
    memset(&HSData, 0, sizeof(COPYDATASTRUCT));
		HSData.dwData = 1;
		HSData.cbData = sizeof(DB_CONNECTION_DATA);
		HSData.lpData = &data;

		data.conn = NULL;

		::SendMessage(hWndReciv,WM_COPYDATA,(WPARAM)hWndSend, (LPARAM)&HSData);
	}
}


CString getShellDataDir(void)
{
	CString sPath;
	TCHAR szPath[MAX_PATH];
	if(SUCCEEDED(SHGetFolderPath(NULL, 
                             CSIDL_APPDATA|CSIDL_FLAG_CREATE, 
                             NULL, 
                             0, 
                             szPath))) 
	{
		sPath.Format(_T("%s\\%s"),szPath,SUBDIR_SHELL_DATA);
	}
	return sPath;
}

BOOL getSuites(CStringArray &str_list)
{
	CDiskObject cdo;
	CString sSuitesPath;

	// Read Suites in subdirectories; 051117 p�d
	sSuitesPath.Format(_T("%s"),getSuitesDir());
	cdo.EnumAllFilesWithFilter(SUITES_FN_WC,sSuitesPath,str_list);

	if (str_list.GetCount() > 0)
	{
		return TRUE;
	}

	return FALSE;

}

BOOL getModules(CStringArray &str_list)
{
	CDiskObject cdo;
	CString sModulesPath;

	sModulesPath.Format(_T("%s"),getModulesDir());
	cdo.EnumAllFilesWithFilter(MODULES_FN_WC,sModulesPath,str_list);

	if (str_list.GetCount() > 0)
	{
		return TRUE;
	}

	return FALSE;
}

CView *getFormViewByID(int idd)
{
	CString sResStr;
	CString sDocName;
	CString sCaption;
	CDocTemplate *pTemplate = NULL;
	CWinApp *pApp = AfxGetApp();

	// Get the stringtable resource, matching the TableIndex
	// This string is compared to the title of the document; 051212 p�d
	sResStr.LoadString(idd);

	POSITION pos = pApp->GetFirstDocTemplatePosition();
	while(pos != NULL)
	{
		pTemplate = pApp->GetNextDocTemplate(pos);
		pTemplate->GetDocString(sDocName, CDocTemplate::docName);
		sDocName = '\n' + sDocName;
		if (pTemplate && sDocName.Compare(sResStr) == 0)
		{	
			POSITION posDOC = pTemplate->GetFirstDocPosition();
			// Open only one instance of this window; 061002 p�d
			while(posDOC != NULL)
			{
				CDocument* pDocument = (CDocument*)pTemplate->GetNextDoc(posDOC);
				POSITION posView = pDocument->GetFirstViewPosition();
				if(posView != NULL)
				{
					CView* pView = pDocument->GetNextView(posView);
					if (pView)
					{
						return pView;
					}
				}	// if(posView != NULL)
			}	// while(posDOC != NULL)
		}
	}
	return NULL;
}

BOOL getSuites(CStringArray &str_list,BOOL clear)
{
	CDiskObject cdo;
	CString sSuitesPath;

	// Read Suites in subdirectories; 051117 p�d
	if (clear) str_list.RemoveAll();	// Clear list
	sSuitesPath.Format(_T("%s"),getSuitesDir());
	cdo.EnumAllFilesWithFilter(SUITES_FN_WC,sSuitesPath,str_list);

	if (str_list.GetCount() > 0)
	{
		return TRUE;
	}

	return FALSE;

}

BOOL getModules(CStringArray &str_list,BOOL clear)
{
	CDiskObject cdo;
	CString sModulesPath;

	if (clear) str_list.RemoveAll();	// Clear list
	sModulesPath.Format(_T("%s"),getModulesDir()); //,SUBDIR_MODULES);
	cdo.EnumAllFilesWithFilter(MODULES_FN_WC,sModulesPath,str_list);

	if (str_list.GetCount() > 0)
	{
		return TRUE;
	}

	return FALSE;
}

void runDoDatabaseTablesInSuitesModules(LPCTSTR db_name,CXTPStatusBar &status_bar,CProgressDialog &prg_ctrl,LPCTSTR sbar_text)
{
	CString sStatusBarText;
	CStringArray arrSuitesAndModules;
	getSuites(arrSuitesAndModules,FALSE);
	getModules(arrSuitesAndModules,FALSE);

	if (arrSuitesAndModules.GetCount() == 0)
		return;

	HINSTANCE hModule = NULL;
	typedef CRuntimeClass *(*Func)(LPCTSTR);
  Func proc;
	// Check if there's any ShellData files; 080421 p�d
	if (arrSuitesAndModules.GetCount() > 0)
	{
		if (prg_ctrl.GetSafeHwnd())
			prg_ctrl.setProgressRange(arrSuitesAndModules.GetCount());
		for (int i = 0;i < arrSuitesAndModules.GetCount();i++)
		{
			hModule = AfxLoadLibrary(arrSuitesAndModules[i]);
			if (hModule != NULL)
			{
				proc = (Func)GetProcAddress((HMODULE)hModule, EXPORTED_DO_DATABASE_TABLES);
				if (proc != NULL)
				{
					sStatusBarText.Format(_T("%s : %s"),sbar_text,arrSuitesAndModules[i]);
					if (status_bar.GetSafeHwnd())
						status_bar.SetPaneText(1,sStatusBarText);
					if (prg_ctrl.GetSafeHwnd())
					{
						prg_ctrl.SetWindowText( arrSuitesAndModules[i].Right(arrSuitesAndModules[i].GetLength() - arrSuitesAndModules[i].ReverseFind('\\') - 1) );
						prg_ctrl.stepProgress(i+1);
					}
					// call the function
					proc((db_name));
				}	// if (proc != NULL)
				AfxFreeLibrary(hModule);
			}	// if (hModule != NULL)
		}	// for (int i = 0;i < arr.GetCount();i++)
	}	// if (arr.GetCount() > 0)
}

void runDoAlterTablesInSuitesModules(LPCTSTR db_name)
{
	CStringArray arrSuitesAndModules;
	getSuites(arrSuitesAndModules,FALSE);
	getModules(arrSuitesAndModules,FALSE);

	if (arrSuitesAndModules.GetCount() == 0)
		return;

	HINSTANCE hModule = NULL;
	typedef CRuntimeClass *(*Func)(LPCTSTR);
	Func proc;
	// Check if there's any ShellData files; 080421 p�d
	if (arrSuitesAndModules.GetCount() > 0)
	{
		for (int i = 0;i < arrSuitesAndModules.GetCount();i++)
		{
			hModule = AfxLoadLibrary(arrSuitesAndModules[i]);
			if (hModule != NULL)
			{
				proc = (Func)GetProcAddress((HMODULE)hModule, EXPORTED_DO_ALTER_TABLES);
				if (proc != NULL)
				{
					// call the function
					proc((db_name));
				}	// if (proc != NULL)
				AfxFreeLibrary(hModule);
			}	// if (hModule != NULL)
		}	// for (int i = 0;i < arr.GetCount();i++)
	}	// if (arr.GetCount() > 0)
}

void setToolbarBtnIcon(LPCTSTR rsource_dll_fn,CXTPControl *pCtrl,int icon_id,LPCTSTR tool_tip,BOOL show)
{
	if (pCtrl)
	{
		if (show)
		{
			pCtrl->SetTooltip(tool_tip);
			HICON hIcon = ExtractIcon(AfxGetInstanceHandle(), rsource_dll_fn, icon_id);
			if (hIcon) pCtrl->SetCustomIcon(hIcon);
		}
		else
			pCtrl->SetVisible(FALSE);
	}
}
